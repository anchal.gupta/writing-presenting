# writing-presenting
[![pipeline status](https://git.ligo.org/anchal.gupta/writing-presenting/badges/master/pipeline.svg)](https://git.ligo.org/anchal.gupta/writing-presenting/commits/master)

This repo will be used for writing (thesis/candidacy/reports) or presentations (conference/thesis/candidacy).

## Thesis:
[thesis.pdf](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/file/thesis/thesis.pdf?job=thesis)

## CTN DAMOP 2020 Poster:
![CTN_DAMOP2020_Poster](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/raw/CTN_DAMOP_Poster/CTN_DAMOP2020_Poster.png?job=CTN_DAMOP2020_Poster)

[CTN_DAMOP2020_Poster.pdf](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/file/CTN_DAMOP_Poster/CTN_DAMOP2020_Poster.pdf?job=CTN_DAMOP2020_Poster)

[CTN_DAMOP2020_Poster_Extended_Abstract.pdf](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/file/CTN_DAMOP_Poster/ExtendedAbstract.pdf?job=CTN_DAMOP2020_Poster)

## CTN LVK Sep 2020 Poster:

[CTN_LVKSep2020_Poster.pdf](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/file/CTN_LVKSep2020_Poster/CTN_LVKSep2020_Poster.pdf?job=CTN_LVKSep2020_Poster)

[CTN_LVKSep2020_Poster_Extended_Abstract.pdf](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/file/CTN_LVKSep2020_Poster/ExtendedAbstract.pdf?job=CTN_LVKSep2020_Poster)


## White Papers

[LIGO-T1900263 Using PLL for Frequency Noise Measurement](https://git.ligo.org/anchal.gupta/writing-presenting/-/jobs/artifacts/master/file/whitePapers/LIGO-T1900263_PLL_for_Freq_Noise/LIGO-T1900263_PLL_for_Freq_Noise.pdf?job=T1900263)
