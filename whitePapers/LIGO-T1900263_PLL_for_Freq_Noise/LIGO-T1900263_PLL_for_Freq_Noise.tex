\documentclass[colorlinks=true,pdfstartview=FitV,linkcolor=blue,
            citecolor=red,urlcolor=magenta]{ligodoc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{booktabs, threeparttable}
\usepackage{array}
\newcolumntype{L}{>{\centering\arraybackslash}{3cm}}
\usepackage{amsmath}
\newcommand{\lp}[1]{\Bar{#1}(s)}
\newcommand{\fs}[1]{\tilde{#1}(f)}
\newcommand{\ts}[2]{#1_{#2}(t)}
\setmarginsrb{1in}{0.5in}{1in}{0.5in}{0.3in}{0.2in}{0.3pt}{0.4in}
%\usepackage{geometry}
%\geometry{
% letterpaper,
% total={210mm,297mm},
% left=20mm,
% right=20mm,
% top=20mm,
% bottom=20mm,
%}
\usepackage{hyperref}% add hypertext capabilities
\hypersetup{
    colorlinks=true,
    linkcolor=cyan,
    filecolor=magenta,
    urlcolor=blue,
}
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{esvect}
\usepackage[mathlines]{lineno}
\usepackage[toc,page]{appendix}
\usepackage[english]{babel}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\rightmark}
\fancyhead[L]{}
\fancyhead[C]{LIGO-T1900263-v1}
\fancyfoot[L]{PLL Freq Noise Measurement - A. Gupta (\hyperlink{mailto://anchal@caltech.edu}{anchal@caltech.edu})}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\date{5/6/2019}

\ligodccnumber{T}{19}{00263}{}{v1}% \ligodistribution{AIC, ISC}


\title{Using PLL for Frequency Noise Measurement}

\author{Anchal Gupta}
\begin{document}

\newpage
\tableofcontents
%-------------------------------------------------%
%Some useful syntax:
%-------------------------------------------------%
%For including figures, enter this:
%\begin{figure}[!h]
%\includegraphics[width=\textwidth,keepaspectratio=true]{filename.xy%z}
%\caption{Figure caption}
%\label{fig:FigLabel}
%\end{figure}

%-------------------------------------------------%
%For including tables, enter this:
%\begin{table}[!h]
%\centering
%\begin{tabular}{|l|l|l|}\hline
%
%R1C1line1 & \multicolumn{1}{m{4cm}|}{R1C2line1} & %\multicolumn{1}{m{10cm}|}{R1C3line1}\\
%R1C1line2 & \multicolumn{1}{m{4cm}|}{R1C2line2} & %\multicolumn{1}{m{10cm}|}{R1C3line2}\\\hline
%R2C1 & \multicolumn{1}{m{4cm}|}{R2C2} & %\multicolumn{1}{m{10cm}|}{R2C3}\\\hline%
%
%\end{tabular}
%\caption{Table Title}
%\label{tab:TableLabel}
%\end{table}

%-------------------------------------------------%
\newpage
\section{Notation meaning}
In this document:
\begin{itemize}
    \item Capital Roman or Greek letters refer to a constant value like $V_{RF}$ or $\Omega_{LO}$.
    \item Small roman or Greek letters refer to the time-varying part of the same Capital symbol. For example, $\ts{\phi}{sig}$, $\ts{\phi}{VCO}$.
    \item Small roman or Greek letters with $\delta$ suffix refer to noise counterpart of the same symbol. For example, $\delta v_{RF}(t)$ is noise of $V_{RF}$.
    \item Small roman or Greek letters with a bar on them are Laplace transforms of the corresponding time domain noise terms. For example, $\lp{\phi_{sig}}$ is noise spectrum of $\ts{\phi}{sig}$.
    \item Small roman or Greek letters with tilde on them are noise spectrums of the corresponding time domain noise terms. For example, $\fs{\phi_{sig}}$ is noise spectrum of $\ts{\phi}{sig}$.
    \item Calligraphic Capital Roman or Greek letters are the full signals. For example,$\mathcal{V_{RF}}(t)$.
    \item Subscript RF refers to signal at RF port of the mixer.
    \item Subscript LO refers to signal at Local Oscillator port of the mixer.
    \item Subscript Mix refers to Mixer.
    \item Subscript VCO refers to Voltage Controlled Oscillator.
    \item Subscript Amp refers to Amplifier.
    \item Subscript AmpIn refers to input referred noise of Amplifier.
    \item Subscript LPF refers to Low Pass Filter.
\end{itemize}

%##########################################################################%
\section{Introduction}
A phase locked loop (PLL) is a feedback circuit where phase is a state variable. In this very limited document, I'll focus on only first order phase locked loops which are sensitive to phase ramp and are used in the context of measuring frequency noise of an incoming signal. I have tried my best to not keep loose ends in the analysis and not make any mistakes, but please feel free to email the undersigned for any suggestions/corrections/comments.\\
\begin{figure}[!h]
\includegraphics[width=\textwidth,keepaspectratio=true]{PLLSchematicClk.pdf}
\caption{Frequency Noise Measurement by PLL}
\label{fig:PLLSch}
\end{figure}
Fig (\ref{fig:PLLSch}) shows the implementation of PLL for frequency noise measurement. This measurement scheme assumes that the frequency noise is coming in as a RF signal at some frequency $\Omega_{LO}$. This could be a Beat Note frequency between two sources or it could be a heatrodyne measurement being done at $\Omega_{LO}$ modulation frequency. The signal is mixed down with a Voltage Controlled Oscillator (VCO) signal which is running at a central frequency same as $\Omega_{LO}$ and is frequency modulated by an external input. The output of the mixer is low passed and amplified and fed into the modulation external input of the VCO.

%-------------------------------------------------%
\subsection{Mixer as phase detector}{\label{sec:Mixer}}
In the time domain, the signal coming in at RF port is:
\begin{equation}
    \mathcal{V_{RF}}(t)=V_{RF}cos(\Omega_{LO}t+\phi_{sig}(t))
\end{equation}
Here, $\Omega_{LO}$ is the local oscillator frequency used in the measurement. This could be a beat frequency between two lasers or it could be a heterodyne measurement frequency and is generally fixed to tens of MHz. $\phi_{sig}(t)$ is the phase noise in the incoming signal corresponding to the frequency noise $\delta\omega_{sig}(t)$ we are trying to measure.
\begin{equation}{\label{eq:pnfn}}
    \frac{\partial \phi_{sig}(t))}{\partial t} = \delta\omega_{sig}(t)
\end{equation}
The signal at LO port looks like:
\begin{equation}
    \mathcal{V_{LO}}(t)=V_{LO}cos(\Omega_{LO}t+\phi_{LO}(t))
\end{equation}
We will always have a low pass filter at the output of the mixer with 3 dB point much lower than $\Omega_{LO}$. We will club mixer loss and low pass filter loss into $M$. In the time domain, the output of the mixer is given by:
\begin{equation}
    \begin{aligned}
        \mathcal{V}_{Mix}(t) &= V_{RF}Mcos(\Omega_{LO}t+\phi_{sig}(t))cos(\Omega_{LO}t+\phi_{LO}(t))\\
        &= \frac{V_{RF}M}{2}\left( cos(\phi_{sig}(t)-\phi_{LO}(t)) + cos(2\Omega_{LO}t\phi_{sig}(t)+ \phi_{LO}(t))\right)
    \end{aligned}
\end{equation}
Due to low pass filtering, the second term drops out. We'll show later in Sec \ref{sec:CheckBack} that the reference point of LO phase does not matter, hence we can change $\ts{\phi}{LO} => \pi/2+\phi_{LO}(t)$. Then we get:
\begin{equation}
    \mathcal{V}_{Mix}(t) = \frac{V_{RF}M}{2} sin(\phi_{LO}(t)-\phi_{sig}(t))
\end{equation}
We'll see in Sec \ref{sec:CheckBack} that in PLL, $\phi_{LO}-\Delta\Phi_{sig}(t) << 1$, i.e. $\phi_{LO}$ would follow $\Delta\Phi_{sig}(t)$ closely. Going ahead with this self-consistent approximation whose consistency will be clear later, we get:
\begin{equation}
    \mathcal{V}_{Mix}(t) = \frac{V_{RF}M}{2}\left( \phi_{LO}(t)-\phi_{sig}(t)\right)
\end{equation}
This means that with the above-mentioned conditions and approximations, the mixer is working as a phase detector. It measures the difference between the phase of the RF signal and the phase of LO. Since we are dealing with frequency noise, using the time domain signal to do analysis is not ideal. Using phase as the state variable makes more sense. So, we'll go into Laplace space to do the calculations. In Laplace space:
\begin{equation}{\label{eq:Mixer}}
    \lp{\mathcal{V}_{Mix}} = \frac{V_{RF}M}{2}\left( \lp{\phi_{LO}}-\lp{\phi_{sig}}\right) = K_M \left( \lp{\phi_{LO}}-\lp{\phi_{sig}}\right)
\end{equation}
where
\begin{equation}{\label{eq:DepOnVrf}}
    K_M = \frac{V_{RF}M}{2}
\end{equation}
$K_M$ has units of $V/rad$. Note here that the transfer function for Mixer and Low Pass Filter, being used as a Phase Detector, actually depends on the RF voltage level. This makes it susceptible to noise in RF voltage. We'll take this up later in noise analysis in Sec(\ref{sec:NoiseAnalysis}).\\
However, there is a way to avoid this as well. If one uses  a Double Balanced Mixer and keeps RF power level high enough to saturate the mixer, than the output of the mixer is independent of RF amplitude. \href{https://www.minicircuits.com/app/AN41-001.pdf}{See this technical note from Mini-Circuits}\cite{MCPD}. It mentions that for a level +7 dBm mixer, if RF power is +1 dBm, then saturation is achieved. The numbers are +10 dBm and +15 dBm for level +17 dBm and +23 dBm mixers respectively.

%-------------------------------------------------%
\subsection{Amplifier}
The amplifier is used with no frequency shaping and a simple gain of $K_A$. For probing purposes, we assume that this amplifier has a summing junction as well with a negative input port B. This makes the time domain output of the amplifier as:
\begin{equation}
    \ts{\mathcal{V}}{Amp} = K_A \left( \ts{V}{A} - \ts{V}{B} + \ts{\delta v}{amp}\right)
\end{equation}
where $\ts{\delta v}{amp}$ is the input referred noise of the amplifier. For port B shorted (which is the normal case), the transfer function of the amplifier in Laplace Space is a constant $K_A$.

%-------------------------------------------------%
\subsection{VCO}
A VCO is a device which can change the frequency of its output proportional to an input voltage change. In the time domain, the output of a VCO is given by:
\begin{equation}
    \mathcal{V_{VCO}}(t)=V_{VCO}cos(\Omega_{LO}t+K_{VCO}V_{in}t + \Phi_{LO}+\ts{\delta\phi}{VCO})
\end{equation}
Here, $K_{VCO}$ is the actuation slope of the VCO in $Hz/V$, $\Phi_{LO}$ is some constant phase difference with a reference and $\ts{\delta\phi}{VCO}$ is the phase noise of VCO at its output. In Laplace space where state variable is phase, the operation of VCO becomes:
\begin{equation}{\label{eq:VCO}}
    \lp{\phi_{VCO}} =\frac{K_{VCO}}{s}\lp{V_{in}}
\end{equation}

%##########################################################################%
\section{Ideal PLL where all components are noiseless}
In an ideal PLL where every component of the PLL is noiseless, the analysis is simple and gives the basic functioning of PLL. So assuming there are no other noise sources than the frequency noise of the signal itself, the incoming RF signal would be:
\begin{equation}
    \mathcal{V_{RF}}(t)=V_{RF}cos(\omega_{LO}t+\phi_{sig}(t))
\end{equation}
where $\phi_{sig}(t)$ is the phase noise of the incoming signal. As discussed earlier, for frequency noise measurement, it is better to use phase as our state variable and so into Laplace space. This means that the incoming signal is actually $\phi_{sig}(t)$ which in Laplace space is $\lp{\phi_{sig}}$. Similarly, we'll call the feedback signal from VCO:
\begin{equation}
    \mathcal{V_{VCO}}(t)=V_{VCO}cos(\omega_{LO}t+\phi_{VCO}(t))
\end{equation}
but in Laplace space with phase as state variable, the signal is $\lp{\phi_{VCO}}$. After the mixer, the signal would be (using Eq(\ref{eq:Mixer})):
\begin{equation}
    \lp{u_1} = K_M (\lp{\phi_{VCO}}-\lp{\phi_{sig}})
\end{equation}
where $u_1(t)$ is time domain voltage signal from low pass filter going into the amplifier. $\lp{u_1}$ is the Laplace transform of $u_1(t)$.
In normal operation, port B as shown in fig(\ref{fig:PLLSch}) will be shorted and not used. The output from the amplifier would be:
\begin{equation}
    \lp{u_2} = K_A \lp{u_1} = K_A K_M (\lp{\phi_{VCO}}-\lp{\phi_{sig}})
\end{equation}
Here, $u_2(t)$ is time domain voltage signal from output of amplifier going into the VCO. Then output from VCO will be:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s}\lp{u_2} = \frac{K_{VCO} K_A K_M}{s}(\lp{\phi_{VCO}}-\lp{\phi_{sig}})
\end{equation}
Solving for $\lp{\phi_{VCO}}$:
\begin{equation}{\label{eq:VCOphisigrel}}
    \lp{\phi_{VCO}} = \frac{K_{VCO} K_A K_M}{K_{VCO} K_A K_M - s}\lp{\phi_{sig}}
\end{equation}
Then in terms of $\lp{u_2}$, we have:
\begin{equation}
    \lp{u_2} = \frac{K_A K_M s}{K_{VCO} K_A K_M - s}\lp{\phi_{sig}}
\end{equation}
We see that $\lp{u_2}$ is a measurable voltage signal directly proportional to the signal of interest $\lp{\phi_{sig}}$. Inverse relationship will be:
\begin{equation}
    \lp{\phi_{sig}} = \left(\frac{K_{VCO}}{s}- \frac{1}{K_A K_M} \right)\lp{u_2}
\end{equation}
Eventually, we want to measure frequency noise spectrum$\fs{\delta\omega_{sig}}$. Using relation in Eq(\ref{eq:pnfn}), taking its Laplace transform:
\begin{equation}
    \lp{\delta\omega_{sig}} = \left(K_{VCO} - \frac{s}{K_A K_M}\right)\lp{u_2}
\end{equation}
This relation in frequency space is:
\begin{equation}
    \fs{\delta\omega_{sig}} = \left(K_{VCO}-\frac{\iota2\pi f}{K_A K_M}\right)\fs{u_2}
\end{equation}
Therefore, for $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$, the spectrum measured by doing FFT of $\ts{u}{2}$ will be related to frequency noise of the signal by:
\begin{equation}{\label{eq:Meas}}
    \fs{\delta\omega_{sig}} = K_{VCO}\fs{u_2}
\end{equation}
Hence, one can measure the frequency noise of a signal by employing this PLL and measuring the FFT of the signal after the amplifier multiplied by the actuation slope of VCO.

\subsection{Checking back validity of assumptions}{\label{sec:CheckBack}}
In assumed in Sec \ref{sec:Mixer} that $\ts{\phi}{VCO}-\ts{\phi}{sig}<<1$. We can check the self-consistency of this assumption now. From Eq (\ref{eq:VCOphisigrel}), we see that:
\begin{equation}
    \fs{\phi_{VCO}} = \frac{K_{VCO} K_A K_M}{K_{VCO} K_A K_M - \iota2\pi f}\fs{\phi_{sig}}
\end{equation}
And for $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$:
\begin{equation}
    \fs{\phi_{VCO}} \approx \fs{\phi_{sig}}
\end{equation}
Going back to time-domain:
\begin{equation}
    \ts{\phi}{VCO} \approx \ts{\phi}{sig}
\end{equation}
Therefore, in steady state, the PLL drives $\ts{\phi}{VCO}$ close to $\ts{\phi}{sig}$ and hence the assumption $\ts{\phi}{VCO}-\ts{\phi}{sig}<<1$ is valid. Futher in Sec \ref{sec:Mixer}, we also assumed that the initial phase of VCO can be set arbitrarily. We can see that since the steady state solution tries to remain close to $\pi/2$ phase difference between RF signal and VCO signal, after an initial transient, PLL will always catch up to make sure their phase difference is close to $\pi/2$. Hence, our assumption that we can set intial phase arbitrarily also holds true for PLL.

%##########################################################################%
\section{Noise Analysis}{\label{sec:NoiseAnalysis}}
Now, we'll see how different noise sources can creep in and affect our measurement of frequency noise. Instead of calculating all noises at once, it is easier to calculate noise contribution from an injection point in the loop one at a time. For independent noise sources, all noises will just add in quadrature in the end to the result.
%-------------------------------------------------%
\subsection{Noises at Amplifier Input}{\label{sec:AmpInNoise}}
At the input of the amplifier, there could be three sources of noise:
\begin{enumerate}
    \item \textbf{Mixer Noise} The mixer can add some Johnson noise at its output.
    \item \textbf{Low Pass Filter} A passive LPF with non-resistive elements would generally be noiseless, but this could be a source of noise as well into the loop.
    \item \textbf{Input referred noise of Amplifier} The amplifier would have an input referred noise which would be injected at this point in the circuit.
\end{enumerate}
All above noise sources could be represented by just one noise $\ts{\delta v}{A}$ which is noise at the input port A of the amplifier. To analyze how this comes into the final result, we'll assume that our signal is noiseless, that is, it is a pure tone sine wave of frequency $\Omega_{LO}$ and $\ts{\phi}{sig}=0$. Then in Laplace space, the output of the mixer (with LPF) would be:
\begin{equation}
    \lp{u_1} = K_M \lp{\phi_{VCO}} + \lp{v_A}
\end{equation}
Then the output of the amplifier would be:
\begin{equation}
    \lp{u_2} = K_A K_M \lp{\phi_{VCO}} + K_A \lp{v_A}
\end{equation}
Then output of VCO would be:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s}K_A K_M \lp{\phi_{VCO}} + \frac{K_{VCO}}{s} K_A \lp{v_A}
\end{equation}
Solving for $\lp{\phi_{VCO}}$, we get:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s - K_{VCO}K_A K_M } K_A \lp{v_A}
\end{equation}
Then, our measurement signal $\lp{u_2}$ will be:
\begin{equation}
    \lp{u_2} = \frac{s}{s - K_{VCO}K_A K_M } K_A \lp{v_A}
\end{equation}
Therefore, when measurement would be done as per Eq(\ref{eq:Meas}), the contribution of $\ts{v}{A}$ noise in the measurement will be:
\begin{equation}
    \fs{\delta\omega_{v_A}} = \left|\frac{\iota 2\pi f}{\iota 2\pi f - K_{VCO}K_A K_M }\right| K_A K_{VCO}\fs{v_A}
\end{equation}
In the approximation range of $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$, contribution is:
\begin{equation}
    \fs{\delta\omega_{v_A}} = \frac{2\pi f}{K_M}\fs{v_A}
\end{equation}
So noises due to mixer thermal noise, Low Pass Filter noise or Amplifier input referred noise are present in measured signal.

%-------------------------------------------------%
\subsection{Phase Noise of VCO}{\label{sec:BNVCOnoise}}
Phase noise due to VCO can also limit the measurement. Here, we need to bifurcate in the two cases of measurement.
\subsubsection{Beat Note Signal Measurement}
When the RF signal is a Beat Note, again assuming zero frequency noise in the signal, the output of the mixer and low pass filter would be:
\begin{equation}
    \lp{u_1} = K_M \lp{\phi_{VCO}}
\end{equation}
Then the output of the amplifier would be:
\begin{equation}{\label{eq:u2pVCOinVCOnoise}}
    \lp{u_2} = K_A K_M \lp{\phi_{VCO}}
\end{equation}
Then at the output of VCO, phase noise of VCO will get added:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s}K_A K_M \lp{\phi_{VCO}} + \lp{\delta\phi_{VCO}}
\end{equation}
Solving for $\lp{\phi_{VCO}}$, we get:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{s}{s - K_{VCO}K_A K_M } \lp{\delta\phi_{VCO}}
\end{equation}
Then, our measurement signal $\lp{u_2}$ will be (using Eq(\ref{eq:u2pVCOinVCOnoise})):
\begin{equation}
    \lp{u_2} =  \frac{s}{s - K_{VCO}K_A K_M } K_A K_M \lp{\delta\phi_{VCO}}
\end{equation}
Therefore, when measurement would be done as per Eq(\ref{eq:Meas}), the contribution of $\ts{\delta\phi}{VCO}$ noise in the measurement will be:
\begin{equation}
    \fs{\delta\omega_{\delta\phi_{VCO}}} = \left|\frac{\iota 2\pi f}{\iota 2 \pi f - K_{VCO}K_A K_M }\right| K_A K_M K_{VCO}\lp{\delta\phi_{VCO}}
\end{equation}
In the approximation range of $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$, contribution is:
\begin{equation}
    \fs{\delta\omega_{\delta\phi_{VCO}}} = 2\pi f\fs{\delta\phi_{VCO}} = \fs{\delta\omega_{VCO}}
\end{equation}
Therefore the frequency noise of VCO contributes directly to the frequency noise measurement of the signal. This puts a direct fundamental limit to this measurement method. The VCO must be more stable than the frequency noise being measured.
\subsubsection{Heterodyne Signal Measurement}
Frequency noise could be measured with the heterodyne measurement as well. In this case, the incoming signal is frequency shifted by $\Omega_{LO}$ using some non-linear method and is beaten with a phase delayed version of itself. The RF signal produced this way will be a $\Omega_{LO}$ wave with frequency jitter same as the original signal. In this case, we have access to the component shifting the frequency by $\Omega_{LO}$ which would be some function generator. As shown in schematic fig (\ref{fig:PLLSch}), one should connect this function generator to the same clock source as the VCO. This way, the two of them will have the same inherent phase noise. If this is not done, then the noise analysis is same as Sec (\ref{sec:BNVCOnoise}).\\
In this case, even if the signal is noiseless, the phase noise of clock $\ts{\delta\phi}{CLK}$, shared by VCO and the function generator will be present at the signal input. Then the output of the mixer and low pass filter would be:
\begin{equation}
    \lp{u_1} = K_M (\lp{\phi_{VCO}} - \lp{\delta\phi_{CLK}})
\end{equation}
Then the output of the amplifier would be:
\begin{equation}{\label{eq:u2pVCOinVCOnoiseHetD}}
    \lp{u_2} = K_A K_M (\lp{\phi_{VCO}} - \lp{\delta\phi_{CLK}})
\end{equation}
Then at the output of VCO, phase noise of VCO will get added as well:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s}K_A K_M (\lp{\phi_{VCO}} - \lp{\delta\phi_{CLK}}) + \lp{\delta\phi_{CLK}}
\end{equation}
Solving for $\lp{\phi_{VCO}}$, we get:
\begin{equation}
    \lp{\phi_{VCO}} = \lp{\delta\phi_{CLK}}
\end{equation}
Then, our measurement signal $\lp{u_2}$ will be (using Eq(\ref{eq:u2pVCOinVCOnoiseHetD})):
\begin{equation}
    \lp{u_2} =  0
\end{equation}
And Voila! This means in such a measurement, ideally, the frequency noise due to VCO would get canceled by frequency noise from function generator as both share the same reference clock. Of course in the real world, this would not be the case, but still, this measurement method will not be limited by the phase noise of VCO.

%-------------------------------------------------%
\subsection{RF Signal Amplitude Noise}
As mentioned in Sec (\ref{sec:Mixer}), the transfer function magnitude of Mixer $K_M$ depends on RF voltage amplitude as in Eq (\ref{eq:DepOnVrf}). Any noise in the amplitude of RF voltage will change the transfer function of the mixer itself. Let's assume that the RF signal amplitude has a noise component $\ts{\delta v}{RF}$. Then in case of everything else being noiseless, the output after the mixer and low pass filter will be:
\begin{equation}
    \lp{u_1} = \frac{M V_{RF}}{2}\lp{\phi_{VCO}} + \frac{M \lp{\delta v_{RF}}}{2}\lp{\phi_{VCO}}
\end{equation}
Then the output of the amplifier would be:
\begin{equation}
    \lp{u_2} = K_A K_M \lp{\phi_{VCO}} + K_A \frac{M \lp{\delta v_{RF}}}{2}\lp{\phi_{VCO}}
\end{equation}
Then output of VCO would be:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s}K_A K_M \lp{\phi_{VCO}} + \frac{K_{VCO}}{s} K_A \frac{M \lp{\delta v_{RF}}}{2}\lp{\phi_{VCO}}
\end{equation}
Solving for $\lp{\phi_{VCO}}$, we get:
\begin{equation}
    \lp{\phi_{VCO}} = 0
\end{equation}
Therefore, at least to first order, the fluctuations in signal amplitude do not affect frequency noise measurement. So this method is insensitive to intensity noise of the signal.

%-------------------------------------------------%
\subsection{Complete form}{\label{sec:CF}}
Here, I provide full forms of noise contributions from various noise sources:
\subsubsection{Beat Note Signal Measurement}{\label{sec:CFBN}}
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &= \left|\fs{\delta\omega_{sig}}\right|^2+\left|\fs{\delta\omega_{VCO}}\right|^2+\left|\frac{2 \pi f }{K_M} \fs{v_{Mix}}\right|^2+\left|\frac{2 \pi f }{K_M} \fs{v_{AmpIn}}\right|^2+\left|\frac{2 \pi f }{K_M} \fs{v_{LPF}}\right|^2
    \end{aligned}
\end{equation}
\subsubsection{Heterodyne Signal Measurement}{\label{sec:CFHetD}}
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &= \left|\fs{\delta\omega_{sig}}\right|^2+\left|\frac{2 \pi f }{K_M} \fs{v_{Mix}}\right|^2+\left|\frac{2 \pi f }{K_M} \fs{v_{AmpIn}}\right|^2+\left|\frac{2 \pi f }{K_M} \fs{v_{LPF}}\right|^2
    \end{aligned}
\end{equation}

%##########################################################################%
\section{Higher order noise analysis}
The previous section helps in getting the contributions of noise sources in the final measured signal. However, since we analyzed the noises one at a time, we basically got first order contributions only. Here, I attempt to do a more complicated noise analysis for the sake of completeness when all noise sources are present together.

%-------------------------------------------------%
\subsection{Beat Note Signal Measurement}
In this case, the output at mixer output after the low pass filter would be:
\begin{equation}
    \lp{u_1} = K_M (\lp{\phi_{VCO}} -\lp{\phi_{sig}})+ \frac{M \lp{\delta v_{RF}}}{2} (\lp{\phi_{VCO}} -\lp{\phi_{sig}}) +  \lp{v_A}
\end{equation}
Here, $\ts{\delta v}{A}$ is total noise due to the sources mentioned in Sec (\ref{sec:AmpInNoise}). Then the output of the amplifier would be:
\begin{equation}{\label{eq:u2pVCOHONABN}}
    \lp{u_2} = K_A K_M (\lp{\phi_{VCO}} -\lp{\phi_{sig}})+ K_A \frac{M \lp{\delta v_{RF}}}{2} (\lp{\phi_{VCO}} -\lp{\phi_{sig}}) +  K_A \lp{v_A}
\end{equation}
Then at the output of VCO, phase noise of VCO will get added:
\begin{equation}
    \lp{\phi_{VCO}} = \left(\frac{K_{VCO}}{s}K_A K_M + K_A \frac{K_{VCO}M \lp{\delta v_{RF}}}{2 s} \right)\left(\lp{\phi_{VCO}} -\lp{\phi_{sig}}\right) + \frac{K_{VCO}}{s} K_A \lp{v_A}+ \lp{\delta\phi_{VCO}}
\end{equation}
Solving for $\lp{\phi_{VCO}}$, we get:
\begin{equation}
    \begin{aligned}
    \lp{\phi_{VCO}} &= \frac{-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\phi_{sig}}\\
    &+ \frac{2K_{VCO}}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}\\
    &+ \frac{2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\delta\phi_{VCO}}
    \end{aligned}
\end{equation}
Then, our measurement signal $\lp{u_2}$ will be (using Eq(\ref{eq:u2pVCOHONABN})):
\begin{equation}
    \begin{aligned}
        \lp{u_2} &=  \left(K_A K_M+K_A \frac{M \lp{\delta v_{RF}}}{2}\right)\\
        &\left( \frac{-2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\phi_{sig}}\right.\\
        &+\frac{2K_{VCO}}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}\\
        &+\left. \frac{2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\delta\phi_{VCO}} \right)\\
        &+ K_A \lp{v_A}\\
        &=  \left(K_A K_M+K_A \frac{M \lp{\delta v_{RF}}}{2}\right)\\
        &\left( \frac{-2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\phi_{sig}}\right.\\
        &+\left. \frac{2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\delta\phi_{VCO}} \right)\\
        &+\frac{2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}
    \end{aligned}
\end{equation}
Therefore, when measurement would be done as per Eq(\ref{eq:Meas}), the noise contributions would be:
\begin{equation}
    \begin{aligned}
        \lp{\delta\omega_{meas}} &= K_{VCO}\lp{u_2}\\
        &=  \left(K_A K_M+K_A \frac{M \lp{\delta v_{RF}}}{2}\right)\\
        &\left( \frac{-2}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}K_{VCO}\lp{\delta\omega_{sig}}\right.\\
        &+\left. \frac{2}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}K_{VCO}\lp{\delta\omega_{VCO}} \right)\\
        &+\frac{2s K_{VCO}}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}
    \end{aligned}
\end{equation}
Here we used relations $\lp{\delta\omega_{sig}} = s\lp{\phi_{sig}}$ and $\lp{\delta\omega_{VCO}} = s\lp{\phi_{VCO}}$. Therefore, in terms of Fourier spectrum, we get:
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &=  \left|K_A K_M+K_A \frac{M \fs{\delta v_{RF}}}{2}\right|^2\\
        &\left( \left|\frac{-2}{\iota 4 \pi f-K_{VCO} K_A (2K_M + M\fs{\delta v_{RF}})}K_{VCO}\fs{\delta\omega_{sig}}\right|^2\right.\\
        &+\left. \left|\frac{2}{\iota 4 \pi f-K_{VCO} K_A (2K_M + M\fs{\delta v_{RF}})}K_{VCO}\fs{\delta\omega_{VCO}}\right|^2 \right)\\
        &+\left|\frac{\iota 4 \pi f K_{VCO}}{\iota 4 \pi f-K_{VCO} K_A (2K_M + M\fs{\delta v_{RF}})} K_A \fs{v_A}\right|^2
    \end{aligned}
\end{equation}
In the approximation range of $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$, the noise contributions are:
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &= \left|\fs{\delta\omega_{sig}}\right|^2+ \left|\fs{\delta\omega_{VCO}}\right|^2+\left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_A}\right|^2
    \end{aligned}
\end{equation}
Writing the most general form by distinguishing different noise sources mentioned in Sec \ref{sec:AmpInNoise}, we get:
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &= \left|\fs{\delta\omega_{sig}}\right|^2+ \left|\fs{\delta\omega_{VCO}}\right|^2+\left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_{Mix}}\right|^2\\
        &+\left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_{AmpIn}}\right|^2 + \left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_{LPF}}\right|^2\\
    \end{aligned}
\end{equation}
Therefore, we see that even in the most general analysis, we get same contributions as we got in Sec \ref{sec:CFBN} with only minor differences. And since derivation there was much more easier to follow, it is sufficient to just use the results of Sec \ref{sec:NoiseAnalysis}.

%-------------------------------------------------%
\subsection{Heterodyne Signal Measurement}
In this case, the noise of the clock will be present in the signal as well. Then the output at the mixer after low pass filter would be:
\begin{equation}
    \lp{u_1} = \left(K_M + \frac{M \lp{\delta v_{RF}}}{2} \right) (\lp{\phi_{VCO}} -\lp{\phi_{sig}}-\lp{\delta\phi_{CLK}}) +  \lp{v_A}
\end{equation}
Here, $\ts{\delta v}{A}$ is total noise due to the sources mentioned in Sec (\ref{sec:AmpInNoise}). Then the output of the amplifier would be:
\begin{equation}{\label{eq:u2pVCOHONAHetD}}
    \lp{u_2} = K_A \left(K_M + \frac{M \lp{\delta v_{RF}}}{2} \right) (\lp{\phi_{VCO}} -\lp{\phi_{sig}}-\lp{\delta\phi_{CLK}}) +  K_A \lp{v_A}
\end{equation}
Then at the output of VCO, phase noise of VCO will get added:
\begin{equation}
    \begin{aligned}
    \lp{\phi_{VCO}} &= \left(\frac{K_{VCO}}{s}K_A K_M + K_A \frac{K_{VCO}M \lp{\delta v_{RF}}}{2 s} \right)\left(\lp{\phi_{VCO}} -\lp{\phi_{sig}}-\lp{\delta\phi_{CLK}}\right)\\
    &+ \frac{K_{VCO}}{s} K_A \lp{v_A}+ \lp{\delta\phi_{CLK}}
    \end{aligned}
\end{equation}
Solving for $\lp{\phi_{VCO}}$, we get:
\begin{equation}
    \begin{aligned}
    \lp{\phi_{VCO}} &= \frac{-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\phi_{sig}}\\
    &+ \frac{2K_{VCO}}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}\\
    &+ \lp{\delta\phi_{CLK}}
    \end{aligned}
\end{equation}
Then, our measurement signal $\lp{u_2}$ will be (using Eq(\ref{eq:u2pVCOHONAHetD})):
\begin{equation}
    \begin{aligned}
        \lp{u_2} &=  \left(K_A K_M+K_A \frac{M \lp{\delta v_{RF}}}{2}\right)\\
        &\left( \frac{-2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\phi_{sig}}\right.\\
        &+\left. \frac{2K_{VCO}}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A} \right)\\
        &+ K_A \lp{v_A}\\
        &=  \left(K_A K_M+K_A \frac{M \lp{\delta v_{RF}}}{2}\right)\\
        &\left( \frac{-2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}\lp{\phi_{sig}}\right)\\
        &+\frac{2s}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}
    \end{aligned}
\end{equation}
Note that contribution of phase noise of the clock got cancelled. Therefore, when measurement would be done as per Eq(\ref{eq:Meas}), the noise contributions would be:
\begin{equation}
    \begin{aligned}
        \lp{\delta\omega_{meas}} &= K_{VCO}\lp{u_2}\\
        &=  \left(K_A K_M+K_A \frac{M \lp{\delta v_{RF}}}{2}\right)\\
        &\left( \frac{-2}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})}K_{VCO}\lp{\delta\omega_{sig}}\right)\\
        &+\frac{2s K_{VCO}}{2s-K_{VCO} K_A (2K_M + M\lp{\delta v_{RF}})} K_A \lp{v_A}
    \end{aligned}
\end{equation}
Here we used relations $\lp{\delta\omega_{sig}} = s\lp{\phi_{sig}}$ and $\lp{\delta\omega_{VCO}} = s\lp{\phi_{VCO}}$. Therefore, in terms of Fourier spectrum, we get:
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &=  \left|K_A K_M+K_A \frac{M \fs{\delta v_{RF}}}{2}\right)\\
        &+\left|\frac{\iota 4 \pi f K_{VCO}}{\iota 4 \pi f-K_{VCO} K_A (2K_M + M\fs{\delta v_{RF}})} K_A \fs{v_A}\right|^2
    \end{aligned}
\end{equation}
In the approximation range of $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$, the noise contributions are:
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &= \left|\fs{\delta\omega_{sig}}\right|^2+\left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_A}\right|^2
    \end{aligned}
\end{equation}
Writing the most general form by distinguishing different noise sources mentioned in Sec (\ref{sec:AmpInNoise}), we get:
\begin{equation}
    \begin{aligned}
        \fs{\delta\omega_{meas}}^2 &= \left|\fs{\delta\omega_{sig}}\right|^2+\left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_{Mix}}\right|^2\\
        &+\left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_{AmpIn}}\right|^2 + \left|\frac{2 \pi f }{K_M (1 + \frac{\fs{\delta v_{RF}}}{V_{RF}})} \fs{v_{LPF}}\right|^2\\
    \end{aligned}
\end{equation}
Again, we see that even in the most general analysis, we get same contributions as we got in Sec (\ref{sec:CFHetD}) with only minor differences.

%##########################################################################%
\section{Probing PLL}
In practice, one would need to find measure the closed loop transfer function to determine the frequency range where the approximation $f \ll \frac{K_A K_M K_{VCO}}{2\pi}$ works and we can use simpler analytical forms. Also, it would help in determining if the PLL is configured correctly.\\
To do so, we need to probe the PLL when it is running in closed loop. One Way to do it is to send a source signal $\ts{u}{p}$ at port B in the amplifier (see fig (\ref{fig:PLLSch})) where the source would be added with a negative sign to port A and then amplified. The signal at output of amplifier would be:
\begin{equation}
    \lp{u_2} = K_A(\lp{u_1}-\lp{u_p})
\end{equation}
Then signal at VCO output would be:
\begin{equation}
    \lp{\phi_{VCO}} = \frac{K_{VCO}}{s}K_A(\lp{u_1}-\lp{u_p}) + \lp{\delta\phi_{VCO}}
\end{equation}
Then output of mixer and low pass filter would be:
\begin{equation}
    \lp{u_1} = K_M \frac{K_{VCO}}{s}K_A(\lp{u_1}-\lp{u_p}) + K_M\lp{\delta\phi_{VCO}} -K_M\lp{\phi_{sig}} + \lp{\delta v_A}
\end{equation}
Let $K_M \frac{K_{VCO}}{s}K_A=G_{OL}(s)$ be the open loop transfer function. Solving for $\lp{u_1}$:
\begin{equation}
    \lp{u_1} = -\frac{G_{OL}(s)}{1-G_{OL}(s)}\lp{u_p} + \frac{K_M}{1-G_{OL}(s)}\lp{\delta\phi_{VCO}} - \frac{K_M}{1-G_{OL}(s)}\lp{\phi_{sig}} + \frac{1}{1-G_{OL}(s)}\lp{\delta v_A}
\end{equation}
Therefore, for probe signal strong enough that $\frac{K_M\lp{\delta\phi_{VCO}}}{\lp{u_p}} \ll 1$, $\frac{K_M\lp{\phi_{sig}}}{\lp{u_p}} \ll 1$ and $\frac{\lp{\delta v_{A}}}{\lp{u_p}} \ll 1$, one can get closed loop transfer function by measuring:
\begin{equation}
    G_{CL}(s) = \frac{\lp{u_1}}{\lp{u_p}} \approx -\frac{G_{OL}(s)}{1-G_{OL}(s)}
\end{equation}
which can be unwrapped to get the open loop transfer function as well:
\begin{equation}
    G_{OL}(s) = -\frac{G_{CL}(s)}{1-G_{CL}(s)}
\end{equation}
The above derivation used the instance of Beat Note measurement but the result will be same for the heterodyne measurement case as well with only change being the cancelling of $\lp{\delta\phi_{VCO}}$.\\
The measured closed loop and open loop transfer function should look like fig (\ref{fig:OLCLTF}).
\begin{figure}[!h]
\includegraphics[width=\textwidth,keepaspectratio=true]{PLLOLCLTF.pdf}
\caption{Closed Loop and Open Loop Transfer Functions}
\label{fig:OLCLTF}
\end{figure}
The simple complete solutions in Sec \ref{sec:CF} are valid for $f \ll f_c$ where $f_c = \frac{K_AK_MK_{VCO}}{2\pi}$.
%-------------------------------------------------%
%\newpage
%\section{Conclusion}

%-------------------------------------------------%
%\newpage

\begin{thebibliography}{References}

    \bibitem{MCPD} MiniCircuits FAQ on phase detectors.
    \href{https://www.minicircuits.com/app/AN41-001.pdf}{https://www.minicircuits.com/app/AN41-001.pdf}
    %Use \cite{Abcd} to cite this paper in slides.

\end{thebibliography}


\end{document}
