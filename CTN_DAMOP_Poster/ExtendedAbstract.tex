\newcommand{\comment}[1]{}
\newcommand{\subparagraph}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out
                                                          % if you need a4paper
%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                                                          % paper

\IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphics} % for pdf, bitmapped graphics files
\usepackage{float}
\usepackage{graphicx}
\graphicspath{{figures/}}
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed
\usepackage{amstext}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    citecolor=Violet,
    filecolor=magenta,
    urlcolor=blue,
}
\usepackage{lettrine}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{titlesec}
%Format
%\titlespacing{command}{left spacing}{before spacing}{after spacing}[right]
\titlespacing\section{0pt}{9pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\titlespacing\subsection{0pt}{6pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\titlespacing\subsubsection{0pt}{6pt plus 4pt minus 2pt}{pt plus 2pt minus 2pt}

\voffset = -0.3 in
\hoffset = -0.5 in
\textheight = 9.7 in
\textwidth = 8 in
\parskip = 0 in

\title{\Huge
Measuring Coatings Brownian Noise for LIGO Mirrors
}
\author{\underline{Anchal Gupta}$^{1}$, {Andrew Wade}$^{1}$, {Craig Cahillane}$^{1}$, and {Rana Adhikari}$^{1}$% <-this % stops a space
\thanks{$^{1}$LIGO California Institute of Technology.  Email: \href{mailto:anchal@caltech.edu}{anchal@caltech.edu}. Log \#DAMOP20-2020-020003}}


\begin{document}
\headsep = 0 in


\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
The Laser Interferometer Gravitational-Wave Observatory (LIGO) uses precision
laser interferometry to probe minute gravitational wave signals originating
from distant astrophysical events. LIGO uses dielectric coatings on the large
test mass mirrors to achieve low loss, high reflectivity
($\mathbf{>99.9994\%}$) and cancellation of thermo-optic noise.
This enables the measurement of displacement due to gravitational waves (GW) to
the order of $\mathbf{10^{-19}\text{m}/\sqrt{\text{Hz}}}$.
Present noise floor limitations in Advanced LIGO, in part, come from the mirror
coatings' Brownian noise. This experiment directly measures this
noise due to crystalline coatings made with alternate layers of
$\text{Al}_{0.92}\text{Ga}_{0.08}\text{As}$ and GaAs. The noise is measured
by beating light from two identical high finesse cavities, locked via
high-performance feedback with two independent lasers, and measuring the
fluctuations in the beat note frequency. This approach complements the
indirect measurement methods and helps in developing the theory of the origin
of this noise. This work contributes to the ongoing efforts on increasing the
sensitivity for future GW detectors as well as other high precision optical
experiments. We present our latest estimate of this noise contribution and
details of the experiment.
\end{abstract}
%\vspace{3pt}
\begin{keywords}
Precision measurement, Febry-Perot Cavities, Frequency Stabilization,
Controls and Feedback.
\end{keywords}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\textbf{\textsc{Introduction}}}
The Advanced LIGO has been detecting gravitational wave signals originating
from merging binary black holes and binary neutron stars
\cite{PhysRevX.9.031040}. The astounding $10^{-19}\text{m}/\sqrt{\mathrm{Hz}}$
displacement sensitivity is achieved with 4-km arm length dual-recycled
Michelson interferometers with Fabry-Perot cavities
\cite{PhysRevLett.116.131103}. To keep high circulating power and low loss in
the arms, the test mass mirrors are coated with alternate quarter wavelength
layers of high and low refractive indices, resulting in high reflectivity
($>99.9994\%$). Further, optimization on individual layer thicknesses is done
to cancel thermo-optic noise.

The current noise floor of these instruments is, in part, dominated by
coatings Brownian thermal noise. Using the fluctuation-dissipation theorem,
one can connect the level of this noise with the mechanical loss angles of
each coating layer \cite{PhysRevD.87.082001}. These loss angles are related
to energy dissipation when coatings are applied with periodic force.

Internal friction is considered as one of the main contributors to this
dissipation mechanism and is supposed to be reduced in high Q crystalline
coatings. Alternative crystalline coating design with
$\text{Al}_{0.92}\text{Ga}_{0.08}\text{As}$ and GaAs layers were developed
keeping similar thermo-optic cancellation and reflectivity \cite{TaraTO}.

We directly measure this noise contribution in this experiment in order to
better understand if the present model of the Brownian noise conforms well
with the observed noise.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\textbf{\textsc{Experimental Setup}}}
The mirrors-under-test are optically bonded to two $1.45"$ fused silica spacer
to form two identical high finesse cavities. The cavities are kept in a
temperature-stabilized vacuum can. These are probed with two independent NPRO
lasers locked to the cavities through high-performance frequency stabilization
servo (FSS). The displacement noise of the mirrors gets converted into the
frequency noise of the transmitted light from the cavities. The heterodyne
beat note frequency between these transmitted lights thus consists of two
times the coating Brownian noise contribution from each mirror. The signal is
then sent to a digital phase-locked loop (DPLL) to measure the frequency noise.
\begin{figure}[h]
\includegraphics[width=\linewidth,keepaspectratio=true]{ctn_schematic.pdf}
\caption{Schematic of the experiment. Waveplates are omitted.
         Temperature stabilization not shown.}
\end{figure}

Further control loops include spatial mode cleaning (PMC), temperature
stabilization, and intensity stabilization (ISS). There are future plans to
reduce the residual RF amplitude modulation from the phase modulators in PDH
loops by feeding back into DC offset and temperature of the phase modulators.

The cavities are made as short as possible within the practical constraints to
reduce beam spot size on the mirrors \cite{TaraThesis}. This increases the
effect of coating Brownian noise which is inversely proportional to the beam
spot area. The cavities are supported at optimum points to reduce
low-frequency seismic noise. These measures make the coating Brownian noise,
the dominant noise source in the frequency range of $100$ $\mathrm{Hz}$ to
$1$ $\mathrm{kHz}$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\textbf{\textsc{Conclusion}}}
We present our latest measured estimates for coatings Brownian noise for
$\text{Al}_{0.92}\text{Ga}_{0.08}\text{As}$-GaAs coated mirrors. Using
Bayesian analysis with the noise model \cite{PhysRevD.87.082001}, we infer
mechanical loss angles for these coatings which can be compared with results
of indirect mechanical mode ring-down measurement methods. The results will
help in increasing sensitivity for future GW detectors and other high
precision optical experiments.

\addtolength{\textheight}{-12cm}   % This command serves to balance the column lengths
                                  % on the last page of the document manually. It shortens
                                  % the textheight of the last page by a suitable amount.
                                  % This command does not take effect until the next page
                                  % so it should come on the page before the last. Make
                                  % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section*{ACKNOWLEDGMENT}

%The preferred spelling of the word �acknowledgment� in America is without an �e� after the �g�. Avoid the stilted expression, �One of us (R. B. G.) thanks . . .�  Instead, try �R. B. G. thanks�. Put sponsor acknowledgments in the unnumbered footnote on the first page.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\bibliographystyle{IEEEtran}
%\bibliography{reference}
\begin{thebibliography}{}
\bibitem{PhysRevX.9.031040}
B.P. Abbot et al. (LIGO Scientific and Virgo Collaboration) \href{https://link.aps.org/doi/10.1103/PhysRevX.9.031040}{Phys. Rev. X 9, 031040 (2016).}
\bibitem{PhysRevLett.116.131103}
B.P. Abbot et al. (LIGO Scientific and Virgo Collaboration) \href{https://link.aps.org/doi/10.1103/PhysRevLett.116.131103}{Phys. Rev. Lett. 119, 161101 (2017).}
\bibitem{PhysRevD.87.082001}
T.   Hong,   H.   Yang,   E.   K.   Gustafson,   R.   X.   Adhikari,   and   Y.   Chen \href{https://link.aps.org/doi/10.1103/PhysRevD.87.082001}{Phys. Rev. D 87, 082001 (2013).}
\bibitem{TaraTO}
T. Chalermsongsak et al. \href{https://doi.org/10.1088/0026-1394/53/2/860}{Metrologia, 53, 860 (2016)}
%\bibitem{Cole}
%Cole, G., Zhang, W., Martin, M. et al. \href{https://doi.org/10.1038/nphoton.2013.174}{Nature Photon 7, 644–650 (2013).}
%\bibitem{FSS}
%Customized version of $3^{rd}$ TTFSS \href{https://dcc.ligo.org/LIGO-E1700065/public}{LIGO-E1700065.}
\bibitem{TaraThesis}
T. Chalermsongsak Thesis \href{https://doi.org/10.7907/7202-WT21}{(Dissertation (Ph.D.)) (2014)}
\end{thebibliography}
\end{document}

\comment{
\subsection{Cavities and lasers}
The mirrors-under-test are optically bonded to two $1.45"$ fused silica spacer to form two identical high finesse cavities. Two independent Non-planar Ring Oscillator (NPRO) 1064 nm Lightwave 126 lasers are used to probe these cavities.

\subsection{Spatial Mode Cleaning}
The lasers go through a metal-spaced triangular cavity with far-end mirror controlled with a Piezoelectric Transducer (PZT). The cavity is locked to the laser frequency allowing only the Gaussian mode to go through.

\subsection{Frequency Stabilization Servo}
The displacement noise of the mirrors gets converted into frequency noise of the transmitted light due to high-performance frequency stabilization servo keeping the lasers locked at resonance \cite{FSS}. This feedback system employs PDH with three actuators viz; PZT and Thermoelectric Cooler on the NPRO crystal, and a broadband Electro-Optic Modulator in the path. The heterodyne beat note frequency between the transmitted light thus contains two times coatings Brownian noise due to each mirror.

\subsection{Temperature Stabilization}
The spacers of the cavities are wrapped with Nichrome wire to individually change temperature, and hence, length of the cavities at longer time scales, to control heterodyne frequency. These cavities sit inside a temperature-controlled vacuum can to see a uniform and constant temperature environment. These feedback loops use custom built $mK$ sensitive temperature sensors and are controlled with PID python scripts running on a computer.

\subsection{Intensity Stabilization Servo}
Pick-offs from the transmitted light from the cavities are used to feedback into an Electro-Optic Amplitude Modulator to stabilize the beat note frequency amplitude.

\subsection{Phase Locked Loop}
The variations in the heterodyne beat note frequency between the transmitted light from the two cavities are measured through a digital phase locked loop within Liquid Instruments' Moku:Lab. Moku itself is stabilized with a SRS FS725 rubidium clock.
}
