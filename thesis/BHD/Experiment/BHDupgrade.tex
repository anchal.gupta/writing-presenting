\section{The BHD upgrade}
\label{sec:BHDupgrade}
\input{BHD/figureTex/40mBHDOpticalLayout}

In November 2021, the CIT 40m was vented for a large-scale upgrade to its
optical layout to add a BHD readout port. Fig.~\ref{fig:40mBHDOpticalLayout}
shows the final optical layout of CIT 40m after the upgrade was completed. This
is only the first phase of the upgrade with in-vacuum Output Mode Cleaners due
to be tested in phase II.

\subsection{PR2 and PR3}
The power recycling cavity at 40m used to have only the \gls{PRM} equipped with
OSEM sensors and coil actuators to control the position and alignment. PR2 and
PR3 were suspended with small towers to passively isolate them from seismic
noise. Further PR2 was a highly reflective mirror with a wrong radius of
curvature which caused issues with power recycling cavity stability (See
Appendix D.2 in Gautam Venugopalan's thesis\cite{Gautam2021}). Since BHD
requires a local oscillator beam extracted before the laser enters the
Michelson interferometer, it was decided to replace PR2 with a more
transmissive mirror ($\sim$2\%) mirror and place it in a small suspension tower
(small with respect to the LIGO site's large suspension towers) with OSEM
sensors and actuators. PR3 was upgraded into the sensed and controllable
suspension stage as well giving us full steering control to the input of the
interferometer. See appendix \ref{app:suspendingOptics} for details on how each
suspension was prepared.

\subsection{SR2 and removing SR3}
For the BHD upgrade, it was decided to convert the Resonant Sideband Extraction
(RSE) configuration of the signal recycling cavity at 40m to a Signal Recycling
(SR) configuration. This was done to enable possible experimentation and
measurement of ponderomotive squeezing (see Chapter 4 of Gautam Venugopalan's
thesis\cite{Gautam2021}). This required removing SR3 and placing single folding
mirror SR2 where SR3 used to be. SR2 steers the light from the antisymmetric
port of the beamsplitter towards the \gls{ITMY} chamber.

\subsection{LO1, LO2, AS1, and AS4}
The transmitted power from PR2 is steered for the local oscillator beam by
another suspended optic right next to \gls{ITMX}, called LO1. This optic
redirects the \gls{LO} beam to another new suspension LO2 on the beam splitter
chamber that steers the beam toward the BHD setup in the \gls{ITMY} chamber.
Here there are two fixed steering mirrors, LO3 and LO4, which direct the
\gls{LO} beam toward the BHD beam splitter (BHDBS). LO3 and LO4 are curved
mirrors to perform mode matching with the \gls{AS} beam at \gls{BHDBS}. The
\gls{AS} beam is collected from
\gls{SRM} by the new suspended optic AS1. The \gls{AS} beam goes through a
fixed beam splitter AS2 where 10\% of light is allowed to go through while the
remaining goes towards another fixed optic AS3 on the \gls{ITMY} chamber. AS3
redirects the beam toward the new suspended optic AS4. Both AS3 and AS4 are
curved mirrors to allow for mode matching with the \gls{LO} beam at the
\gls{BHDBS}. The 10\% transmitted AS beam from AS2 is sent to the RF photodiode
AS55 for keeping the option of using the RF readout method (see
Sec.\ref{subsec:RFreadout}) as well.

\subsection{Suspension controls and diagnostics}
While the suspensions greatly reduce the seismic noise at frequencies above the
pendulum resonance of the suspension, the high Q of the pendulum resonances
means that the suspensions can be easily excited at around 1 Hz through seismic
or terrestrial noise. To avoid prolonged ringing of the suspensions at these
resonances, the pendulum resonances are damped actively around the 1 Hz
frequency region. The new suspensions required a new set of sensor and control
electronics to be installed.

\input{BHD/figureTex/suspensionDamping}

Fig.~\ref{fig:suspensionDamping} shows the suspension control loop implemented
for each suspension. The OSEM sensor signals are read in a satellite amplifier
which converts the OSEM photodiode current to voltage. This signal is low
passed by anti-aliasing filters at 8 kHz and then read by ADC at a 16 kHz
sampling rate. In the digital domain, we take linear combinations of the 5 OSEM
sensors to create 4 physical degrees of freedom of motion sensed by them,
namely, Position, Pitch, Yaw, and Side motion. And we get a fifth orthogonal
combination that represents unphysical motion for a rigid body, which we call
the null stream or butterfly vector. To identify the correct linear
combinations (the input matrix) required to take the information from OSEM
basis to the physical DOF basis, we do a free swing test. During this test, the
actuators to the optics are shutoff (keeping any DC actuation still on to
ensure the measurement is made near the nominal position of the optic), and
then the optic is kicked by giving two 1 second long pulses on one of the face
coils. Since the resonances of the optic are all near 1 Hz, these 1 sec-long
pulses excite all resonances in the suspended optic. We take the OSEM data
during this free swing and look at the power spectrum. We identify the
resonance peaks associated with each degree of freedom based on the closeness
to theoretical resonance values and create an input matrix that separates the
resonances in individual outputs.

The separated signals for physical degrees of freedom are then amplified and
low passed through the feedback filters. Coil actuation signals are created
through these filtered signals using another output matrix which uses the coils
to actuate on particular physical degrees of freedom. The coil outputs are sent
through DAC to an anti-imaging filter (8 kHz low pass filter), and then to the
coil driver circuits which drive current through the OSEM coils to actuate on
the magnets.

\subsection{Photodiodes}
Four new photodiodes were installed for this upgrade. Two of the photodiodes
are meant to read the BHD signal at DC. These are Laser Components IG17X3000G1i
receiving most of the BHD output light. Pick-off light from the two outputs
paths of the \gls{BHDBS} is sent to two custom-built RF photodiodes, resonant
at 44 MHz and 55 MHz. These were named BH44 and BH55 respectively. Each
resonant RF photodiode output is demodulated in both quadratures and read
through DAC for sensing and control purposes. Part of the pick-of light is also
sent to a CCD camera (not shown in the figure) to help in aligning the \gls{LO}
and \gls{AS} beams to overlap with each other.
