\section{Preliminary results with BHD}

We have been able to use the Balanced Homodyne Readout at CIT 40m using the
\gls{LO} phase locking mentioned in the last section. As a preliminary test, we
locked the interferometer in two configurations, with RF readout, and with BHD
readout, and compared the noise for the two cases.

\subsection{Michelson Interferometer configuration}

\input{BHD/figureTex/MICHBHDNB}

The interferometer was first locked in \gls{MICH} configuration by using the
usual RF readout method at the \gls{AS} port. This uses the AS55Q channel to
feedback to \gls{BS} keeping the Michelson differential length changes locked
to a point. The green traces in Fig.~\ref{fig:MICHBHDNB} show the total noise
and the dark noise limit for this configuration. Then the \gls{LO} phase angle
was locked using the two methods: single RF demodulation using BH55 and dual RF
demodulation using BH44. We can see in Fig.~\ref{fig:MICHBHDNB} that the dark
noise floor for the BHD readout is considerably low in comparison to the RF
readout method. However, this is because, in our current optical layout, only
10\% of the \gls{AS} beam reaches the AS55 RF photodiode. So the sensitivity of
the RF readout scheme is reduced by 10 times than what would be when all of the
\gls{AS} beam goes to it. Even then, the sensitivity gain due to the bright
\gls{LO} beam is evident as the noise floor for the BHD scheme is less than
more than an order of magnitude.

In this measurement, the noise of the BHD scheme is limited by the \gls{LO}
phase control noise from 20 Hz to 200 Hz. The noise bump at 50 Hz was an
artifact in the \gls{LO} phase control noise that we have successfully removed
in the later attempts as can be seen in the noise curves presented in
Sec.\ref{sec:LOphaseLocking} but we have not measured the \gls{MICH}
sensitivity with the better \gls{LO} phase control yet.

We also see that the BHD scheme worked better with BH44 controlled \gls{LO}
phase. This is expected in the ideal case as BH44 locks the \gls{LO} phase to
the correct value. The fact that we get any sensitivity with the BH55 \gls{LO}
phase lock is already perplexing. We think that remnant differential arm offset
or contrast defect light at \gls{AS} port significantly changes the \gls{LO}
phase angle lock point with BH55 such that it gains \gls{MICH} sensitivity. It
needs to be investigated further how exactly we gained this sensitivity and if
we can take advantage of this to sense and reduce differential length offset or
contrast defect.

\subsection{Fabry-P\'erot Michelson Interferometer configuration}

\input{BHD/figureTex/FPMIBHDNB}

The interferometer was first locked in
\gls{FPMI} configuration using the usual RF readout method. This involves
locking the \gls{CARM} degree of freedom with REFL55I signal, the \gls{MICH}
with REFL55Q, and \gls{DARM} with AS55Q. See Sec.\ref{subsec:FPMIlocking} for a
more detailed description of achieving the lock in \gls{FPMI} configuration
using the RF readout. It is important to lock the interferometer before we can
attempt using BHD readout because the \gls{LO} phase angle fluctuates through
multiple cycles of $2\pi$ radians in the absence of control of the \gls{DARM}
degree of freedom. None of our current \gls{DARM} phase locking methods would
work in locking the \gls{DARM} phase angle with such a large dynamic range as
all methods rely on a linear region of error signal production.

Once \gls{DARM} is locked with AS55Q, we locked the \gls{DARM} phase angle
using BH55 with the single RF demodulation method (see
Sec.\ref{subsec:SingleRFDemod}) with a \gls{UGF} of 50 Hz. Then we shift the
error signal for the \gls{DARM} loop from AS55Q to the BHD port which is the
difference between the two DC photodiodes at the output of the \gls{BHDBS}.
Fig.~\ref{fig:FPMIBHDNB} shows the noise of \gls{DARM} degree of freedom when
\gls{FPMI} is locked with RF readout or with BHD.

Note that without many optimizations, we already see lower noise when BHD
readout is used. We expect to see more noise reduction soon with the CIT 40m
BHD upgrade team working on achieving the \gls{LO} phase lock at the true
optimum \gls{DARM} phase angle of $\phi_{\mathrm{LO}}$. Also, \gls{FPMI}
configuration is not the best one to see the benefit of BHD since the
\gls{DARM} beam is attenuated in transmission through the misaligned \gls{PRM}.
The current goal of the team is to achieve the BHD readout in \gls{PRMI}
configuration where the \gls{LO} beam strength is amplified by the power
recycling gain, significantly increasing the sensitivity of the BHD readout.
The overall goal is to reach \gls{PRFPMI} locking configuration to see
improvement in \gls{DARM} sensitivity for the full length of the
interferometer.
