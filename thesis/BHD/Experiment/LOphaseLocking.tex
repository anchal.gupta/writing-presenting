\section{Local oscillator phase locking}
\label{sec:LOphaseLocking}

As we saw in Eq.~\ref{eq:BHD}, the \gls{LO} phase angle sets the sensitivity of
BHD and it is a free parameter since the path length of \gls{LO} beam can move
independently of the carrier field in the interferometer. It is thus important
to control this phase angle and keep it locked to the desired point, which is
$\pi/2$ for conventional use of BHD. We looked into the following methods that
can be used to lock the \gls{LO} phase angle.

\subsection{Single RF demodulation}
\label{subsec:SingleRFDemod}

As we saw in Sec.\ref{subsec:RFreadout}, the phase modulation sidebands of the
input field are present in the \gls{AS} port. If we pick off a part of one of
the output beams of the \gls{BHDBS}, we get an overlapped field between the
\gls{AS} beam with sidebands and the \gls{LO} beam. This overlapped beam of
\gls{AS} sidebands and the \gls{LO} beam would be available even if the output
of \gls{BHDBS} is sent through output mode cleaners, in the reflected field
from them. We picked off this beam in our experiment at CIT 40m and sent it to
resonant RFPD, named BH55. This photodiode is resonant at 55 MHz. Let's look at
the power on this photodiode that oscillates at the sideband frequency:

\begin{equation}
\label{eq:BH55}
\begin{aligned}
P_{BH55} &= \left(-i E_{\mathrm{LO}}E_{\mathrm{AS}}^*
                  +i E_{\mathrm{LO}}^* E_{\mathrm{AS}}
            \right)\\
         &= 2 \mathrm{Re}
           [
           i E_{\mathrm{LO}}^* E_{\mathrm{AS}}
           ]\\
         &= 2 \mathrm{Re}
           [
           i E_{\mathrm{c}} e^{i \phi_{\mathrm{LO}}}
           \left(
             - i E_{\mathrm{s}} e^{i\Omega t - i 2k_+ L_{\mathrm{C}}}
                 sin(2k_+ L_{\mathrm{D}})
             - i E_{\mathrm{s}} e^{-i\Omega t - i 2k_- L_{\mathrm{C}}}
                 sin(2k_- L_{\mathrm{D}})
           \right)
           ]\\
         &= 2 E_{\mathrm{c}} E_{\mathrm{s}} sin(2 \delta k L_{\mathrm{D}})
            \mathrm{Re}
            [e^{i (\Omega t - 2 \delta k L_{\mathrm{C}} + \phi_{\mathrm{LO}})}
             -
             e^{i (-\Omega t + 2 \delta k L_{\mathrm{C}} + \phi_{\mathrm{LO}})}
            ]
\end{aligned}
\end{equation}

In the last step above, we use operation conditions where the laser is locked
to the \gls{CARM} mode, $2 k L_{\mathrm{C}} = 2 n \pi$ where $n \in
\mathbb{Z}$, thus $e^{-i 2 k_{\pm} L_{\mathrm{C}}} = e^{\pm i 2 \delta k
L_{\mathrm{C}}}$. Similarly, at dark point operation $k L_{\mathrm{D}} = m \pi$
where $m \in \mathbb{Z}$, thus $sin(2k_{\pm} L_{\mathrm{D}}) = sin(\pm 2 \delta
k L_{\mathrm{D}})$. On demodulation at $\Omega$ with demodulation phase angle
of $- 2 \delta k L_{\mathrm{C}}$, we get:

\begin{equation}
\label{eq:BH55IQ}
\begin{aligned}
P_{BH55I} &= 2 E_{\mathrm{c}} E_{\mathrm{s}} sin(2 \delta k L_{\mathrm{D}})
             \mathrm{Re}
             [e^{i \phi_{\mathrm{LO}}} - e^{i \phi_{\mathrm{LO}}}
             ] = 0\\
P_{BH55Q} &= 2 E_{\mathrm{c}} E_{\mathrm{s}} sin(2 \delta k L_{\mathrm{D}})
             \mathrm{Re}
             [e^{i (\pi / 2 + \phi_{\mathrm{LO}})}
             - e^{i (-\pi / 2 + \phi_{\mathrm{LO}})}
             ]\\
          &= - 4 E_{\mathrm{c}} E_{\mathrm{s}} sin(2 \delta k L_{\mathrm{D}})
             sin(\phi_{\mathrm{LO}})
\end{aligned}
\end{equation}

So, we do have a signal proportional to $\phi_{\mathrm{LO}}$ in one quadrature.
But there is a catch here. If this signal is used for locking the \gls{LO}
phase angle, it will lock to $\phi_{\mathrm{LO}} = 0$, but as we saw in
Sec.\ref{eq:BHD}, we want to lock it to $\pi/2$ otherwise the BHD signal would
be zero. So this method cannot be used to lock the \gls{LO} phase angle. We
need the derivative of this signal, so that becomes a cosine function in
$\phi_{\mathrm{LO}}$ giving us a zero crossing at $\pi/2$.

\input{BHD/figureTex/BH55signal}

Fig.~\ref{fig:BH55signal} shows how BH55 error signal has zero crossing at
$\phi_{\mathrm{LO}} = 0$ through phasor diagram. In practice though, we found
that the BHD response of the 40m prototype was non-zero when we locked the
\gls{LO} phase angle with BH55. This probably happens because of a contrast
defect in the interferometer. Leakage light at \gls{AS} port due to contrast
defect does not suffer through $\pi/2$ phase shift and since the \gls{LO} beam
also has 55 MHz phase sidebands on it, this leakage \gls{AS} beam can beat with
the \gls{LO} sidebands at 55 MHz to give a zero-crossing at non-zero
$\phi_{\mathrm{LO}}$.

\subsection{RF + Audio dither dual demodulation}

Since we need the derivative of the BH55 signal, we can dither the \gls{LO}
phase angle by either shaking one of the suspended mirrors in \gls{AS} beam
path, AS1 or AS4, or by shaking one of the suspended mirrors in \gls{LO} beam
path, LO1 or LO2. If the length of \gls{LO} path (with respect to \gls{AS} beam
path) is modulated at angular frequency $\Omega_a$, then the \gls{LO} beam
would get audio phase modulation sidebands on it:

\begin{equation}
E_{\mathrm{LO}, dithered} = E_{\mathrm{c}}e^{-i \phi_{LO}}
                            + i E_{\mathrm{s,a}}
                            \left(
                            e^{-i \phi_{LO} + i \Omega_a t}
                            + e^{-i \phi_{LO} - i \Omega_a t}
                            \right)
\end{equation}

This would affect the Q quadrature of BH55 (using Eq.~\ref{eq:BH55IQ}) as (only
listing terms that are varying at $\Omega_a$):

\begin{equation}
\label{eq:BH55Qdithered}
\begin{aligned}
P_{BH55Q, dithered} &= 2 E_{\mathrm{s,a}} E_{\mathrm{s}}
                       sin(2 \delta k L_{\mathrm{D}})
                       \mathrm{Re}
                       [i e^{i (\pi / 2 + \phi_{\mathrm{LO}} + \Omega_a t)}
                       - i e^{i (-\pi / 2 + \phi_{\mathrm{LO}} - \Omega_a t)}
                       ]\\
\end{aligned}
\end{equation}

We digitally demodulate the BH55Q signal at the audio angular frequency
$\Omega_a$, and get:

\begin{equation}
\label{eq:BH55Q_IQ}
\begin{aligned}
P_{BH55Q, I} &= -4 E_{\mathrm{s,a}} E_{\mathrm{s}}
                sin(2 \delta k L_{\mathrm{D}})
                cos(\phi_{\mathrm{LO}})\\
P_{BH55Q, Q} &= 0
\end{aligned}
\end{equation}

Thus the BH55\_Q\_I signal can be used to lock the \gls{LO} phase angle, and
with this signal the locking point would be $\phi_{\mathrm{LO}} = \pi/2$ just
like we want.

We tried this method at CIT 40m with partial success. The lock was not very
robust, because the noise at audio frequencies is still very high and the
length modulation of the beams cause jitter in the angular alignment of
\gls{LO} beam with \gls{AS} beam on \gls{BHDBS}. Further, the \gls{UGF} of the
\gls{LO} phase lock loop is limited by the audio dither frequency, so to get
higher
bandwidth, the audio dither frequency needs to be higher. But since the optics
are suspended, actuation at higher frequencies is suppressed as $f^{-2}$
limiting the loop gain that can be obtained.

\subsection{Dual RF demodulation}

\input{BHD/figureTex/BH44signal}

Since we pick off \gls{LO} beam from the transmission of PR2, the \gls{LO} beam
also has phase modulations on it as were applied to the carrier beam. At CIT
40m, two sets of phase modulations are applied on the input carrier beam, one
at 11 MHz, and one at 55 MHz. Just like how the audio phase modulation
sidebands get the derivative of the BH55 signal, the 11 MHz sidebands also get
the derivative of the BH55 signal, at 44 MHz and 66 MHz. We chose to pick off
another beam from another output of \gls{BHDBS} and sent it to another resonant
RFPD BH44 which is resonant at 44 MHz. This way, we do not need to demodulate
twice, although we are losing half of the signal in the 66 MHz frequency.
Fig.~\ref{fig:BH44signal} shows the error signal zero crossing for this scheme.
Note that any contrast defect or \gls{DARM} offset generating carrier light at
\gls{AS} port does not affect the error signal in this scheme as it beats two
RF sidebands with each other.

\subsection{LO Phase control testing}
We tested the \gls{LO} phase control loop in three different configurations. In
each configuration, the \gls{UGF} was set to 50 Hz to have a fair comparison
between two methods of locking: single RF demodulation with BH55 and dual RF
demodulation with BH44.

\input{BHD/figureTex/LOPhaseOLTF_SB} \input{BHD/figureTex/LOPhaseNB_SB}

\subsubsection{Single bounce off \gls{ITMX}, Mach-Zehnder:} First configuration
was Mach-Zehnder between \gls{AS} beam and \gls{LO} beam. \gls{ITMY},
\gls{ETMY}, and \gls{ETMX} are misalgined so that there is no michelson
interferometer and \gls{AS} beam is a simple single bounce off of \gls{ITMY}.
Fig.~\ref{fig:LOPhaseOLTF_SB} shows the \gls{OLTF} for the two locking methods.
We see that with the same feedback filters, we get the same response while
using the different sensors. The \gls{OLTF}s are fitted with the model of the
feedback loop. The fitted \gls{OLTF} are used to compute unsuppressed noise in
the loops and shown in the noise budget plot in Fig.~\ref{fig:LOPhaseNB_SB} for
this configuration. The unsuppressed noise curves in this configuration set the
equality of the two sensors in terms of noise performance because, for a simple
Mach-Zehnder configuration, the two sensors are optically equivalent and do not
depend on fine-tuning of any other parameter for them to work. The suppressed
noise performance is also almost the same: BH55 controlled \gls{LO} phase has a
residual \gls{RMS} of 0.04 radians when averaged over 1 second in comparison to
BH44 controlled \gls{LO} phase residual noise of 0.06 radians.

\subsubsection{Michelson Interferometer:}
\input{BHD/figureTex/LOPhaseOLTF_MICH} \input{BHD/figureTex/LOPhaseNB_MICH}
Second configuration we tested was Michelson interferometer between \gls{BS},
\gls{ITMX}, and \gls{ITMY} locked with the RF readout method (see
Sec.\ref{subsec:RFreadout}). Fig.~\ref{fig:LOPhaseOLTF_MICH} shows the
\gls{OLTF} for the two locking methods. Again, achieving almost the same
\gls{OLTF} in the two configurations was possible with \gls{UGF} set to 50 Hz.
Fig.~\ref{fig:LOPhaseNB_MICH} shows the noise budget for this locking
configuration for the two methods. Note that the noise floor in this
configuration increased by a factor of about 10 in comparison to the
Mach-Zehnder case in the previous test (see Fig.~\ref{fig:LOPhaseNB_SB}). This
is because the optical gain for both sensors has reduced due to the weaker 55
MHz sideband present at the \gls{AS} port as Michelson is locked to the dark
fringe. In this configuration, the BH55 loop suppressed noise twice as much as
BH44, achieving \gls{LO} phase residual \gls{RMS} noise of 0.04 radians in
comparison to BH44 controlled \gls{LO} phase residual \gls{RMS} noise of 0.08
radians when averaged over 1 second.

\begin{comment} The source of extra noise can be traced back to more noise in
the sensed signal itself as the unsuppressed noise for BH44 is twice as much as
BH55. This can be because there are two sources of 44 MHz signal describing the
same \gls{LO} phase angle. One from the beat between 11 MHz sidebands of
\gls{AS} beam and 55 MHz sidebands of \gls{LO} beam, and vice versa. Since the
accumulated phase between the two sidebands would be slightly different, this
can create a mixing of two error signals with different phase delays, thus a
single quadrature of BH44 photodiode output could be in a position where it
reads one error signal source maximally while facing noise due to the other
error signal source. This would need to be tested further to confirm. And we
would need to figure out a way to tune the demodulation phase for BH44 such
that the two error signals constructively contribute to the feedback.
\end{comment}

\subsubsection{Fabry-P\'erot Michelson Interferometer:}
\input{BHD/figureTex/LOPhaseNB_FPMI}

The third configuration we tested was Michelson interferometer with
Fabry-P\'erot arm cavities. We tested the two methods while keeping the similar
\gls{OLTF} with \gls{UGF} set to 50 Hz. Fig.~\ref{fig:LOPhaseNB_MICH} shows the
noise budget for the two locking configurations. We see that the BH44 \gls{LO}
phase lock is considerably more noisy above 20 Hz in comparison to the BH55
\gls{LO} phase lock. Even though the two methods achieve the same residual
\gls{RMS} noise of 0.04 radians, the higher noise density above 20 Hz did not
allow us to lock \gls{FPMI} with BHD readout. While the two methods seemed
comparable in the single bounce test, the fact that BH55 became better in
\gls{MICH} and \gls{FPMI} configurations suggests that BH55 gains extra
sensitivity with the presence of differential length offset in Michelson
interferometer and contrast defect due to differences in reflectivities of the
two Fabry-P\'erot arm cavities. The team is further investigating these
effects.
