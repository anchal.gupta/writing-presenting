\section{Gravitational waves detector current readout methods}
\label{sec:currentMethods}

\input{BHD/figureTex/MICHschematic}

Gravitational wave detectors utilize a Michelson interferometer to detect
differential arm length (\gls{DARM}) changes produced by gravitational waves.
Most of them are added with Fabry-P\'erot arm cavities, power recycling cavity,
and signal extraction cavity to improve the detection sensitivity, but the
differential length sensing comes from the Michelson interferometer part.
Fig.~\ref{fig:MICHschematic} shows a simplified schematic of how Michelson
interferometer senses gravitational waves. Let's assume the input electric
field amplitude of the laser to the interferometer be $E_{\mathrm{in}}$ in a
frame of reference rotating at the optical frequency. We assume the following
convention on phase rotations while reflecting from the beamsplitter: the
transmitted light suffers no phase change and the reflection from the input
port towards the X arm suffers a phase delay of $\pi/2$, and the reflection
from the Y arm to the output port gets a phase gain of $\pi/2$. This means that
after the beam splitter, the field in the arms is given by:

\begin{equation}
\begin{aligned}
E_{\mathrm{BS} \rightarrow \mathrm{X}} &= -\frac{i}{\sqrt{2}}E_{\mathrm{in}}\\
E_{\mathrm{BS} \rightarrow \mathrm{Y}} &= \frac{1}{\sqrt{2}}E_{\mathrm{in}}\\
\end{aligned}
\end{equation}

The fields in the arms travel to the mirror and reflect, accumulating phase
delay of $k L_{\mathrm{X}}$ and $k L_{\mathrm{Y}}$, where $k = 2\pi / \lambda$
and $L_{\mathrm{X}}$ and $L_{\mathrm{Y}}$ are the arm lengths. The beams merge
at the beam splitter to get the following fields at the \gls{AS} and \gls{REFL}
ports:

\begin{equation}
\label{eq:MICHresp}
\begin{aligned}
E_{\mathrm{AS}} &= i E_{\mathrm{in}}
                   \frac{e^{-i 2 k L_{\mathrm{Y}}}
                         - e^{-i 2 k L_{\mathrm{X}}}}
                        {2}
                = i E_{\mathrm{in}} e^{-i 2 k L_{\mathrm{C}}}
                  \frac{e^{i 2 k L_{\mathrm{D}}}
                        - e^{-i 2 k L_{\mathrm{D}}}}
                       {2}\\
E_{\mathrm{REFL}} &= E_{\mathrm{in}}
                     \frac{e^{-i 2 k L_{\mathrm{Y}}}
                           + e^{-i 2 k L_{\mathrm{X}}}}
                          {2}
                  = E_{\mathrm{in}} e^{-i 2 k L_{\mathrm{C}}}
                    \frac{e^{-i 2 k L_{\mathrm{D}}}
                          + e^{-i 2 k L_{\mathrm{D}}}}
                         {2}
\end{aligned}
\end{equation}

Here, we define common arm length as $L_{\mathrm{C}} = \frac{L_{\mathrm{X}} +
L_{\mathrm{Y}}}{2}$ and the differential arm length as $L_{\mathrm{D}} =
\frac{L_{\mathrm{X}} - L_{\mathrm{Y}}}{2}$. Note that any field coming at the
antisymmetric port is $\pi/2$ phase shifted with respect to the input beam.
Readers can verify that this property of Michelson interferometers is
independent of the BS phase convention. Now, the light power at the \gls{AS}
port is given by:

\begin{equation}
\label{eq:AS_MICH}
P_{\mathrm{AS}} = E_{\mathrm{AS}}^* E_{\mathrm{AS}}
                = P_{\mathrm{in}}sin^2(2 k L_{\mathrm{D}})
\end{equation}

If the Michelson interferometer is somehow maintained such that \gls{AS} port
is dark, then for small differential arm length changes such as those generated
by gravitational waves, we can approximate the antisymmetric power output as
the square of the gravitational wave signal.

\begin{equation}
P_{\mathrm{AS}}(t) \approx P_{\mathrm{in}}\left(\frac{4 \pi L h_{\mathrm{GW}}
(t)}{\lambda}\right)^2
\end{equation}

where $h_{\mathrm{GW}}$ is the gravitational wave strain on an arm's length of
L. From here onwards, we'll see how two different readout schemes are possible
to read the gravitational wave signal.

\subsection{DC readout (Offset readout)}
\label{subsec:DCreadout}

\input{BHD/figureTex/DCRschematic}

There are two issues with relying on reading the Michelson interferometer at
the absolute dark point. First, it is not possible to control the
interferometer at this point since the light power (which is the only real
quantity we can measure) is quadratically dependent on differential arm
difference, so the output increases with \gls{DARM} moving in either direction.
For a successful lock point to exist, we need an error signal which crosses
zero at the lock point, that is, it is positive after the lock point and
negative before it, so that negative feedback can hold it at the zero point.
Secondly, and more importantly, the output signal is a square of the
gravitational wave signal reducing its sensitivity considerably.

To fix both these issues at once, a commonly used technique is to lock the
Michelson at a small offset from the true dark point. This provides a non-zero
slope at the lock point so that the interferometer can be locked reliably to
the offset point, and it allows some of the interferometer circulating light to
leak out of the \gls{AS} port. This leakage electric field is strong and only
depends on common length fluctuations of the interferometer. If the common
length fluctuations are minimized by other loops (for example, by locking the
main laser to the common length fluctuations of the interferometer), then we
get a strong local oscillator to beat the signal field with, thus giving a
linear signal in the differential arm length. A simpler way to look at this is
to simply Taylor series expand Eq.~\ref{eq:AS_MICH} around a \gls{DARM} offset
such that $L_{\mathrm{D}} = \Delta L + L h_{\mathrm{GW}}$:

\begin{equation}
P_{\mathrm{AS}, \Delta L} = P_{\mathrm{in}}sin^2(2 k \Delta L)
                            + 2 k L h_{\mathrm{GW}}
                              P_{\mathrm{in}}sin(4 k \Delta L)
\end{equation}

Thus we get a linear response in gravitational wave signal at this offset point
on top of some light $P_{\mathrm{in}}sin^2(2 k \Delta L)$ that will always be
present there. This scheme is currently employed in Advanced LIGO and is
commonly referred to as the DC readout scheme. Note that in practice, the
\gls{DARM} signal is suppressed by the locking loop and we read out the signal
by extracting the information from the error signal and control signal in the
loop. See Sec.\ref{subsec:calibration_pipeline} for how this is done.

\subsection{RF readout}
\label{subsec:RFreadout}

\input{BHD/figureTex/RFRschematic}

Just like there is \gls{PDH} technique to utilize RF sidebands to lock a cavity
at resonance instead of using offset locking, a similar technique is available
to lock the \gls{DARM} using RF sidebands to true dark point of the
interferometer. Assume an RF angular frequency ($\Omega$) phase modulation is
applied to the input laser to the interferometer. For simplicity, we'll focus
only on first-order sidebands. Thus input electric field to the interferometer
is now:

\begin{equation}
E_{\mathrm{in}} = E_{\mathrm{c}}
                  + E_{\mathrm{s}}(i e^{i \Omega t} + i e^{-i \Omega t})
\end{equation}

\input{BHD/figureTex/MICH}

Here, the 'c' subscript is for the carrier field, and 's' subscript is for the
sideband field. Utilizing Eq.~\ref{eq:MICHresp} the electric field at the
anti-symmetric port is given by:

\begin{equation}
\begin{aligned}
E_{\mathrm{AS}} =& i E_{\mathrm{c}} e^{-i 2 k L_{\mathrm{C}}}
                     \frac{e^{i 2 k L_{\mathrm{D}}} - e^{-i 2 k
L_{\mathrm{D}}}}{2}\\
                 &- E_{\mathrm{s}} e^{i \Omega t} e^{-i 2 k_+ L_{\mathrm{C}}}
                   \frac{e^{i 2 k_+ L_{\mathrm{D}}} - e^{-i 2 k_+
L_{\mathrm{D}}}}{2}\\
                 &- E_{\mathrm{s}} e^{-i \Omega t} e^{-i 2 k_- L_{\mathrm{C}}}
                   \frac{e^{i 2 k_- L_{\mathrm{D}}} - e^{-i 2 k_-
L_{\mathrm{D}}}}{2}
\end{aligned}
\end{equation}

where $k = 2 \pi / \lambda_c = \omega_c / c$, $k_- = (\omega_c - \Omega) / c =
k - \delta k$, and $k_+ = (\omega_c + \Omega) / c = k + \delta k$. $\omega_c$
is the carrier optical angular frequency. Further, let's assume that the
interferometer is at the dark point so that $2 k L_{\mathrm{D}} = n\pi +
\phi_{\mathrm{GW}}$ where $\phi_{\mathrm{GW}}$ is the small phase offset
between the two arms due to gravitational wave signal, and $n \in \mathbb{Z}$.
Fig.~\ref{fig:MICH} shows the carrier and sideband electric fields at the
\gls{AS} port. If we place an RF photodiode at the \gls{AS} port and we focus
on the signals oscillating at the modulation frequency of $\Omega$:

\begin{equation}
\begin{aligned}
P_{\mathrm{AS}, \Omega} &= 2 \mathrm{Re}
                           [
                           \left(
                           i E_{\mathrm{c}} e^{-i 2 k L_{\mathrm{C}}}
                           \frac{e^{i 2 k L_{\mathrm{D}}} - e^{-i 2 k
L_{\mathrm{D}}}}{2}
                           \right)
                           \left(
                           - E_{\mathrm{s}} e^{i \Omega t} e^{-i 2 k_+
L_{\mathrm{C}}}
                           \frac{e^{i 2 k_+ L_{\mathrm{D}}} - e^{-i 2 k_+
L_{\mathrm{D}}}}{2}
                           \right)^*\\
                           &\quad \quad+
                           \left(
                           i E_{\mathrm{c}} e^{-i 2 k L_{\mathrm{C}}}
                           \frac{e^{i 2 k L_{\mathrm{D}}} - e^{-i 2 k
L_{\mathrm{D}}}}{2}
                           \right)^*
                           \left(
                           - E_{\mathrm{s}} e^{-i \Omega t} e^{-i 2 k_-
L_{\mathrm{C}}}
                           \frac{e^{i 2 k_- L_{\mathrm{D}}} - e^{-i 2 k_-
L_{\mathrm{D}}}}{2}
                           \right)
                           ]\\
                      &= -\frac{1}{2} E_{\mathrm{c}}E_{\mathrm{s}} \mathrm{Re}
                         [
                         i e^{i 2 \delta k L_{\mathrm{C}} - i \Omega t}
                           \left(
                           e^{-i 2 \delta k L_{\mathrm{D}}} + e^{+i 2 \delta k
L_{\mathrm{D}}}
                           - e^{-i 2 (2 k + \delta k) L_{\mathrm{D}}}
                           - e^{i 2 (2 k + \delta k) L_{\mathrm{D}}}
                           \right)\\
                         &\quad \quad-
                         i e^{i 2 \delta k L_{\mathrm{C}} - i \Omega t}
                           \left(
                           e^{-i 2 \delta k L_{\mathrm{D}}} + e^{+i 2 \delta k
L_{\mathrm{D}}}
                           - e^{-i 2 (2 k - \delta k) L_{\mathrm{D}}}
                           - e^{i 2 (2 k - \delta k) L_{\mathrm{D}}}
                           \right)
                         ]\\
                       &= -\frac{1}{2} E_{\mathrm{c}}E_{\mathrm{s}} \mathrm{Re}
                          [ i e^{i 2 \delta k L_{\mathrm{C}} - i \Omega t}\\
                          &\quad \quad
                           \left(
                           - e^{-i n \pi \frac{\delta k}{k}
                                -i 2 \phi_{\mathrm{GW}}}
                           - e^{+i n \pi \frac{\delta k}{k}
                                + i 2 \phi_{\mathrm{GW}}}
                            + e^{+i n \pi \frac{\delta k}{k}
                                 -i 2 \phi_{\mathrm{GW}}}
                            + e^{-i n \pi \frac{\delta k}{k}
                                 +i 2 \phi_{\mathrm{GW}}}
                            \right)
                          ]\\
\end{aligned}
\end{equation}

Note that we ignored products like $\frac{\delta k}{k} \phi_{\mathrm{GW}}$ as
it is the gravitational wave phase shift suppressed further by the ratio of RF
frequency to the optical frequency. Demodulating this signal at $\Omega$ with a
phase of $\pi/2 - 2 \delta k L_{\mathrm{C}}$ will give:

\input{BHD/figureTex/MICH_AS_REFL}

\begin{equation}
\begin{aligned}
P_{\mathrm{AS}, \Omega, Q}
                      &= -\frac{1}{2} E_{\mathrm{c}}E_{\mathrm{s}} \mathrm{Re}
                         [ i e^{i \pi / 2}\\
                         &\quad \quad
                          \left(
                          - e^{-i n \pi \frac{\delta k}{k}
                               -i 2 \phi_{\mathrm{GW}}}
                          - e^{+i n \pi \frac{\delta k}{k}
                               + i 2 \phi_{\mathrm{GW}}}
                           + e^{+i n \pi \frac{\delta k}{k}
                                -i 2 \phi_{\mathrm{GW}}}
                           + e^{-i n \pi \frac{\delta k}{k}
                                +i 2 \phi_{\mathrm{GW}}}
                           \right)
                         ]\\
                        &= - E_{\mathrm{c}}E_{\mathrm{s}}
                           \left(
                           cos(n \pi \frac{\delta k}{k}
                                   + 2 \phi_{\mathrm{GW}})
                           - cos(n \pi \frac{\delta k}{k}
                                   - 2 \phi_{\mathrm{GW}})
                           \right)\\
                        &= 2 E_{\mathrm{c}}E_{\mathrm{s}}
                           sin(n \pi \frac{\delta k}{k})
                           sin(2 \phi_{\mathrm{GW}})
\end{aligned}
\end{equation}
Since we have a choice on what value of $n$ to use, we can choose it to our
advantage. If we choose $n$ such that $n \pi \frac{\delta k}{k} = (4m +
1)\frac{\pi}{2}$ where $m \in \mathbb{Z}$, then we would maximize the
sensitivity of the signal. Note that the minimum macroscopic arm length
difference that suffices this for a $\sim$ 50 MHz RF sideband is roughly 1.5m.
This asymmetry is known as Schnupp asymmetry. In practice, the Schnupp
asymmetry is smaller than this number, which only reduces the sensitivity to
$\phi_{\mathrm{GW}}$ slightly. Thus the demodulated signal in one quadrature is
directly proportional to the phase shift due to a gravitational wave:

\begin{equation}
\label{eq:AS_MICH_RF}
\begin{aligned}
P_{\mathrm{AS}, \Omega, Q} \propto \phi_{\mathrm{GW}}
\end{aligned}
\end{equation}

This method allows one to truly remain at a dark point while being linearly
sensitive to the gravitational wave signal. Fig.~\ref{fig:MICH_AS_REFL} shows
the phasor diagram representation of gravitational waves audio sidebands
entering the Michelson interferometer with the input phase modulated field and
how the sidebands then sample the gravitational wave signal at the AS port.
