\section{Mathematical formalism}

\input{BHD/figureTex/BHDschematic}

The idea of BHD is simple: just like the DC readout scheme, we want to bring a
strong local oscillator to beat with the gravitational wave signal field.
Instead of using a \gls{DARM} offset to get this field locally, we can pick off
this local oscillator field before the laser enters the Michelson
interferometer, and then overlap it with the antisymmetric (AS) port output
field on a beamsplitter. Fig.~\ref{fig:BHDschematic} shows a simplified
schematic for this scheme.

Let's begin the \gls{AS} port electric field presented in
Sec.\ref{sec:currentMethods}.

\begin{equation}
E_{\mathrm{AS}} = i E_{\mathrm{in}} e^{-i 2 k L_{\mathrm{C}}}
                  \frac{e^{i 2 k L_{\mathrm{D}}} - e^{-i 2 k
L_{\mathrm{D}}}}{2}
                = - 2 E_{\mathrm{in}} e^{-i 2 k L_{\mathrm{C}}} sin(2 k
L_{\mathrm{D}})\\
\end{equation}

The local oscillator (LO) field is at the same optical frequency as the carrier
field as it is picked off from the input of the Michelson interferometer.
However, since the \gls{LO} field is traveling a different optical path than
the \gls{AS} field, it can have a different phase than the carrier field. So
the local oscillator field is given by:

\begin{equation}
E_{\mathrm{LO}} = E_{\mathrm{in}}e^{-i \phi_{\mathrm{LO}}}
\end{equation}

When these two fields overlap with each other on a beamsplitter, the two output
fields from the beam splitter are (assuming the beamsplitter is exactly 50:50):

\begin{equation}
\begin{aligned}
E_{\mathrm{1}} &= \frac{1}{\sqrt{2}} E_{\mathrm{LO}}
                 + \frac{i}{\sqrt{2}}E_{\mathrm{AS}}\\
E_{\mathrm{2}} &= \frac{i}{\sqrt{2}} E_{\mathrm{LO}}
                 + \frac{1}{\sqrt{2}}E_{\mathrm{AS}}\\
\end{aligned}
\end{equation}

Assuming equal transimpedance of %T_I$ on both photodetectors (hence balanced),
the photocurrents on the two photodiodes are:

\begin{equation}
\begin{aligned}
P_{\mathrm{1}} &= \frac{T_I}{2}
                   \left(
                   |E_{\mathrm{LO}}|^2 + |E_{\mathrm{AS}}|^2
                   -i E_{\mathrm{LO}}E_{\mathrm{AS}}^*
                   +i E_{\mathrm{LO}}^* E_{\mathrm{AS}}
                   \right)\\
P_{\mathrm{2}} &= \frac{T_I}{2}
                  \left(
                  |E_{\mathrm{LO}}|^2 + |E_{\mathrm{AS}}|^2
                  +i E_{\mathrm{LO}} E_{\mathrm{AS}}^*
                  -i E_{\mathrm{LO}}^* E_{\mathrm{AS}}
                  \right)\\
\end{aligned}
\end{equation}

When the photocurrent from two photodiodes is subtracted, we get the following:

\begin{equation}
\begin{aligned}
P_{\mathrm{BHD}} &= i T_I \left(
                     E_{\mathrm{LO}}^* E_{\mathrm{AS}}
                    - E_{\mathrm{LO}}E_{\mathrm{AS}}^*
                    \right)\\
                 &= - 2 T_I |E_{\mathrm{in}}|^2
                    \mathrm{Im}[
                    - 2
                    e^{i \phi_{\mathrm{LO}}}  e^{-i 2 k L_{\mathrm{C}}} sin (2
k L_{\mathrm{D}})
                    ]\\
                 &= 4 T_I |E_{\mathrm{in}}|^2
                    sin(\phi_{\mathrm{LO}} - 2 k L_{\mathrm{C}}) sin(2 k
L_{\mathrm{D}})\\
\end{aligned}
\end{equation}

During operation, when the laser is locked to the \gls{CARM} mode, $2 k
L_{\mathrm{C}} = 2 n \pi$ where $n \in \mathbb{Z}$. For operating near the dark
point in \gls{AS} port, the gravitational wave signal $h_{\mathrm{GW}}$ will
appear in \gls{DARM} as $L_{\mathrm{D}} = m\frac{\pi}{k} + L_{\mathrm{C}}
h_{\mathrm{GW}}$, the the BHD output becomes:

\begin{equation}
\label{eq:BHD}
P_{\mathrm{BHD}} \approx 4 T_I |E_{\mathrm{in}}|^2
                         sin(\phi_{\mathrm{LO}}) 2 k L_{\mathrm{C}}
h_{\mathrm{GW}}
\end{equation}

Thus, in the ideal case, we get the maximum linear response of the BHD output
to the gravitational wave signal for the choice of $\phi_{\mathrm{LO}} =
\pi/2$. Note that this expression is indeed equivalent to the DC readout
expression when $\phi_{\mathrm{LO}} = \pi/2$ but, there is no DC offset in the
readout signal. The absence of DC light reduces the shot noise on the detector.
Additionally, we have an extra free parameter in tuning the readout, the
homodyne phase angle $\phi_{\mathrm{LO}}$ which can be adjusted to read out
different quadratures for quantum benefits if required.
