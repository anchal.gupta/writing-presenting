\section{Optical delay line frequency discrimination scheme}
\label{sec:ODFD}

\input{VoyALS/figureTex/ODFDSchematic}

Fig.~\ref{fig:ODFDSchematic} shows this scheme which uses long dichroic optical
fiber cable in place of a reference cavity. The main feature of this scheme is
the use of optical delay line frequency discrimination (ODFD) which works on
the same principle as described in Sec.\ref{subsec:DFD} but at optical
frequencies.

The transmitted auxiliary laser and a pick-off of the main laser are overlapped
and sent to a fiber collimator into a fiber optic cable that supports
transmission of both the main laser wavelength and auxiliary laser wavelength.
This fiber goes to a fiber beam splitter which launches the two outputs into
two different paths, one path taking it through a long optical fiber spool
which adds a time delay of $\tau_{\mathrm{d}}$, and the other path is short.
The two paths are recombined on another beam splitter. The outputs of the
second beamsplitter go to the free space through fiber collimators. Here, two
dichroic mirrors separate the auxiliary wavelength from the main wavelength.
This step can be done with waveguides also. The electric fields at the two
output ports for each Frequency are then given by:

\begin{equation}
\begin{aligned}
E_{out, 1} &= \frac{1}{\sqrt{2}} E_{in}
             \left(
                   e^{i 2 \pi \nu_{\mathrm{main/aux}} t}
                   + i e^{i 2 \pi \nu_{\mathrm{main/aux}} (t -
\tau_{\mathrm{d}})}
             \right)\\
E_{out, 2} &= \frac{1}{\sqrt{2}} E_{in}
            \left(
                  i e^{i 2 \pi \nu_{\mathrm{main/aux}} t}
                  + e^{i 2 \pi \nu_{\mathrm{main/aux}} (t - \tau_{\mathrm{d}})}
            \right)
\end{aligned}
\end{equation}

The two outputs are read on two balanced photodiodes and a difference is taken
(similar to the balanced homodyne detection), the output is then:

\begin{equation}
\begin{aligned}
P_{\mathrm{ODFD, main}} &= E_{in}^2
                           sin(2 \pi \nu_{\mathrm{main}}\tau_{\mathrm{d}})\\
P_{\mathrm{ODFD, aux}} &= E_{in}^2
                          sin(2 \pi \nu_{\mathrm{aux}}\tau_{\mathrm{d}})\\
\end{aligned}
\end{equation}

The short fiber can be stretched with a fiber stretcher to ensure that both $2
\pi \nu_{\mathrm{main}}\tau_{\mathrm{d}}$ and $2 \pi
\nu_{\mathrm{aux}}\tau_{\mathrm{d}}$ both are integer multiples of $2 \pi$ at
DC. Then the fluctuations in both lasers will show as linear signals at the two
outputs. The auxiliary laser signal can be multiplied in gain by $-p/q$ (ratio
of the main laser frequency to auxiliary laser frequency) to generate the
desired signal:

\begin{equation}
\Delta_\mathrm{ALS} = \delta \nu_{\mathrm{main}} -
                      \frac{\nu_{\mathrm{main}}}{\nu_{\mathrm{aux}}}
                      \delta \nu_{\mathrm{aux}}
\end{equation}

This signal can be fed back to the main laser. This is an exciting way to solve
the \gls{ALS} problem for Voyager since it does not require high-cost
equipment. The bottleneck in this scheme would be to keep $\tau_{\mathrm{d}}$
from drifting, especially if the setup will be used for calibration of the
interferometer as well. Further noise analysis is required to look into other
possible noise sources and their effect on the performance of \gls{ALS}.
