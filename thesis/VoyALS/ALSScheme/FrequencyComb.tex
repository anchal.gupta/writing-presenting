\section{Frequency comb scheme}

\input{VoyALS/figureTex/FreqCombSchematic}

The invention of the frequency comb\cite{FreqComb} at the start of this century
springboarded the precision measurement community in optical physics to orders
of magnitude improvements\cite{FreqCombReview}. The ability to transfer
stability from atomic clocks to other optical frequencies of choice improved
the precision measurements one can do using frequency measured against these
stable optical references\cite{Hollberg2005}. An optical frequency comb is a
source of light with multiple narrow linewidth frequencies present which are
all separated from each other with a constant frequency, known as the
repetition rate. A typical $n^{\mathrm{th}}$ tooth of a frequency comb can be
described as:

\begin{equation}
\nu_n = \nu_{\mathrm{CEO}} + n \nu_{\mathrm{rep}}
\end{equation}

where $\nu_{\mathrm{CEO}}$ is known as the carrier offset envelope and
$\nu_{\mathrm{rep}}$ is called the repetition rate of the comb. A favorable
condition arises if the frequency comb is so-called "octave spanning" that is
its highest frequency tooth is at least twice in frequency as its lowest
frequency tooth. In this particular case, one can measure the carrier envelope
offset by frequency doubling the frequency comb light on a second harmonic
generation stage optimized by frequency doubling the lower half of the
comb\cite{Jones2000}. The output of this stage naturally has the frequency
doubled lower teeth beating with the original comb's upper teeth at the
carrier-envelope offset frequency. This method is usually used to feedback to
the frequency comb and remove the carrier envelop offset, but we think for our
purpose, we can use the measured carrier-envelope offset in feedforward.

Fig.~\ref{fig:FreqCombSchematic} shows the scheme that will use such an
octave-spanning frequency comb. The main laser frequency and auxiliary laser
frequency can be any wavelength in the low absorption region of silicon and
within the span of the frequency comb. This is a big benefit of this scheme as
we do not need to generate specific wavelengths using non-linear crystals for
being compatible with the \gls{ALS} scheme. We can choose wavelengths that have
low noise high power laser sources and amplifiers, and high quantum efficiency
photodiodes already available.

The scheme beats the transmitted auxiliary laser and a pick-off of the main
laser with the frequency comb. Each optical frequency beats with all the teeth
of the comb, but the closest tooth in frequency generates an RF frequency tone
that is picked up by the beat photodiodes. Let's assume that the main laser
beats with the $p^{\mathrm{th}}$ tooth and the auxiliary laser beats with the
$q^{\mathrm{th}}$ tooth. Then the beat frequencies will be:

\begin{equation}
\begin{aligned}
\Delta_{\mathrm{m}} &=  \nu_{\mathrm{CEO}} + p \nu_{\mathrm{rep}}
                              - \nu_{\mathrm{main}}\\
\Delta_{\mathrm{a}} &=  \nu_{\mathrm{CEO}} + q \nu_{\mathrm{rep}}
                              - \nu_{\mathrm{aux}}\\
\end{aligned}
\end{equation}

The frequency comb is also sent through a \gls{SHG} stage and then onto a
photodiode to get a signal corresponding to the $\nu_{\mathrm{CEO}}$. This
signal is mixed with the two beats generated above and low passed to remove the
carrier envelop offset and its fluctuations in feedforward. The two signals
after this mixing become:

\begin{equation}
\begin{aligned}
\Delta_{\mathrm{m}}' &=  p \nu_{\mathrm{rep}}
                                    - \nu_{\mathrm{main}}\\
\Delta_{\mathrm{a}}' &=  q \nu_{\mathrm{rep}}
                                   - \nu_{\mathrm{aux}}\\
\end{aligned}
\end{equation}

Then the signal from the auxiliary beat is sent to a Direct Digital Synthesis
(DDS) chip. This device takes an input sinusoidal signal and multiplies the
frequency by a fixed number provided to it as a digital input. DDS chips with
the frequency multiplier number in 64-bit precision are available these days.
We provide this chip the number $p/q$ to multiply with the auxiliary beat
signal to get:

\begin{equation}
\Delta_{\mathrm{a}}'' =  p \nu_{\mathrm{rep}}
                                   - \frac{p}{q}\nu_{\mathrm{aux}}
\end{equation}

After this step, the two beat frequencies are mixed on a mixer and low passed
to take their difference. Upto the precision of the DDS chip, the repetition
rate component and any fluctuations due to it get canceled in this step, giving
an \gls{ALS} error signal that is proportional to the correct difference
between the two frequencies:

\begin{equation}
\Delta_{\mathrm{ALS}} = \nu_{\mathrm{main}} - \frac{p}{q}\nu_{\mathrm{aux}}
\end{equation}

Note that this difference follows the condition set in Eq.~\ref{eq:compWorks}
for the \gls{ALS} scheme to work. We also do not need a costlier low noise
frequency comb for this method to work as all the noise in the frequency comb
is canceled in feedforward. Nevertheless, creating an octave-spanning frequency
comb is a challenging task, and would require significant time and money. So
alternate methods would be desirable if possible.
