\chapter{Voyager}
\label{ch:Voyager}

The current generation of gravitational wave detectors is detecting about one
compact binary merger event each week in a full sensitivity observation run.
While this is an impressive achievement given less than a decade ago it was
still unconfirmed if gravitational waves exist, to build more statistics and
collect more details about these events, we need to improve the sensitivity of
future detectors. The current range for Advanced LIGO is about 220 MPc for 1.4
$M_{\cdot}$ mass binary neutron star merger and about 1.3 GPc for 10
$M_{\cdot}$ binary black hole merger if the events are optimally oriented to
the detector antenna pattern\cite{Miller2015}. With the ongoing "A+" upgrade,
the detector sensitivity might increase by 1.5 times.

At this time, no further upgrade of the Advanced LIGO sites is approved. A
subset of the LIGO collaboration has proposed a cryogenic silicon
interferometer to be placed at the Advanced LIGO sites\cite{Adhikari2020}. This
proposal aims to improve the range of the Advanced LIGO detectors by 4 to 5
times, increasing the detection rate of compact binary mergers by an estimated
100 times. This is exciting as this requires no site location search or new
large-scale construction while serving as an intermediate research platform for
larger and more complex proposed next-generation gravitational wave detectors
such as Cosmic Explorer\cite{CEP2100003} and Einstein Telescope\cite{ET}.

I contributed to the design study of Voyager as the final part of my thesis
research. My contribution was limited to proposing the arm length stabilization
schemes for Voyager. In this chapter, we'll go through a quick introduction to
the main features of the proposed Voyager detector, and then we'll define the
problem statement for the requirements from arm length stabilization system for
this detector.

\section{Proposed cryogenic silicon interferometer}
\input{VoyALS/figureTex/VoySchematic}

The main feature of the Voyager proposal is to use cryogenic crystalline
silicon as the substrate material for the test masses in the same dual recycled
Fabry-P\'erot Michelson interferometer configuration.
Fig.~\ref{fig:VoySchematic} shows the proposed schematic of the
detector\cite{Adhikari2020}. The choice of crystalline silicon is motivated on
multiple fronts. In current detectors, when the circulating power inside the
arm cavities is attempted to increase, it causes wavefront distortions at the
mirror surface due to temperature gradients in the fused silica substrate for
the test masses. This limits the circulating power and thus the achievable
noise floor in high frequencies due to quantum shot noise. Since silicon is a
good thermal conductor, the temperature gradient on the mirror body will be
less severe, allowing for larger circulating power in the arm cavities without
wavefront distortions.

Secondly, the thermo-elastic noise of the substrate limits the noise floor of
the detector. Crystalline silicon has a zero crossing of the coefficient of
thermal expansion at 123 K. So if the test masses are cooled to this
temperature, the test mass will be insensitive to thermal fluctuations to first
order, greatly reducing the classical noise source in the detector at low
frequencies. Third, the coating Brownian noise, which is currently the
dominating classical noise source reduces due to low temperature and adoption
of amorphous Silicon coatings. Fig.~\ref{fig:VoyagerNB} shows the noise budget
of the proposed Voyager detector and its comparison with Advanced LIGO and "A+"
sensitivity.

\input{VoyALS/figureTex/VoyagerNB}

The switch in substrate material from fused silica to crystalline silicon can
be very beneficial, but this requires major changes in the rest of the detector
subsystems. For instance, Silicon is opaque to wavelengths smaller than 1200
nm. Thus the Voyager proposal adopted 2 um wavelength to be used for the main
laser. More study is being done on developing a laser source at 2 um wavelength
and amplifying it to high power for injecting it into the interferometer. To
continue utilizing the benefits of squeezed vacuum injection and
frequency-dependent squeezing through filter cavities, new squeezer designs and
filter cavity designs are also required.

Further, achieving 123 K cooling without contacting the test masses is another
big challenge. The proposal plans to achieve the cooling through radiative
cooling with cooled thermal shields around the test masses. Another important
consideration due to the change of wavelength is to look into high quantum
efficiency photodetectors for 2 um wavelength.

\input{VoyALS/Voyager/ALSgeneral}
