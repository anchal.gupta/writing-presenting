\section{Laser frequencies and ALS requirements}

\input{VoyALS/figureTex/SiAbsorption}

The choice of crystalline silicon substrates for the test masses of Voyager
creates new requirements for the laser wavelengths to be used.
Fig.~\ref{fig:SiAbsorption} shows the absorption coefficient in silicon. Laser
frequencies below 1.2 $\mu$m are severely absorbed in crystalline silicon. So
the adopted choice of main laser frequency is around 2 $\mu$m.

A simple extension of the existing second harmonic generation method for
\gls{ALS} (see Sec.\ref{sec:ALS}) purposes would mean a 1 $\mu$m wavelength for
the auxiliary laser, but that would get absorbed in the silicon test masses. So
the auxiliary laser would have to be above 1.3 $\mu$m and we would need to find
an alternate scheme for ALS in Voyager. In this section, I will first define
our problem statement and the goal of \gls{ALS}.

\input{VoyALS/figureTex/ALSgeneral}

Figure Fig.~\ref{fig:ALSgeneral} shows the general simplified scheme of
\gls{ALS} in voyager. The auxiliary laser is locked to one of the arms of the
interferometer. Let's call this arm length, $L_{\mathrm{arm}}$. This means that
the auxiliary laser frequency $\nu_{ALS}$ obeys this relationship:

\begin{equation}
\nu_{\mathrm{aux}} = m_1 \frac{c}{2 L_{\mathrm{arm}}}
\end{equation}

Here, $m_1$ is an integer to denote the cavity mode to which the auxiliary
laser is resonant. The fluctuations of the arm cavity are transferred to
auxiliary laser frequency through open loop gain H(s) (dropping s in equations
for ease of notation) as:

\begin{equation}
\label{eq:AUXlock}
\delta \nu_{\mathrm{aux}} = \frac{H}{1 -H}\nu_{\mathrm{aux}}\frac{\delta
                            L_{\mathrm{arm}}}{L_{\mathrm{arm}}}
\end{equation}

Our main objective in \gls{ALS} is to bring the main laser to an offset lock
with the arm cavity, that is, to transfer arm length fluctuations to the main
laser frequency at an arbitrary offset, such that when the offset is reduced,
the main laser is resonant with the arm cavity. Thus, our required lock point
for the main laser frequency $\nu_{\mathrm{main}}$ has the condition:

\begin{equation}
\label{eq:ALScond1}
\delta \nu_{\mathrm{main}} = - \nu_{\mathrm{main}}\frac{\delta
                               L_{\mathrm{arm}}}{L_{\mathrm{arm}}}
\end{equation}

and

\begin{equation}
\label{eq:ALScond2}
\nu_{\mathrm{main}} = m_2 \frac{c}{2 L_{\mathrm{arm}}}
                      + \Delta \frac{c}{2L_{\mathrm{arm}}}
\end{equation}

Here $m_2$ is another integer corresponding to the closest resonant tooth in
the resonance modes supported by the cavity and $\Delta$ is the fraction of
free spectral range of the cavity by which the main laser frequency is away
from resonance.

Now, let's assume a general optical frequency comparison scheme that provides
the following output:

\begin{equation}
\label{eq:comp}
X_{\mathrm{comp}}(\nu_{\mathrm{main}}, \nu_{\mathrm{aux}}) =
                   \delta\nu_{\mathrm{main}} + k \delta \nu_{\mathrm{aux}}
\end{equation}

where k is a constant defining the comparison function.

The main laser is typically locked to a reference cavity $L_{\mathrm{ref}}$
with an open loop gain of $G$ as shown in the figure. If the output of the
comparator is fed back to the reference cavity length with a feedback gain of
$M$:

\begin{equation}
\delta L_{\mathrm{ref}} = M(\delta \nu_{\mathrm{main}} + k \delta
\nu_{\mathrm{aux}})
\end{equation}
Here $\delta L_{\mathrm{ref}}$ is written in units of frequency for ease of
notation. Then at the control point of the \gls{ALS} loop:

\begin{equation}
\begin{aligned}
\delta \nu_{\mathrm{main}} &= G (\delta \nu_{\mathrm{main}}
                                 - M(\delta\nu_{\mathrm{main}}
                                 + k \delta \nu_{\mathrm{aux}}))\\
\delta \nu_{\mathrm{main}} &= - \frac{GMk}{1 - G + GM}
                                \delta \nu_{\mathrm{aux}}
\end{aligned}
\end{equation}

Further, using the auxiliary laser lock condition from Eq.~\ref{eq:AUXlock}:
\begin{equation}
\begin{aligned}
\delta \nu_{\mathrm{main}} &= - \frac{GMk}{1 - G + GM}\frac{H}{1 - H}
                                \nu_{\mathrm{aux}}
                                \frac{\delta L_{\mathrm{arm}}}
                                     {L_{\mathrm{arm}}}\\
\delta \nu_{\mathrm{main}} &= - \frac{k}{
                                    \left(
                                      \frac{1}{MG}
                                      - \frac{1}{M}
                                      +1
                                    \right)
                                    \left(
                                       \frac{1}{H}
                                       - 1
                                    \right)}
                                \delta \nu_{\mathrm{aux}}
                                \frac{\delta L_{\mathrm{arm}}}
                                     {L_{\mathrm{arm}}}
\end{aligned}
\end{equation}

At high feedback gains (at low frequencies) for any general feedback loops
where $|M|>>1$, $|G|>>1$, and $|H|>>1$ regardless of the sign of the feedback
loop reduces down to:

\begin{equation}
\delta \nu_{\mathrm{main}} = k \nu_{\mathrm{aux}}
                             \frac{\delta L_{\mathrm{arm}}}{L_{\mathrm{arm}}}
\end{equation}

That is a comparator $X_{\mathrm{comp}}$ defined in Eq.~\ref{eq:comp} locks the
main laser frequency fluctuations to $k$ times the auxiliary laser frequency.
To fulfill the condition laid down in Eq.~\ref{eq:ALScond1}, we will need:

\begin{equation}
\begin{aligned}
- \nu_{\mathrm{main}}\frac{\delta L_{\mathrm{arm}}}{L_{\mathrm{arm}}}
         &= k \nu_{\mathrm{aux}}
            \frac{\delta L_{\mathrm{arm}}}{L_{\mathrm{arm}}} \\
\nu_{\mathrm{main}} &= -k \nu_{\mathrm{aux}} \\
m_2 \frac{c}{2 L_{\mathrm{arm}}} + \Delta \frac{c}{2 L_{\mathrm{arm}}}
         &= -k m_1 \frac{c}{2 L_{\mathrm{arm}}} \\
k &= - \frac{m_2}{m_1} - \frac{\Delta}{m_1}\\ k &\approx -\frac{m_2}{m1}
\end{aligned}
\end{equation}

The offset of the main laser from the reference can be controlled as an offset
in the \gls{ALS} loop, so the second term above can be discarded. It is
important though to have the first term to fulfill the condition in
Eq.~\ref{eq:ALScond1}. Thus the comparator required for \gls{ALS} in Voyager
must have the following form:

\begin{equation}
k = -\frac{m_2}{m_1} \approx -\frac{\nu_{\mathrm{main}}}{\nu_{\mathrm{aux}}}
\end{equation}

This means that to keep the main laser offset locked to the arm cavity, it must
follow the fluctuations of auxiliary laser frequency in the same direction and
must be scaled in proportion to the optical frequency ratio. Another way of
looking at it is that the comparator function must take the difference between
the relative frequency motion of the two optical frequencies, that is:

\begin{equation}
\label{eq:compWorks}
X_{\mathrm{good}}(\nu_{\mathrm{main}}, \nu_{\mathrm{aux}})
   = \frac{\delta \nu_{\mathrm{main}}}{\nu_{\mathrm{main}}}
     - \frac{\delta \nu_{\mathrm{aux}}}{\nu_{\mathrm{aux}}}
\end{equation}

To test this, we should check the case of the current \gls{ALS} scheme in
advanced LIGO. The auxiliary laser is generated by frequency doubling the main
laser frequency. Thus the ratio $-m_2/m_1$ becomes $-1/2$ for the required
value of k. For comparing the frequencies, the current \gls{ALS} scheme
frequency doubles the main laser and takes a beatnote between it and the
auxiliary laser, returning $\propto 2 \nu_{\mathrm{main}} - \nu_{\mathrm{aux}}$
which is indeed equivalent to a comparator $X_{\mathrm{comp}}$ with $k=-1/2$.
