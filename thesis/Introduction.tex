\extrachapter[Next-generation technologies for gravitational wave
              detectors]{Introduction}

\begin{center}
\Large{Next-generation technologies for gravitational wave detectors}
\end{center}

As far back as we see in history, humans have tried to look into outer space
for the answers about origins of our universe. Astronomy is an old scientific
field that has been improved to unimaginable degrees as time progressed. Two
binary black holes, each about 30 times as massive as our Sun merged and sent
out gravitational wave ripples. After traveling for about 1.3 billion years,
they reached Earth on September 14th, 2015, where we had just started looking
for them. The pair of detectors of Laser Interferometer Gravitational-Wave
Observatory\cite{Abbott2016} had observed a new kind of signal, heralding a new
era in astronomy and giving us a new window to probe the universe.

Since then, we have made our detectors better and have observed more than 90
confirmed compact binary coalescences\cite{Abbott2021d}. Eight years ago, we
did not even know if gravitational waves existed and if Einstein's general
theory of relativity produced correct results. This advancement took place
through decades of planning and teamwork among a large collaboration of
scientists. As I entered Caltech to learn how to do research in experimental
physics, I was passed the baton to take it further. Constant advancement is the
key to achieving the unimaginable. With that in mind, my thesis work comprised
of working on developing the next generation of technologies to make
gravitational wave detection more accurate, more precise, and more robust.
Gravitational wave astronomy, like any other sub-field of astronomy, must move
into the next phase of collecting a large sample of accurate and precise data.
This will help in building statistics to improve our understanding of the
universe from a new perspective.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{aLIGONB.pdf}
\caption[Advanced LIGO noise budget]
        {Advanced LIGO noise budget made using gwinc\cite{gwinc}. The grey
         curve is the measured noise in observation run "O3a" at LIGO Hanford
         Observatory.}
\label{fig:aLIGONB}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{allDetNB.pdf}
\caption[Future proposed detector sensitivity]
        {Sensitivty of future proposed detectors. Estimated using
         GWINC\cite{gwinc}.}
\label{fig:allDetNB}
\end{figure}

This thesis is divided into four parts, representing the four next-generation
technologies I have researched. The first part focusses on finding solutions
for the coatings Brownian noise that limits the current generation of
gravitational waves detectors from 30 Hz to 300 Hz region (see
Fig.~\ref{fig:aLIGONB}). This is a classical source of noise, fundamentally
present in the high reflectivity coatings used in the test masses of the
detectors. In this part, I'll describe an experiment we performed to directly
observe the coatings Brownian noise due to a new class of crystalline coatings,
measuring displacement noise at the level of $2\times10^{-18}$
m/$\sqrt{\mathrm{Hz}}$.

In the second part, I will describe our work at the 40m prototype of LIGO at
Caltech, where we upgraded the interferometer layout to create a Balanced
Homodyne Detection readout port. To the best of my knowledge, this is the first
time this detection method has been implemented on a suspended interferometer.
This method would be beneficial to remove the excess classical noise in the
low-frequency band as seen in the measured noise trace in
Fig.~\ref{fig:aLIGONB}. We tested important bottlenecks in the implementation of
this readout and measured preliminary noise improvement estimates for a
successful follow-up by advanced LIGO to upgrade the site detectors later.

In the third part, we work on developing a new calibration method for future
gravitational wave detectors. The gravitational waves detection community is
planning to expand the network of detectors and build new detectors with plans
to reduce the noise floor by more than an order of magnitude (see
Fig.~\ref{fig:allDetNB}). While this is good for measuring more compact binary
coalescences, the estimate of the event parameters would become limited by the
calibration uncertainty in the response function of the detectors, if the
present calibration method is not updated. In this part, I will describe our
work on developing an absolute calibration of the detector
referred to the ultra-stable optical common length mode of the arm cavities in
the detectors that would reduce systematic error contribution.

In the fourth part, I'll describe my small contribution to the ongoing work for
the proposed cryogenic upgrade to LIGO sites, called Voyager. This upgrade
would present new challenges to old solutions that worked in the current
generation of detectors. I worked on designing the arm-length stabilization
system for Voyager. This subsystem is currently used in locking several control
loops in the detector to reach science observation mode. Thus it is crucial to
find a solution for it along with planning of the detector upgrade.
