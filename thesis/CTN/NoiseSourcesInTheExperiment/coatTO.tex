\section{Coatings Thermo-Optic Noise}
\label{sec:coatTO}

Coatings thermo-optic noise is typically a dominant noise source in mirror
coatings. When a Gaussian beam of laser falls on a mirror surface, stress
inhomogeneity causes fluctuations in the temperature~\cite{Zener1938}. Due to
thermo-elasticity, the thickness of each layer fluctuates with the temperature,
proportional to their coefficient of thermoelasticity (CTE) $\alpha_j$ of the
$j^{th}$ layer. The same thermal fluctuations also induce changes in the
refractive indices of each layer that are proportional to their coefficients of
thermo-refractivity (CTR) $\beta_j$.

Since these two pathways of noise are driven by the same noise source, they are
coherently added such that total thermo-optic noise is given by \cite[Eq.
4.]{Evans2008}:

\begin{equation}
S_{coatTO}^x = \Gamma_{tc}
               S^{\Delta T} (\bar{\alpha_\text{c}}d
                             - \bar{\beta}\lambda
                             - \bar{\alpha_\text{s}}
                               d \frac{C_\text{c}}{C_\text{s}})
\label{eq:coatTO}
\end{equation}

where $\bar{\alpha_\text{c}}$ and $\bar{\beta}$ are effective compound CTE and
CTR for the coating respectively, $\bar{\alpha_\text{s}}$ is the effective CTE
for the substrate, and $\Gamma_{tc}$ is the thick coating correction factor
(see Sec.\ref{subsec:CoatEffParams} below). $S^{\Delta T}$ is single-sided
Gaussian beam profile weighted temperature fluctuation \gls{PSD} is given by
(see Sec.\ref{subsec:tempPSD} below):

\begin{subequations}
\begin{align}
S^{\Delta T} &= \frac{2^{3/2} k_\text{B} T^2}
                    {\pi\kappa_\text{s}w} M(f/f_\text{T})\\
   M(\Omega) &= Re\left[
                         \int\limits_0^\infty\!
                                \mathrm{d}u\,
                                \frac{u\ \mathrm{e}^{-u^2/2}}
                                     {\left(u^2-\mathrm{i}\Omega\right)^{1/2}}
                  \right]
\end{align}
\label{eq:tempPSD}
\end{subequations}

The negative sign in Eq.~\ref{eq:coatTO} means an optimization can be carried
out to make these two coupling mechanisms cancel each other. The mirrors we are
testing have a coating structure that is optimized to minimize this noise by
adjusting the layer thicknesses. This was experimentally demonstrated in
previously published work\cite{Tara2016}.

In the following two subsections, I briefly describe the roadmap to calculating
coatings thermo-optic noise by calculating the required effective parameters,
correction factors, and temperature \gls{PSD}.

\input{\NoiseBudget/CoatEffParams}

\input{\NoiseBudget/CoatTempPSD}

