\section{Technical Noise Sources}


In this section, I'll briefly describe all other noise sources in the
experiment that are not fundamentally present for all mirrors with Bragg
coatings but are present in this experiment due to our choice of setup,
sensing, and control of the experiment.

\subsection{Seismic Noise}
\label{subsec:seismic}
Low-frequency acceleration of the experimental apparatus couples into the
bending motion of the cavities as shown in Fig.~\ref{fig:noise_sources}. The
bending of the cavity leads to changes in the longitudinal length of the cavity
which directly affects the readout frequency from the cavity. This technical
noise coupling is minimized by placing the cavity supports at the Airy points,
and verifying the same with finite element analysis simulations
\cite{TaraThesis}. If we assume mounting errors of $\pm 0.5$ mm and common-mode
rejection due to mounting the two cavities on a common platform, the coupling
from acceleration into cavity strain is estimated to be
$6\times10^{-12}\,m^{-1}\,s^{2}$. The brown curve in Fig.~\ref{fig:ctn_nb}
shows the estimated coupled seismic noise from measured seismic vertical
acceleration in our laboratory.

\subsection{Sensing Noise}
\label{subsec:PLL}
The digital phase-locked loop (DPLL) used to readout the time series of beat
note frequency injects its own frequency noise. Any frequency reading device
can only read frequency noise as well as its own reference oscillator. With
rubidium clock stabilization, the frequency noise of our DPLL was found to be
less than 1 mHz/$\sqrt{\text{Hz}}$ up to 4 kHz. This was measured by generating
a 27.344 MHz signal by Moku:Lab and feeding it back to itself through a long
cable for frequency measurement. Along with this, the beatnote detector's dark
and shot noise also contributes to the sensing noise. The green curve in
Fig.~\ref{fig:ctn_nb} shows the contribution of this noise in the measurement.

\subsection{Controls Shot Noise}
\label{subsec:shot}
Shot noise in the resonant RF Photodiodes used in the \gls{PDH} loop for FSS
(see Sec.\ref{sec:fss}) adds noise to the frequency of the laser. We ensure
that enough light is falling on these detectors to keep them shot noise limited
rather than dark noise limited. For total power $P_0$ incident on the cavity,
the PDH shot-noise is estimated by:
\begin{equation}
\begin{aligned}
S_{PDHshot} &= \left(\frac{f_p}{2 P_0 m}\right)^2
               \left(1 + \frac{f}{f_p} \right)^2 \\
            &\quad
               \left(
                  2h\nu P_0 \left[
                                J_0(\Gamma)^2 (1-\eta)
                                + 3 J_1(\Gamma)^2
                            \right]
               \right)
\end{aligned}
\end{equation}
Here, the first term in parenthesis is the PDH discriminant term with units of
Hz/W, where $m$ is the modulation index of sidebands used for PDH. The second
term in parenthesis accounts for cavity pole $f_p$ and the third term is simply
the shot noise for the total power falling on the detectors including carrier
and sidebands. Overall, this noise goes down as we increase the power of the
laser. The grey curve in Fig.~\ref{fig:ctn_nb} shows the contribution of this
noise in our experiment as negligible in all of the frequency ranges.

\subsection{Laser Frequency Noise}

\label{subsec:laserFreq}
NPRO lasers are inherently low linewidth as they are stabilized with a
temperature-controlled crystal cavity in a monolithic non-planar geometry. The
free-running frequency noise \gls{ASD} of Nd:YAG NPRO is assumed to be 1
kHz/$\sqrt{\text{Hz}}$ at 10 Hz and assumed to fall as $1/f$ at higher
frequencies \cite{Willke2000}. We measured the frequency noise suppression of
our FSS (see Sec.\ref{sec:fss}) and applied that to get the estimated residual
frequency noise of the laser in the transmission from the cavities. The magenta
curve shows the suppressed laser frequency noise contribution in the beat note
frequency noise.

\subsection{Laser Amplitude Noise}
\label{subsec:photoThermal}

Some absorption of light always happens at the coatings. This means that the
coating gets heated due to the incident power on it. In the case of a cavity,
this is the circulating power inside the cavity. Therefore, any fluctuations in
the intensity of light drive fluctuations in the temperature of the coating.
Since the overall heat capacity of coatings is small, the delay in temperature
rise/fall due to intensity rise/fall is negligible and a near-instant response
is seen in the measurement frequency band. Since this is another source of
temperature fluctuations, the phase noise follows the same thermo-optic pathway
described in Sec.\ref{sec:coatTO} and can be written as:

\begin{equation}
\label{eq:photothermal}
S^x_\text{photoThermal} = |H(f)|^2 P^2_{abs} S_{RIN}
\end{equation}

where $H(f)$ is the complex photothermal transfer function (see below) for a
mirror, $P_{abs} = a_{coat}P_{circ}$ is power absorbed by the mirror and
$S_{RIN}$ is the \gls{PSD} of relative intensity noise (RIN) of the incident
laser. The blue curve in Fig.~\ref{fig:ctn_nb} shows the contribution of this
noise source in the beat note frequency noise.

\input{\NoiseBudget/PhotoThermalTFCalc}
