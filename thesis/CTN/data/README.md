# Data Files

This directory contains all required data files to plot figures that are
present in the paper.

## Origin of Files

### Coating Structure
[CoatingStructure.txt](CoatingStructure.txt) was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb). This notebook itself reads the data from [cit_ctnlab/ctn_noisebudget/Data/SavedPSDs_20200805/OptimizedCoatingStackv5.csv](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/Data/SavedPSDs_20200805/OptimizedCoatingStackv5.csv)

### FSS OLTF files
[North_FSS_OLTF.txt](North_FSS_OLTF.txt) and
[South_FSS_OLTF.txt](South_FSS_OLTF.txt) were measured and compiled on
Aug 5th, 2020. The code for measurement and compilation is present at
[cit_ctnlab/ctn_labdata/data/20200805_FSS_OLTFs](https://git.ligo.org/cit-ctnlab/ctn_labdata/-/tree/master/data/20200805_FSS_OLTFs).

## Contribution of Bulk and Shear noise fields
[qBk_qSk.txt](qBk_qSk.txt) was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb).

## Noise Budget for fitted bulk loss angle
[NoiseBudgetResultsData.pkl](NoiseBudgetResultsData.pkl) was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb). This pickled file contains dictionary of all noise budget traces plotted in the paper.

## Bayesian probability distribution
[bayProbData.txt](bayProbData.txt)  was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb).

## Bulk Loss Angle fit value tex string
[Bulk_Loss_Fit_Value_String.tex](Bulk_Loss_Fit_Value_String.tex) was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb). This is to ensure there is a single value referred in all places in the paper.

## Noise Budget for fitted bulk loss angle with power-law slope
[NoiseBudgetResultsData_Slope.pkl](NoiseBudgetResultsData.pkl) was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final_Slope.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final_Slope.ipynb). This pickled file contains dictionary of all noise budget traces plotted in the paper.

## Bayesian probability distribution for bulk loss angle and frequency dependence power-law slope
[bayProbData.txt](bayProbSlopeData.pkl)  was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final_Slope.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final_Slope.ipynb). This pcikled file contains 3D data to plot teh heatmap of bayesian probability distribution with two variables, bulk loss angle and its frequency dependence power-law slope.

## Bulk Loss Angle with slope fit value tex string
[Bulk_Loss_Fit_With_Slope_Value_String.tex](Bulk_Loss_Fit_With_Slope_Value_String.tex) was produced in
[cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final_Slope.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final_Slope.ipynb). This is to ensure there is a single value referred in all places in the paper.

## Skew-normal histogram fit example
[skewNormalFitEx.pkl](skewNormalFitEx.pkl) was produced in [cit_ctnlab/ctn_noisebudget/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb](https://git.ligo.org/cit-ctnlab/ctn_noisebudget/-/blob/master/BayesianAnalysis/CTN_BayesianAnalysis_Final.ipynb).
