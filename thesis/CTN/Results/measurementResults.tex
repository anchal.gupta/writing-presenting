\section{Measurement Results}
\label{sec:measResults}

\subsection{Measured noise and estimation of noise budget}
\label{subsec:measResults}


\input{CTN/figureTex/CTN_Noise_Budget}

Fig.~\ref{fig:ctn_nb} shows the measured beatnote spectrum (orange curve) along
with the estimated budget of different noise sources. For each curve, the
shaded region is a 68\% confidence interval estimated by measured noise or
propagating errors of initial parameter uncertainties. The beatnote fluctuation
\gls{ASD} measurement as the output of the \gls{DPLL} (see Sec.\ref{sec:bndet})
comes in units of $\text{Hz}/\sqrt{\text{Hz}}$ that is used on the left axis.
An equivalent displacement noise of the cavity length is calculated by
multiplying with $\frac{c}{L\lambda}$, to get the right axis in units of
$\text{m}/\sqrt{\text{Hz}}$. Here we measured a noise level of
$2\times10^{-18}\,m/\sqrt{Hz}$ at 200 Hz.

The black curve shows the estimated total noise of the experiment which
includes coatings Brownian noise (red curve). The level of the coatings
Brownian noise in this plot is adjusted to explain the observed total noise by
fitting the bulk loss angle value of the coatings (see
Sec.\ref{subsec:bayAnalysis} below).

The laser amplitude noise (blue) was measured by using the calculation
mentioned in Sec.\ref{subsec:photoThermal}. For $S_{RIN}$ in
Eq.~\ref{eq:photothermal}, the relative intensity noise of the transmitted
lasers were measured right before the beatnote measurement using two witness
photodiodes (i.e. they were out of the intensity stabilization servo loop).

The laser frequency noise (magenta) was estimated as described in
Sec.\ref{subsec:laserFreq}. For this estimation, the open loop transfer
function of the frequency stabilization servo measured earlier (see
Fig.~\ref{fig:FSS_OLTFs}) was used. The DC power levels on the RF photodiodes
were measured right before the beatnote spectrum measurement and were used to
scale any changes in the frequency suppression due to different circulating
laser power. However, this should be noted that the gain is high enough in the
frequency band of interest that the minor changes in circulating laser power
did not affect the performance of the loop much. Using the same DC power level,
the controls shot noise (grey curve) of the experiment was also estimated in
realtime (see Sec.\ref{subsec:shot})

Coating thermo-optic (dashed blue, see Sec.\ref{sec:coatTO}), substrate
Brownian (dashed orange, see Sec.\ref{sec:subBr}), and substrate thermoelastic
(dashed yellow, see Sec.\ref{sec:subTE}) noises were calculated using coatings
material parameters listed in table \ref{tab:nb_symbols}.

The sensing noise (green, see Sec.\ref{subsec:PLL}) of the measurement, and the
seismic noise coupling (brown, see Sec.\ref{subsec:seismic}) were measured
before the setup of science mode and were assumed to remain the same during the
entirety of the science mode observation run.

We believe the measured noise floor is the true noise floor of the experiment
because the measured noise level was not showing any reduction when the control
loop gains were increased further or the incident power on the cavities was
increased. This meant that the noise floor is due to a noise source that can
not be suppressed by the different control loops. The excess noise below 70 Hz
should be due to the scattered light inside the vacuum can reaching back to the
\gls{PDH} lock photodiode. Measured noise from 70 Hz to 600 Hz shows a
near-flat
dominant noise contribution which we believe is coming from the coatings
thermal noise.

\subsection{Bayesian analysis to infer loss angles}
\label{subsec:bayAnalysis}


With the parameters listed in Table.~\ref{tab:nb_symbols}, I derived an estimate
of all noise curves except for the coatings Brownian noise. Then, for any
assumed value of the bulk loss angle for coating layers (assumed to be the same
for GaAs and \AlGaAs), I can obtain an estimate of the total expected beatnote
frequency noise (depicted as the solid black curve in Fig.~\ref{fig:ctn_nb}).



\input{CTN/figureTex/likelihoodAnalysis}

The measured time-series data of the beatnote frequency was divided into
segments of 5s with half overlap. Welch function of the Python library signal
was used with the Hanning window to calculate a \gls{PSD} estimate for each of
these 5s long segments. This provided us with PSD with frequency bin widths of
0.2 Hz. Consecutive 5 bins were merged taking their median value to average out
correlations in the neighboring bins due to the Hanning window used. This gave
us 1 Hz frequency bins. Bins from 70 Hz to 100 Hz were chosen and the rest are
discarded.

For each bin, we had as many PSD estimates as the number of time segments we
created. We took the logarithm of these PSD (logPSD) estimates and fit the
histogram obtained to a skew-normal distribution to get an estimate of the
probability distribution of logPSD of beatnote frequency noise in each of the
frequency bins. An example of such a fit is shown in Fig.~\ref{fig:SkewNormFit}
for the frequency bin at 200 Hz.

\input{CTN/figureTex/SkewNormFit}

The same analysis as mentioned above is repeated with 0.5 s time-series
segments, which gave logPSD probability distribution for frequency bins spaced
at 10 Hz. We took bins from 100 Hz to 600 Hz from this set. This ensured that
the weightage of different decades of frequency noise data is roughly equal in
our noise analysis to get the correct coupling of power-law frequency
dependence of the noise.

Further, from the data set, we removed frequency bins coinciding with harmonics
of 60 Hz as this was a known source of noise due to leakage of AC power into
our control loop electronics. The section between 260 Hz and 290 Hz is also
removed as a known stationary noise peak which is always present.

With this, we have a distribution of logPSD of beatnote frequency for various
frequency values. We calculated the log-likelihood function for each of these
frequency values using the distribution fitted above and summed them with each
other to obtain the total log-likelihood value.

\input{CTN/figureTex/BayesianProbDist}

I did a grid search on possible values of the bulk loss angle between zero and
$1.6\times10^{-3}$ with a step size of $1\times 10^{-6}$ and calculated
likelihood probability for each value as shown in Fig.~\ref{fig:bayDist} using
the method described above. A prior distribution was assumed as shown in
Fig.~\ref{fig:bayDist} in the shape of a Gaussian centered at $5.33 \times
10^{-4}$ that was measured by Penn et al.\cite{Penn2019}. The Bayesian
probability distribution is calculated as the product of prior and likelihood
distribution. The Bayesian inferred value came out to be:
\begin{equation}
\BulkLAFit
\end{equation}
where the limits enclose a 90\% confidence interval. This value was estimated
with a shear loss angle value of $5.2 \times 10^{-7}$ taken from Penn et
al.~\cite{Penn2019} indirect measurements. Since the frequency dependence of
total noise on the shear loss angle is similar to that of the bulk loss angle,
we cannot fit two degrees of freedom into our result. Simulations done by Penn
et al.~\cite{Penn2019} suggest that shear loss angle value should be very low
in these coatings, so its contribution to the coatings Brownian noise is
assumed to be negligible as well. Hence, we decided to only fit for bulk loss
angle value using shear loss angle value as determined by the indirect
experiment.
