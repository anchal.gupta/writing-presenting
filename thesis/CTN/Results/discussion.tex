\section{Discussion with respect to other experiments}

The particular sample of \AlGaAs and GaAs coated mirrors that we had for this
measurement were only available to us. These coatings were specially designed
to cancel the thermo-optic effect that was experimentally observed in previous
experiment\cite{Tara2016}. So it is hard to compare the results of this
measurement with other experiments conducted in this field, but here I'll try
to cover a pertinent discussion about using these materials for a crystalline
coating candidate.

Penn et al.\cite{Penn2019} measured the bulk and shear loss angles indirectly
using Q measurements of disk resonators with or without a crystalline coating
of \AlGaAs \, and GaAs. They measured the bulk loss angle to be $5.33 \times
10^{-4}$ and $5.2 \times 10^{-7}$ but report that overall coating loss angle is
$4.78 \times 10^{-5}$. This indicates that their bulk and shear loss angle
definitions might be different from ours. Cole et al.\cite{Cole2013} also
measured similar coatings using a direct measurement setup in which a cavity is
made with the mirrors under test and compared with a ultra low expansion (ULE)
reference cavity at 698 nm through a frequency comb. They reported the loss
angle upper bound value to be $2.5\times10^{-5}$.

More recently, Yu et al.\cite{Yu2022} performed a more advanced experiment with
these coatings utilizing different polarizations of the laser resonating in the
same cavity. They were able to perform a more careful study of spatial
correlations of noise originating from the coatings which allowed them to
separate global displacement noise from local displacement noise. Since the
correlation length of coatings Brownian noise is very small, it appears as
spatially local noise in these measurements. They reported coatings Brownian
noise measurement at the same level as expected for a loss angle of
$2.5\times10^{-5}$ but they were able to see that after they removed global
excess noise of unknown origin present in these coatings. This global excess
noise seems to have the same frequency dependence as the coatings Brownian
noise (1/f in \gls{ASD}) but is about 30 times larger in \gls{PSD}.

Yu et al.\cite{Yu2022} also found intrinsic birefringence noise, attributed to
fluctuations in refractive index along one polarization axis. This intrinsic
birefringence noise added another factor of 10 to the observed \gls{PSD},
measured by comparing it against a polarization-averaged locking scheme. Thus,
overall the global excess noise and birefringence noise could cause about 300
times more observed noise in \gls{PSD}. If wrongly attributed all to the loss
angle of the coatings or if we call it an "effective loss angle", this would
result in a loss angle value to be about 17 times higher than what indirect
measurements measured.

In our direct measurement which can not distinguish between global excess
noise, birefringence noise on single polarization, and the coatings Brownian
noise, we get a loss angle value that is 15 times that measured by Penn et
al.\cite{Penn2019}. While we can not scientifically attribute our larger
measured loss angle to these effects, with the new information the experimental
setup can be improved to dig deeper into the noise floor and confirm Yu et
al.\cite{Yu2022} measurements.

\section{Possible future improvements to the experiment}

Hindsight is always 20/20. Learning from our experience, I think we can improve
this experiment in a variety of ways. The first obvious change to do is to
utilize the polarization averaged locking scheme devised by Yu et
al.\cite{Yu2022}. By sending the laser first to an \gls{AOM} and upshifting the
first-order beam by cavity birefringence splitting (the difference between the
resonance of the two polarization modes), we can cancel away the birefringence
noise at the \gls{PDH} error point.

Secondly, we can try to lock another laser to the same cavity in the same
polarization-averaged way but to TEM01 mode while the first laser is locked to
TEM10 mode. Just like Yu et al.\cite{Yu2022}, we would be able to get rid of
any common mode global excess noise in this measurement. We do not need two
cavities either for this setup, hugely reducing the complexity of the
experiment and the need of controlling the beatnote frequency through thermal
means.

Along with these major experimental design changes, many minor improvements can
also be done. Our vacuum can window were glass with anti-reflection (AR)
coatings but parallel surfaces. This made etalon effects feasible and the
possibility of backscattered light reflecting back towards the cavity polluting
the \gls{PDH} signal. Whenever possible, such vacuum can windows should be
wedged with an angle large enough to dump all scattered light to a designated
beam dump.

Another interesting change would be to implement a frequency discriminator
based on an optical delay line implemented on a thermally and mechanically
stabilized pool of optical fiber cable (also see Sec.\ref{sec:ODFD}). This
would allow us to measure the frequency noise without using an identical cavity
or additional laser. However, more work would be required to make sure that the
fiber noise does not dominate in this case.

We can also put coils around the cavity and maybe ferrite material to produce
magnetic fields to measure the effects of fluctuations in local magnetic fields
on the electro-optic coefficients of such crystalline coatings. Similarly, some
electrode plates can be placed as well to measure these effects for electric
field couplings.

\section{Future of crystalline coatings}

Crystalline coatings are an active topic of research and are promising for
achieving the target sensitivities for future gravitational wave detectors.
More study is required to understand the source of birefringence noise which is
still unknown. If polarization averaging can be done at the \gls{PDH} error
point, maybe some clever material optimization and/or coating structure
optimization can cancel this noise for crystalline materials within the coating
itself.

The global excess noise though needs to be addressed, otherwise using these
coatings would not be feasible for gravitational wave detection purposes. Since
this noise is directly observed but is not present in loss angle measurements
made with disk resonators, there is some indication that the optical contacting
of these crystalline coatings on an amorphous substrate is not as harmless as
thought earlier. This effect needs to be studied properly and mitigated if
possible. Maybe new growth recipes are required to meaningfully use the
low-loss crystalline coatings for ultra-precision measurements.

Finally, we need to improve our understanding of the coatings Brownian noise as
well, particularly for crystalline coatings where the material is not
necessarily isotropic and could be very anisotropic due to stress. We need to
understand any frequency dependence of the loss angle as well, and if that can
be utilized to further engineer better coatings for future gravitational wave
detectors.
