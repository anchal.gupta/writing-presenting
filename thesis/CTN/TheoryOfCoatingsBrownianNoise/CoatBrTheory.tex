\section{Calculating Coatings Brownian Noise} \label{sec:CoatBrTheory}

The mirrors in LIGO test masses are coated with alternative layers of high and
low refractive index materials. If the layers' thickness is such that the laser
wavelength undergoes $\pi/4$ phase shift, then a special condition arises: the
reflected electric field from each layer interface constructively interferes
while the transmitted fields interfere destructively. As a stack, this allows
the coating to provide very high reflection such as $99.9995\%$ as the case for
advanced LIGO coatings. High reflection is required to permit higher
circulation of power in the arm cavities so that the sensitivity to the
gravitational wave strain is more. Thus, the adoption of such highly reflective
mirrors is required for detecting gravitational waves.

But this advantage also results in an extra noise source. The thin coatings on
the mirror surfaces have vibrations originating purely because they are at a
finite temperature. This noise is called Coatings Brownian Noise as it is
similar to the Brownian motion of particles suspended in a liquid. Using the
fluctuation-dissipation theorem mentioned in the last section, we can estimate
the noise imparted on the phase of the reflected light from these coatings. In
this section, I will mostly use the analysis presented by Hong et al.
\cite{Hong2013} to explain the origin of this noise and the relationship of the
coatings' loss angle to it. This is an attempt to supplement their work by
laying out a coatings thermal noise calculation roadmap for experimentalists.
This work has been added to the Gravitational Waves Interferometer Noise
Calculation tool\cite{gwinc}.

Let's index the coating layers starting from the layer closest to the surface
as the $0$ layer and the last layer as the $N-1$ layer. In our notations, we
can consider the vacuum as $-1$ layer and the substrate of the mirror as layer
$N$. Then one can calculate the reflectivity of the interface from $(j-1)^{th}$
to $j^{th}$ layer:
\begin{equation}
r_j = \frac{n_j - n_{j+1}}{n_j + n_{j+1}}
\end{equation}
Next, we can calculate the one-way phase shift that occurs in light one passing
from $j^{th}$ layer is defined as:
\begin{equation}
\phi_j = 2 \pi \frac{d_j n_j}{\lambda}
\end{equation}
From these parameters, we define Transmission Matrix from $j^{th}$ layer and
Reflection Matrix at the interface from $(j-1)^{th}$ to $j^{th}$ layer as
\cite[Eq. 98, 99]{Hong2013}:
\begin{subequations}
\begin{align}
\mathbf{R_j} &= \frac{1}{\sqrt{1 - r_j^2}}\begin{pmatrix}
                                                1 & -r_j \\ -r_j & 1
                                          \end{pmatrix} \\
\mathbf{T_j} &= \begin{pmatrix}
                        e^{i \phi_j} & 0 \\ 0 & e^{-i \phi_j}
                \end{pmatrix}
\end{align}
\end{subequations}
These matrices transform onward and backward-going fields from the left side of
an optical element to the right side of it. Multiplying these matrices in order
of their appearance to light coming from the left, we get the total
transformation matrix of the coating as \cite[Eq. 101]{Hong2013}:

\begin{equation}
\mathbf{M} = \mathbf{R_N} \mathbf{T_{N-1}} \mathbf{R_{N-1}}
              ...
              \mathbf{R_1} \mathbf{T_0} \mathbf{R_0}
\end{equation}
Then the complex reflectivity of the entire coating stack can be written as
\cite[Eq. 102]{Hong2013}:
\begin{equation}
\rho = - M_{21} / M_{22}
\end{equation}


Note that we want to understand how changes in the individual layer parameters
like its thickness and refractive index propagate into changes to the overall
complex reflectivity of the entire coating stack. The layer thickness $d_k$ is
associated with the $\phi_k$, and layer refractive index $n_k$ is associated
with the interface reflectivity $r_{k-1}$ and $r_{k}$, so we will calculate the
derivatives of $\rho$ with respect to these variables. We define $\rho_k$ as
the complex reflectivity for $k$ layered stack with layers from $(N-k+1)^{th}$
layer to $N$th layer on top of the same substrate but the $(N-k)^{th}$ layer
material on top instead of vacuum. Then the total transformation matrix for
this k-layered stack would be:
\begin{equation}
\begin{aligned}
\mathbf{M_k} &= \mathbf{R_N} \mathbf{T_{N-1}} \mathbf{R_{N-1}}
               ...
               \mathbf{R_{N-k+1}} \mathbf{T_{N-k}} \mathbf{R_{N-k}}\\
\rho_k &= - \mathbf{M_k}_{21} / \mathbf{M_k}_{22}
\end{aligned}
\end{equation}
One can identify easily that:
\begin{equation}
\mathbf{M_{k+1}} = \mathbf{M_{k}} \mathbf{T_{N-k-1}} \mathbf{R_{N-k-1}}
\end{equation}
This allows us to write a recursion relation for $\rho_{k+1}$ as:
\begin{equation}
\rho_{k+1} = \frac{r_{N-k-1}+\rho _k e^{2 i \phi _{N-k-1}}}
                {1 + r_{N-k-1} \rho _k e^{2 i \phi _{N-k-1}}}
,\quad \rho_{0} = r_N
\end{equation}
We can use this to calculate the following derivatives:
\begin{equation}
\begin{aligned}
\frac{\partial\rho_{k+1}}
     {\partial\rho_k} &= \frac{\left(1 - r_{-k+n-1}^2-1\right)
                               e^{2 i \phi _{N-k-1}}}
                              {\left(1+\rho _k r_{-k+n-1}
                                     e^{2 i \phi _{-k+n-1}}
                               \right)^2}\\
\frac{\partial\rho_{k+1}}
     {\partial r_{N-k-1}} &= \frac{1-\rho _k^2 e^{4 i \phi _{-k+n-1}}}
                                  {\left(1+\rho _k r_{-k+n-1}
                                         e^{2 i \phi _{-k+n-1}}
                                   \right)^2}\\
\frac{\partial\rho_{k+1}}
     {\partial\phi_{N-k-1}} &= -2i\frac{\left(1 - r_{-k+n-1}^2-1\right)
                                        e^{2 i \phi _{N-k-1}}}
                                       {\left(1+\rho _k r_{-k+n-1}
                                              e^{2 i \phi _{-k+n-1}}
                                        \right)^2}
\end{aligned}
\end{equation}
From here, it is straightforward to calculate derivates of $\rho = \rho_{N}$ by
using (for $j\ge1$):
\begin{equation}
\begin{aligned}
\label{eq:rho_rk_phik}
\frac{\partial \rho}
     {\partial r_j} &= \left(\prod_{k=N-j}^{N-1} \frac{\partial\rho_{k+1}}
                                                         {\partial\rho_k}
                       \right)
                       \frac{\partial\rho_{N-j}}
                            {\partial r_{j}}\\
\frac{\partial \rho}
     {\partial \phi_j} &= \left(\prod_{k=N-j}^{N-1} \frac{\partial\rho_{k+1}}
                                                         {\partial\rho_k}
                          \right)
                          \frac{\partial\rho_{N-j}}
                               {\partial\phi_{j}}
\end{aligned}
\end{equation}
And for $j=0$:
\begin{equation}
\frac{\partial \rho}{\partial r_0} = 1 ,\quad
\frac{\partial \rho}{\partial\phi_0} = 0
\end{equation}
Now, let's try to see the connection of these derivatives to the amplitude and
the phase of the reflected light. Relative amplitude fluctuations of the
reflected light can be given by:
\begin{equation}
\frac{\delta E_{refl}}{E_{refl}} = \frac{\delta |\rho|}{|\rho|} =
\mathrm{Re}[\delta \log \rho]
\end{equation}
While the phase fluctuations of the reflected light are given by:
\begin{equation}
\delta \phi_{refl} = \delta \angle \rho = \mathrm{Im}[\delta \log \rho]
\end{equation}
Hong et al.\cite{Hong2013} further define functions $\xi(\vv{x})$ and
$\zeta\vv{x}$ to represent the phase fluctuations and amplitude fluctuations
as:
\begin{equation}
\xi(\vv{x}) + i \zeta(\vv{x}) = -\frac{i}{2k_0}\delta \log \rho(\vv{x})
\end{equation}
where $\vv{x}$ is a transverse vector for the location on the mirror surface.
These functions are expanded into individual contributions from $\phi_j$ and
$r_j$ using chain rule as:
\begin{equation}
\xi(\vv{x}) + i \zeta(\vv{x}) = -\delta z_s(\vv{x}) - \sum_{j=1}^{N}
\int_{z_{j+1}}^{z_j}
\left[
1 + \frac{i \epsilon (z)}{2}
\right]
u_{zz}(\vv{x}, z)
\end{equation}
Here $z_j$ is in the position of the interface between $j^{th}$ to $(j-1)^{th}$
layers, $z_s$ is the position of the substrate surface, $u_{zz}(\vv{x}, z)$ is
the longitudinal strain inside the coating layer, and $\epsilon (z)$ is defined
as \cite[Eq. 25]{Hong2013}:
\begin{equation}
\begin{aligned}
\epsilon_j(z) &= (n_j + \beta_j)
                 \frac{\partial \log(\rho)}{\partial \phi_j} \\
              &\quad
                 - \beta_j \left[
                             \frac{1 - r_j^2}{2r_j}
                             \frac{\partial \log(\rho)}{\partial \phi_j}
                             - \frac{1 + r_j^2}{2r_j}
                               \frac{\partial \log(\rho)}{\partial \phi_{j+1}}
                          \right] \\
              &\quad
                   \times cos[2k_0n_j(z-z_{j+1})] \\
              &\quad
                 - t_j^2 \beta_j \frac{\partial \log(\rho)}{\partial r_j}
                   sin[2k_0n_j(z-z_{j+1})]
\end{aligned}
\end{equation}
where $t_j = \sqrt{1 - r_j^2}$, and $\beta_j = \frac{\partial n_j}{\partial \log
l_j}$ is the elasto-optic coefficient for the $j^{th}$ layer.

The strain $u_{zz}(\vv{x}, z)$ is calculated by taking transfer functions from
the bulk and shear strain fields to layer thickness and surface height
\cite[Table. I.]{Hong2013}:
\begin{subequations}
\begin{align}
C_j^B     &= \sqrt{\frac{1+\sigma_j}{2}} \\ C_j^{S_A} &= \sqrt{1-2\sigma_j} \\
D_j^B     &= \frac{1-\sigma_s-2\sigma_s^2}{\sqrt{2(1+\sigma_j)}}
             \frac{Y_j}{Y_s} \\
D_j^{S_A} &= -\frac{1-\sigma_s-2\sigma_s^2}{2\sqrt{1-2\sigma_j}}
              \frac{Y_j}{Y_s} \\
D_j^{S_B} &= \frac{\sqrt{3}(1-\sigma_j)(1-\sigma_s-2\sigma_s^2)}
                  {2\sqrt{1-2\sigma_j}(1+\sigma_j)}
             \frac{Y_j}{Y_s}
\end{align}
\end{subequations}
The coatings material is assumed to have loss angles as the fractional
imaginary parts in the bulk and shear modulus of the coatings materials as:

\begin{equation}
\bar{K} = K(1 + i\Phi_B),\quad \bar{\mu} = \mu(1 + i\Phi_S)
\end{equation}
Calculations for the strain noise fields in Bulk and Shear mode for each
coating material is given by \cite[Eq. 96]{Hong2013}:
\begin{equation}
\label{eq:strainNoiseFields}
S_j^X = \frac{4 k_B T \lambda_j \Phi_j^X (1 - \sigma_j - 2\sigma_j^2)}
             {3 \pi f n_j Y_j (1 - \sigma_j)^2 \mathcal{A}_{eff}}
             ,\quad\quad X = B,S
\end{equation}
Finally, we define transfer functions from each layer's bulk and shear stress
noise fields to phase noise of reflected light \cite[Eq. 94]{Hong2013}:
\begin{subequations}
\begin{align}
q_j^B  &= \int\limits_{z_{j+1}}^{z_j}
          \frac{dz}{\lambda_j}
          \left[
              \left[
                  1 - \mathrm{Im} \left(\frac{\epsilon_j(z)}{2}\right)
              \right] C_j^B
              + D_j^B
          \right]^2 \\
q_j^S  &= \int\limits_{z_{j+1}}^{z_j}
          \frac{dz}{\lambda_j}
          \left[
              \left[
                  1 - \mathrm{Im} \left(\frac{\epsilon_j(z)}{2}\right)
              \right] C_j^{S_A}
              + D_j^{S_A}
          \right]^2 + [D_j^{S_B}]^2
\end{align}
\end{subequations}
and to the amplitude noise of the reflected light as \cite[Eq. 95]{Hong2013}:

\begin{subequations}
\begin{align}
p_j^B  &= \int\limits_{z_{j+1}}^{z_j}
          \frac{dz}{\lambda_j}
          \left[ C_j^B \mathrm{Re} \left(\frac{\epsilon_j(z)}{2}\right)
          \right]^2 \\
p_j^S  &= \int\limits_{z_{j+1}}^{z_j}
          \frac{dz}{\lambda_j}
          \left[ C_j^{S_A}
              \mathrm{Re} \left(\frac{\epsilon_j(z)}{2}\right)
          \right]^2
\end{align}
\end{subequations}
This allows us to write the single-sided power spectral density of the
displacement noise of the coatings (due to phase noise in reflected
light)\cite[Eq. 94]{Hong2013}



\begin{equation}
S^z_\text{coatBr} = \sum_j \left(q_j^B S_j^B + q_j^S S_j^S\right)
\end{equation}
And the single-sided power spectral density of the recoil of the mirror (due to
amplitude noise imparted on the reflected light) \cite[Eq. 95]{Hong2013}:
\begin{equation}
S^z_\text{coatBr} = \sum_j \left(p_j^B S_j^B + p_j^S S_j^S\right)
\end{equation}
Note that both expressions above give noise in units of $\text{m}^2/\text{Hz}$.
While it is trivial to understand how reflected phase noise generates an
equivalent displacement noise, one would require using the mirror's overall
mechanics to understand if the second contribution is meaningful or not. Hong
et al.\cite{Hong2013} provide an interesting discussion on this topic if anyone
is interested, but for the coatings, we have considered so far, the
coefficients $p_j^B$, $p_j^B$ are 5 to 6 orders of magnitude smaller than
$q_j^B$, $q_j^B$ and thus the amplitude noise due to coatings thermal noise can
be neglected. For further discussions and more details on calculations of the
transfer functions and strain noise fields mentioned above, please refer to
Hong et al.\cite{Hong2013}.
