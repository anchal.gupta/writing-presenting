\section{Fluctuation-Dissipation Theorem} \label{sec:FDT}

In our physics major education, we are taught how temperature is a measure of
the average internal motion of a body, whether it is a collection of gas,
liquid, or solid. The motion itself is supposed to be random and unpredictable,
while we know the mean distribution of the particle kinetic energies or
vibrational energies.

For fluids in particular, the seminal paper by Einstein puts a vivid picture of
particle diffusion in fluids that is seen through microscopes. For a particle
suspended in a liquid, the higher the temperature, the faster the diffusion of
particle will happen through the fluid volume, and the more viscous the fluid
is, the slower the diffusion would happen. This gives us important insight on
the relationship of the degree of such brownian motion and the environment
properties like temperature and frictional losses.

Fluctuation-dissipation theorem\cite{Callen1952} generalizes this relationship
by stating:

\begin{center}


"Given that a system obeys detailed balance, thermodynamic fluctuations in a
physical variable at equilibrium predict the dissipation of energy in the
dynamics of the same physical variable, and vice versa."

\end{center}

This means that the channels or pathways through which a system dissipates
energy to the environment are used in reverse when the system is at
equilibrium but receives noise from the environment through the same channels.

In more technical terms, one can find out the frequency distribution of noise
that an environment would cause to a system through the dissipation rate of
energy in that system when oscillated at particular frequencies. For example,
take a mechanical system of admittance $Z(f)$ for a parameter $x$, where the
imaginary part of the impedance shows the reactance of the system and the real
part shows the dissipation of the system.


\begin{equation}
\tilde{v}(f) = i \omega \tilde{x}(f) = Z(f) \tilde{F}(f)
\end{equation}


Then, at equilibrium at temperature T, the single-sided power spectral density
of the noise of the system is given by:


\begin{equation}
S_x(f) = \frac{k_B T}{\pi^2 f^2} \mathrm{Re}[Z(f)]
\end{equation}


This gives experimentalists access to predict and estimate the Brownian motion
of this system, by measuring the mechanical impedance $Z(f)$. One can force the
system at frequency $f$, and magnitude $F_0$ and observe the amount of energy
dissipated by the system $W_{diss}$ in one period of oscillation. Then the real
part of admittance is:


\begin{equation}
\mathrm{Re}[Z(f)] = \frac{4 \pi f}{F_0^2}W_{diss}
\end{equation}


The dissipation can be written as an angular fraction $\phi$ of the elastic
energy stored in a single cycle of oscillation $U$ as:


\begin{equation}
\mathrm{Re}[Z(f)] = \frac{4 \pi f}{F_0^2}U \phi
\end{equation}


Such angular loss fraction is known as the loss angle of the admittance and
will be a quantity of interest in future chapters. Note that another way to
measure mechanical admittance is to measure the impulse response of the system.
One can measure the quality factor of the decay of the system and the loss
angle is inversely proportional to this quality factor:


\begin{equation}
\phi = \frac{2\pi}{Q}
\end{equation}
