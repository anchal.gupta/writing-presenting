\chapter{Introduction to Coatings Brownian Noise}
\label{ch:TheoryBrownianNoise} \def \Theory {CTN/TheoryOfCoatingsBrownianNoise}

The astounding $10^{-19}\text{m}/\sqrt{\mathrm{Hz}}$~ displacement sensitivity
of advanced LIGO is achieved by using high circulating power in the 4-km arms.
Higher circulating power means more photon averaging while reading the
gravitational wave signal. This is achieved by utilizing special high
reflectivity Bragg coatings on the test masses, with the End Test Mass mirror
coating achieving 5 ppm transmission (that is 99.99975\% reflectivity) for 1064
nm wavelength laser.

Presently these mirror coatings are made of alternate layers of amorphous
silica and titania-doped tantala~\cite{Harry2006}. While this reduced the noise
floor in AdvLIGO substantially, the 10-500 Hz region of the design sensitivity
in AdvLIGO is limited by the coatings Brownian noise~\cite{Martynov2016}.
Coatings Brownian noise is the result of fluctuations in the material by virtue
of it being at a finite temperature. Various methods are being pursued to
reduce this noise~\cite{Steinlechner2018}. These include new coating
structures~\cite{Yam2015, Steinlechner2015, Steinlechner2016, Pan2014} and
novel materials~\cite{Steinlechner2018aSi, Pan2018, Cumming2015, Cole2013} for
the coating. The fluctuation-dissipation theorem relates the energy dissipation
in a mechanical system with its equilibrium noise
fluctuations~\cite{Callen1952}. The disordered structure of amorphous materials
leads to mechanical loss peaks at low temperatures~\cite{Pohl2002}. The lack of
strong cohesion and order between the molecules of the material result in
internal friction that leads to Brownian noise. Hence, it is suggested that
crystalline materials might be able to provide lower Brownian noise solutions.

Cole et al.~\cite{Cole2013} fabricated a coating with crystalline \AlGaAs and
GaAs, and used substrate-transfer with optical bonding to apply the coating to
a fused silica substrate. Preliminary results suggest that this material choice
can improve Brownian noise performance. Penn et al.~\cite{Penn2019} performed
indirect noise measurement for this coating structure by doing mechanical
ring-down measurements of silica discs, with and without these coatings
applied. This provides an estimate of the loss angles of the coating.
Chalermsongsak et al.~\cite{Tara2016} optimized the coating structure made with
these materials to achieve coherent cancellation of thermo-optic noise and
demonstrated it as well.

In this part of my research, I delved into directly measuring the phase noise
on light due to such crystalline coatings. We inherited the experiment
Chalermsongsak et al.~\cite{Tara2016} used to prove thermo-optic cancellation
in these coatings, which I, with help from Andrew Wade, and Craig Cahillane,
took to improved sensitivity limit, so that we can observe their coatings
Brownian noise.

Our experiment complements the work in Penn et al.~\cite{Penn2019} as we can
backtrack the same loss angle values using the theoretical model of Hong et
al.~\cite{Hong2013}. This helps us not only in generating more information
about these particular crystalline coatings but also understanding the nature
of actual phase noise reflected by optically bonded coatings with small beam
areas and verifying the theoretical understanding we have so far.

In this chapter, I will explain how fluctuation-dissipation theorem formalizes
the relationship between internal friction in a material and the noise fields
that emerge through interaction with environment. Then, we will take the case
of a mirror with a Bragg coating on it, and see how brownian motion of the
mirror surface and coating layers are related to their mechanical properties.
Then, we'll look into how this affects the light that reflects off the mirrors
and becomes a noise source for gravitational waves detectors.

\input{\Theory/FDT.tex}

\input{\Theory/CoatBrTheory.tex}

\input{\Theory/AlGaAsIntro.tex}
