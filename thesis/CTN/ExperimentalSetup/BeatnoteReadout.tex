\section{Beatnote Detection and Readout}
\label{sec:bndet}

The transmitted laser beams from the cavity carry displacement noise of the
mirror surface as frequency noise because of the strong \gls{PDH} locking
explained above. The main purpose of having two equivalent paths is so that we
can refer them against each other to measure their differential cavity length
noise imprinted on their optical frequencies. The two beams are interfered with
each other on a 50:50 beamsplitter and one of the outputs is read by a
custom-built RF photodiode resonant at 27.34 MHz.

The resulting heterodyne beatnote is tracked by a digital phase-locked loop
(DPLL) with 10 kHz bandwidth. This DPLL is implemented on an \gls{FPGA} inside
the Liquid Instruments' Moku:Lab which in turn records the beatnote frequency
at 15.625 kSa/s. The digital implementation allowed for a larger dynamic range
readout of the frequency with high precision. The clock of Moku was referenced
to an SRS FS725 Rubidium atomic clock 10 MHz signal for reducing sensing
frequency noise and increasing stability over long durations.

\subsection{Vacuum can temperature control} \label{subsec:VacCanTempCtrl}

\input{CTN/figureTex/CanTempTimeSeries}

To ensure the \gls{PLL} tracked the heterodyne signal with good linearity, we
made sure that the beatnote frequency did not drift more than the bandwidth
capabilities during the measurement time, which was typically 960 s. For this,
the cavity temperature needed to be controlled. The cavity temperature control
required a uniform and constant temperature environment to cool off radiatively
since we only employed heating actuation on cavities as explained in
Sec.\ref{subsec:CavTempCtrl}. Thus the cavities were enclosed inside a vacuum
can that was thermally insulated with 2-inch CERTIFOAM 25. This foam enclosure
was further covered with aluminum tape to increase the reflectivity of the
surface.

The temperature of the vacuum can is monitored with an Analog Devices AD590
sensor through custom-built low noise temperature sensing
circuits~\cite{D1800304}. This circuit was designed to have mK sensitivity to
temperature changes. The temperature signal was sent to an ADC and controlled
by a Python \gls{PID} script. The control output was used to heat the vacuum
can using OMEGALUX silicone rubber heaters wrapped around it. This provides
temperature stability of $\pm30~\text{mK}$ in the cavity environment (see
Fig.~\ref{fig:VacCanTempCtrl}).

\subsection{Cavity temperature control} \label{subsec:CavTempCtrl}

\input{CTN/figureTex/CavTempTimeSeries}

With a stable environment for the cavities, we needed to control the beatnote
frequency value to keep it parked at the center of the resonant photodiode
frequency (27.34 MHz). This was achieved by differentially heating the
cavities.

The cavities were wrapped by nichrome wires which were used to actuate length
changes to the cavity spacer by heating. The difference frequency is captured
before cavities right after the laser heads on a wide bandwidth New Focus 1611
photodiode and read by a Mini-Circuits UFC-6000 frequency counter. This
pre-cavity beatnote frequency is used by a Python \gls{PID} script to control
and limit the relative temperature of the cavities by controlling the currents
in the nichrome wires via a custom-built MOSFET current driver circuit. Using
the laser temperature control signal as a witness detector, we estimated the
temperature fluctuations of the individual cavities and found it to be less
than $\pm30~\text{mK}$ (see Fig.~\ref{fig:CavTempCtrl}). All these efforts
resulted in a stable beatnote frequency with a drift of less than 1 kHz in more
than 20 minutes.
