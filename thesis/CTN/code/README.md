## Running code for figure plotting

* Run following to create cit_ctn conda environment:
```
cd code
conda env create --file cit_ctn_algaas_conda_end.yml
```
* Activate cit_ctn environment and create ipython kernel with it:
```
conda activate cit_ctn
python -m ipykernel install --user --name=cit_ctn
```
* While opening jupyter notebooks, change kernel to cit_ctn by dropdown menu:
Kernel>Change Kernel>cit_ctn (if not already selected)
