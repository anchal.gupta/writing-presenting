from gwinc.ifo.noises import *
from gwinc.ifo import PLOT_STYLE


class aLIGO_AlGaAs(nb.Budget):

    name = 'Advanced LIGO with AlGaAs Coatings'

    noises = [
        QuantumVacuum,
        Seismic,
        Newtonian,
        SuspensionThermal,
        CoatingBrownian,
        CoatingThermoOptic,
        SubstrateBrownian,
        SubstrateThermoElastic,
        ExcessGas,
    ]

    calibrations = [
        Strain,
    ]

    plot_style = PLOT_STYLE
