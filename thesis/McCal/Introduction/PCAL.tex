\section{Current calibration methods and proposals}

\subsection{Photon calibrator}
\label{subsec:PCAL}

Advanced LIGO uses a technique known as Photon Calibrator (PCAL)
\cite{Karki2016}. The objective of calibration is to send a known input signal
to the detector as a \gls{DARM} strain and measure the detector output of the
signal to get the response function of the detector. The input signal should be
strong enough to give a good signal-to-noise ratio from the detector output.
But it can not be too strong, otherwise the error readout for the \gls{DARM}
length control signal in the detector would become non-linear causing errors in
the calibration.

To get around this, the PCAL method injects input signals only at specific
calibration frequencies as sinusoidal waves and calibrates the response
function at those frequencies. A detector model is used to interpolate the
calibration at all other frequencies. Since the response function of the
detector depends on a few parameters, only 4 or 5 calibration lines are enough
to get these parameter values accurately and track their changes during the
observation. Using discrete frequencies also allows for the calibration to be
run while in observation mode, damaging sensitivity in only small frequency
regions around the calibration line frequencies.

\input{McCal/figureTex/PCAL}

PCAL method uses a 1047 nm laser beam that is reflected off of one of the
suspended test masses. The radiation pressure of the laser beam moves the
suspended test mass as a function of the laser power. The PCAL system modulates
the power of the beam to inject motion at certain calibration line intensities.
This creates motion in one of the mirrors in one of the arm cavities,
modulating the length of that arm cavity, and thus creating a \gls{DARM} and a
CARM signal simultaneously. To ensure that the beam does not interfere with the
thermal state of the coatings on the test mass, the beam incidence point is
kept off-center. But then to ensure no angular motion of the optic is
stimulated, another identical beam is sent at the diametrically opposite point
on the other side of the center.

This takes care of how the actuation is done on the mirrors for simulating a
signal. But to calibrate, we need to know how much the mirror moved from the
power modulation on the beam. For this purpose, part of the laser power is
measured in the transmitter module before being sent to the optic, and the
reflected light is measured after it reflects off the optic. For measuring this
light level, \textit{Gold Standard} power sensors are used that are calibrated
annually at the National Institute of Standards and Technology (NIST) in
Boulder, CO. From the two measurements of light power level before and after it
hits the mirror, photon radiation pressure on the mirror is estimated and then
using an estimate of the mechanical transfer function from force to
displacement, the actual motion of the optic is estimated. This is then used to
calibrate the \gls{DARM} output of the detector.

While this method works well for the desired uncertainty levels for the
Advanced LIGO sensitivity, it can not be improved further as much for the
next-generation detectors. In the best case scenario, the measured \gls{DARM}
strain uncertainty achieved by this method is in the range of 2\% to 5\% as
reported for the first and second observation runs of Advanced LIGO
detectors\cite{Cahillane2017}. The main reason for this high uncertainty
despite very low characterized uncertainty ($\sim$ 0.75\%) in the photon
calibrator is due to the observed drift of the optomechanical response function
in between each calibration run. This raises the issue of systematic
calibration error and a more complicated topic of how to infer accurate
calibration information across the detection frequency band with the few lines
of calibration that are used during observation.

The major components in the uncertainty of the photon calibrator measurement
come from laser intensity noise, photodiode calibration uncertainty, and
rotation of the optic due to the imprecise location of the beam spot on the
mirror. Besides these, there is a lot of room for systematic noise to creep in
since an estimate of the mechanical transfer function is used. The large optics
have vibration modes in a few kHz frequency range. These resonances could get
rung up because of the localized forcing from the photon calibration beams.
Because of these reasons, the estimates of laser power calibration and beam
spot position on the mirrors vary over time and result in systematic errors.
This method converts the \gls{DARM} response function calibration problem to
calibration of laser power measurement and precise estimation of beam location
and mechanical transfer function, which are easier problems but have limits on
how well one can estimate them. Thus there is a need for improving the
calibration method for the next generation of gravitational wave detectors.

\subsection{Newtonian calibrator}
\label{subsec:NCAL}

\input{McCal/figureTex/NCAL}

Another way to apply a known force on the test mass is to apply it using a
modulating gravitational force. This method is used in the Virgo gravitational
wave detector in Itay\cite{Estevez2021} for calibration, and used in
conjunction with the photon calibrator method in KAGRA gravitational wave
detector in Japan\cite{Inoue2018}. In this method, a rotating distribution of
accurately known masses is used to apply a modulating gravitational wave force
on the test mass from outside the vacuum chamber. Depending on the distance $d$
from the rotor (see Fig.~\ref{fig:NCal}), different multipole moments of mass
distribution apply the gravitational force with different amplitudes and at
different harmonics of the rotation frequency. This method has been
successfully tested and characterized recently at Advanced LIGO\cite{Ross2021}.

Since the actuation method does not involve anything touching the test mass,
not even photons, and is applied to the whole mass rather than at small regions
on the surface, this method has several advantages over the photon actuators
used in the photon calibrator method. There is no thermal distortion due to the
absorption of the actuation laser, no known excitation of higher order modes,
and no spurious rotation effects due to beam localization. The initial results
of this method\cite{Ross2021} suggest a less than 1\% uncertainty in
calibration which is statistical noise limited in measurement.

But the accurate estimation of applied force and displacement response to the
applied force remains a possible systematic source of uncertainty in the
future. Careful analysis of the effect of the modulating gravitational field on
the penultimate mass in the suspension chain and interaction with the test mass
through the suspension wires is required and modeled using finite element
analysis simulations. The Newtonian calibrator also applies a modulating torque
on the test mass causing it to move in angular degrees of freedom as well. The
length changes in the interferometer arm due to these rotations are calculated
by using beam position measurement on the test mass. This is then corrected in
the measurement. The beam position on the optics is found to move around by
about 0.5 mm over the course of hours\cite{RanaSaid}. This would result in a
systematic error in calibration of the order of 3-4\% assuming the same beam
offset positions as reported in O3\cite{Ross2021}. Thus, it might be required
to measure the beam position frequently to correct this systematic error.

The rotation of the rotator near the vacuum chamber inevitably transfers
vibration through the ground to the optical table inside even through the
seismic isolation system. Other bad effects like vibrations in the beam tube
and chamber walls cause spurious scattering that can increase noise in the
interferometer operation. This creates practical challenges in improving this
technique further. Calibrating the detector using this method at high
frequencies is hard to achieve as it would mean rotating the rotor at higher
speeds quietly. Further, it can only simultaneously calibrate at two or three
frequencies. Nevertheless, this is another exciting calibration method that
with more development can help reduce the calibration uncertainty in future
gravitational wave detectors.
