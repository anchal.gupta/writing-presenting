\section{Astrophysical and cosmological parameter estimation}
To get a bird's eye view of what is at stake due to calibration uncertainty, it
is important to know possible astrophysical and cosmological measurements that
can be made using gravitational waves. We list a few such parameter estimation
problems that can benefit from improved sensitivity of future-generation
gravitational wave detectors if the calibration uncertainty is reduced as well.

\subsection{Binary black hole mass and spin distribution}
With more than 90 detections of binary black hole mergers, we now have a
measured distribution of mass and the effective spin of the black holes\cite
{Abbott2019b}. This data helps us determine the formation channels for such
black holes and the origins of the blackhole population in general. Calibration
uncertainty in amplitude and phase directly affects the uncertainty in measured
estimates of black hole masses and effective spins. Further, since these
parameters affect the gravitational waveform in the first-order dynamics, the
post-Newtonian correction terms rely on the powers of the blackhole masses and
effective spins. Thus, it is important to measure these parameters with good
certainty to avoid increasing uncertainty in derived quantities or parameters
that affect higher-order post-Newtonian correction terms.

\subsection{Neutron star equation of state}
The neutron star equation of state has various candidate models based on
nuclear model physics or phenomenological models. One of the determining
parameters among the various proposed equation of state is the dimensionless
tidal deformability ($\Lambda$) of neutron stars\cite{Chatziioannou2020}. When
binary neutron stars are about to merge, they tidally deform each other
inducing quadrupole moments in each neutron star. The strength of this
quadrupole moment and its evolution is determined by the tidal deformability
factor $\Lambda$. The deformed neutron stars change the gravitational wave
phase evolution in a characteristic way that can be measured with accurate GW
phase information. Chatziioannou\cite{Chatziioannou2020} has shown tidal
deformability constraints on neutron star equation of state based on a study of
the only confirmed binary neutron star merger detected by GW detectors
GW170817\cite{GW170817}, and for another likely binary neutron star merger
GW190425\cite{GW190425}. This study also concludes that more detections with
future gravitational waves would be helpful but the tidal deformability could
be limited by detector calibration uncertainty and systematic waveform
uncertainty\cite{Dudi2018, Samajdar2018}.

\subsection{Tests of general relativity}
Gravitational wave measurements allow a unique way to test predictions of
general relativity. Many studies have tested general relativity on all the
gravitational wave signals measured so far\cite{GRtest1, GRtest2, GRtest3,
Abbott2021c}, but none have been able to find any significant deviations. The
simpler form of these tests looks for correlations in the residual data when
waveform has been subtracted from a measured gravitational wave. Other more
involved tests look for departure in the gravitational waveform or the
estimated parameters in higher order post-Newtonian terms in different
frequency bands. These tests look for the same information from different parts
of the event such as the inspiral time, merger, and ringdown after merger. With
higher sensitivity detectors in the future and better calibration on them, we
hope to find new physics through such tests on a larger set of observations.

\subsection{Hubble constant estimation}
The Hubble constant has been famously in tension due to conflicting
measurements that are now at least 4.4 standard deviations
apart\cite{Riess2019}. On one side are the measurements based on X-ray
observations of galaxy clusters and the spectral distortion of the cosmic
microwave background with the Planck observatory, which utilizes the
$\Lambda$CDM model to estimate the value of the sky. This method estimates the
Hubble constant in the early universe to be $67.4 \pm 0.5$ km s$^{-1}$
Mpc$^{-1}$\cite{Planck2020}. It is in agreement with other high redshift
measurements based on "inverse distance ladder" \cite{Macaulay2019} or with
baryonic acoustic oscillation measurements\cite{Addison2018}. On the other
hand, Hubble constant measurement made in the local universe by measuring
Cepheid variable stars in the Large Magellanic Cloud gives a value of $74.03
\pm 1.42$ km s$^{-1}$ Mpc$^{-1}$\cite{Riess2019}. This is again corroborated by
several other measurements in the local universe such as gravitational lensing
in H0LiCOW\cite{Birrer2019}, and supernovae in NIR\cite{Dhawan2018, Burns2018}.
There has been a lot of research on this topic which I cannot fully list here,
but we have found an interesting avenue for the possibility of finding new
physics.

Gravitational waves can be used to calculate the Hubble constant value as well.
When two compact binary objects coalesce together, the frequency evolution of
the gravitational wave signal gives information about the "Chirp Mass" of the
compact binary given by $\mathcal{M} = \frac{(m_1 m_2)^{3/5}}{(m_1 +
m_2)^{1/5}}$. The amplitude of the gravitational waveform also depends on the
same chirp mass, and thus one can infer how much the gravitational wave reduced
in amplitude when it was detected on Earth. Since gravitational waves also
reduce in amplitude as $D_{\mathrm{L}}^{-2}$ where $D_{\mathrm{L}}$ is the
luminosity distance traveled by the gravitational wave, we can estimate the
luminosity distance of the source. Thus gravitational waves serve as a standard
siren for luminosity distance which can be used along with other methods of
estimating the redshift of the source to measure the Hubble constant.

In particular, the binary neutron star detection GW170817\cite{Abbott2017} was
also detected in multiple electromagnetic transient signals in different
frequency bands. The redshift value of the source inferred from these
electromagnetic detections was used to determine the Hubble
constant\cite{Hubble1} without using conventional cosmic 'distance
ladder'\cite{Freedman2001} for the luminosity distance. However, since it is a
single event, the uncertainty in the measurement is not enough to settle the
Hubble tension.

Other ways to determine the redshift of the gravitational wave source have been
used to utilize a larger set (47) of binary blackhole
mergers\cite{Abbott2021b}. In this study, 47 gravitational wave sources were
used. The redshift of the sources was determined by either using sky
localization to determine the host galaxy of the event or by using a population
model of the mass distribution of black holes. The Hubble constant from this
analysis came out to be $68^{+8}_{-6}$ km s$^{-1}$ Mpc$^{-1}$. The poor
constraints are due to the fact that sky localization is not very good with
present detectors and that galaxy catalogs are not complete. This study
included the binary neutron star detection GW170817\cite{Abbott2017} which
alone provides almost the same confidence in the Hubble constant values as
estimated by the 46 other sources.

The uncertainty in the sky localization of the events is not entirely dependent
on a single detector calibration but would be improved if all detectors improve
in sensitivity and calibration precision. It is important to not miss follow-up
electromagnetic confirmations of future binary neutron star mergers due to poor
sky localization. On the other hand, the uncertainty in luminosity distance
measured by gravitational waves directly contributes to the uncertainty of the
estimated Hubble constant, and thus is an important parameter to estimate with
more precision.
