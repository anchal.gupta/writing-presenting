\chapter{Introduction}

Advanced LIGO has detected more than 90 compact binary mergers of Black holes
and neutron stars in distant galaxies so far\cite{Abbott2021d}. This incredible
feat is achieved by keeping the noise floor of the detector down to $10^{-20}
\mathrm{m}/\sqrt{\mathrm{Hz}}$. While we have started observing these events
and measuring some properties of the merging bodies, science does not stop at
merely proving that such bodies merge. We need to collect more data on the
rates of mergers, and more importantly, precise information on the merging
objects and the merging dynamics. That is, we need our detector output to be
accurate and precise about the astrophysical and cosmological parameters that
we estimate from the merging events.

Suppose gravitational wave $h_{\mathrm{GW}}$ from a merging event is arriving
at the detector. The detector measures the strain and converts it into a
digital count in its computer. This conversion in the Laplace domain can be
written as the detector response function $\mathcal{R}(s)$ such that:

\begin{equation}
\tilde{Y}_{out}(s) = \mathcal{R}(s) \tilde{h}_{\mathrm{GW}}(s)
\end{equation}

We need to know the response function of our detector accurately and with high
precision so that we can multiply the detected counts with the inverse of our
response function model to get back the gravitational wave strain that must
have come to the detector.

\begin{equation}
\tilde{h}_{\mathrm{meas}}(s) = \mathcal{R_{\mathrm{model}}}^{-1}(s)
                               \tilde{Y}_{out}(s)
\end{equation}

This measured strain $h_{\mathrm{meas}}$ is matched with different model
waveforms of compact binary mergers to determine if an event has been detected
and to estimate the parameters of the merging bodies.

Thus it is important to accurately and precisely estimate $\mathcal{R}(s)$ for
the detector. If the signal is strong enough, we will be able to distinguish it
as an event, but the uncertainty in the measured signal grows with the signal
strength if the uncertainty in $\mathcal{R_{\mathrm{model}}}$ remains the same.
Thus, it is possible that for a strong signal the uncertainty in the measured
parameters of the events is dominated by uncertainty in
$\mathcal{R_{\mathrm{model}}}$, rather than the statistical noise of the
detector. This is the worst place for a metrological experiment to be in: being
limited by the knowledge of the response function of your detector rather than
the noise floor of the detector.

On the other hand, there are some continuous gravitational wave sources like
pulsars which require long time averaging (over years) of the detector output.
Such sources might be missed if the uncertainty in the calibration of
$\mathcal{R_{\mathrm{model}}}$ is high.

Advanced LIGO currently reports the uncertainty in the response function phase
and amplitude ranging from 2\% to 5\% in the detection frequency
band\cite{Cahillane2017}. This can be interpreted as the measurement
uncertainties are limited by the detector noise for events with SNR up to about
50. If the next generation of gravitational waves detectors improve their
sensitivity, they would need to measure the response function with better
uncertainty to remain unaffected by calibration uncertainties. In this part of
my research, I focused on working on a calibration technique that less
sensitive to systematics, and scalable in achieving a desired calibration
uncertainty. We set our goal to perform calibration with less than
0.1\% uncertainty to show the proof of principle.

In this chapter, we'll go through a quick look at how calibration uncertainty
affects astrophysical and cosmological parameters estimation and how the
calibration is currently done in Advanced LIGO.

\input{McCal/Introduction/AstroCosmo}

\input{McCal/Introduction/parameterEstimation}

\input{McCal/Introduction/PCAL}
