\section{Calibration requirements for astrophysical parameter estimation}
\label{sec:parameterEstimation}

While determining the best strategy for calibration of the detector, it might
seem that determining the detector parameters to a better certainty would be
the goal. But as the end goal is to determine the astrophysical parameters
better, the calibration method should be focused on achieving that. We
performed the following Fisher information analysis adopted from Hall et
al.\cite{Hall2019} to estimate how the
calibration uncertainty propagates to astrophysical parameter estimation.

\subsection{Framework}

Suppose a frequency-domain waveform $h$ is described by a set of parameters
$\vect{\Theta}$, $h=h(f; \vect{\Theta})$, and the detector's strain noise PSD
is given by $S_{det}$ which has units of
$[{\mathrm{strain}}^2\,{\mathrm{Hz}}^{-1}]$. The lower bound on the parameter
estimation uncertainties on $\vect{\Theta}$ can be obtained by inverting a
fisher matrix $\vect{I}^{\mathrm{ast}}(\Theta)$ as
${\mathrm{Cov}}(\vect{\Theta}) \ge \left[\vect{I}^{\mathrm{ast}}
(\vect{\Theta}) \right]^{-1}$. In particular, the $(i, j)$th element of this
Fisher matrix is given by:

\begin{equation}
\vect{I}^{\mathrm{ast}}_{ij} = \left(
                              \frac{\partial h}{\partial \Theta_i}
                              \Big{|}
                              \frac{\partial h}{\partial \Theta_j}
                              \right)
\label{eq:fisher_ast}
\end{equation}

where the derivatives are evaluated at $\vect{\Theta}_{\mathrm{t}}$, the true
parameters describing the signal $h$, and we have defined the inner product as:

\begin{equation}
\left(a | b\right) = 4 {\mathrm{Re}}
                     \left[
                     \int df \frac{a^\ast(f) b(f)}{S_{det}(f)}
                     \right]
\end{equation}

As shown in Ref.~\cite{Hall2019}, when the calibration of the interferometer is
uncertain, we can define the ``measured'' waveform as:

\begin{equation}
g(f; \vect{\Theta}, \vect{\Gamma}) = \frac{
                                           R(f; \vect{\Gamma})
                                           }{
                                             R(f; \vect{\Gamma}_{\mathrm{t}})
                                             }
                                      h(f; \vect{\Theta})
\end{equation}

where $R$ is the response function (with units of strain/W) that converts the
detector output power fluctuations to the DARM strain, and
$\vect{\Gamma}_{\mathrm{t}}$ are the true parameters describing the
interferometer's calibration.

Using $g(f; \vect{\Theta}, \vect{\Gamma})$ as the combined model of strain
regeneration after detection, we can create a complete Fisher information
matrix $\vect{I}^{\mathrm{t}ot}$ given by:

\begin{equation}
\vect{I}^{\mathrm{t}ot}_{ij} = \left(
                               \frac{\partial g}{\partial \mu_i}
                               \Big{|}
                               \frac{\partial g}{\partial \mu_j}
                               \right),\quad \mu \in \{\Theta, \Gamma\}
\label{eq:fisher_tot}
\end{equation}

This matrix holds complete information on uncertainty propagation between
calibration uncertainty and the astrophysical parameter estimation
uncertainties, as well as, the lower bound on the astrophysical parameter
estimation uncertainties due to statistical noise. If all astrophysical
parameters appear first in the indices and then all calibration parameters
appear in the indices, the total fisher matrix has the following shape.

\begin{equation}
\vect{I}^{\mathrm{t}ot}_{ij} =
      \begin{pmatrix}
           \begin{pmatrix}
           . & . & . & . & . & . & . \\
           . & . & . & . & . & . & . \\
           . & . & . & . & . & . & . \\
           . & . & . & \vect{I}^{\mathrm{ast}} & . & . & . \\
           . & . & . & . & . & . & . \\
           . & . & . & . & . & . & . \\
           . & . & . & . & . & . & . \\
           \end{pmatrix} &
           \begin{pmatrix}
           . & . & . & . & . \\
           . & . & . & . & . \\
           . & . & . & . & . \\
           . & . & \vect{I}^{\mathrm{c2a}} & . & . \\
           . & . & . & . & . \\
           . & . & . & . & . \\
           . & . & . & . & . \\
           \end{pmatrix} \\
           \begin{pmatrix}
           . & . & . & . & . & . & . \\
           . & . & . & . & . & . & . \\
           . & . & . & \vect{I}^{\mathrm{a2c}} & . & . & . \\
           . & . & . & . & . & . & . \\
           . & . & . & . & . & . & . \\
           \end{pmatrix} &
           \begin{pmatrix}
           . & . & . & . & . \\
           . & . & . & . & . \\
           . & . & \vect{I}^{\mathrm{cal}} & . & . \\
           . & . & . & . & . \\
           . & . & . & . & . \\
           \end{pmatrix} \\
      \end{pmatrix},
\end{equation}

The diagonal blocks are individual fisher matrices for astrophysical parameters
and calibration parameters and the diagonal blocks serve as error propagation
matrices. The error on the astrophysical parameters $\Delta \vect{\Theta}$ due
to calibration uncertainties $\Delta \vect{\Gamma}$ can be computed as:

\begin{equation}
	\Delta \vect{\Theta} = - \left(\vect{I}^{\mathrm{ast}}\right)^{-1}
\vect{I}^{\mathrm{c2a}} \Delta \vect{\Gamma},
\end{equation}

Note that the number of rows of $\vect{I}^{\mathrm{c2a}}$ is given by the size
of $\vect{\Theta}$ and the number of columns given by the size of
$\vect{\Gamma}$.

\input{McCal/tableTex/BNS_BBH_params}

\subsection{Effect of calibration errors on astrophysical parameter estimation}

\input{McCal/tableTex/detParams}

\input{McCal/figureTex/BNSCalErrCompDiffDet}

\input{McCal/figureTex/BBHCalErrCompDiffDet}

We calculated the effect of fractional calibration errors on the fractional
errors of the estimated astrophysical parameters using the framework mentioned
above. We ran the analysis for two kinds of compact binary mergers, binary
neutron star (BNS) mergers, and binary black hole (BBH) mergers. Table
\ref{tab:BNS_BBH_params} and Table \ref{tab:detParams} shows the nominal
values of the coalescence event parameters and detector parameters used for
calculating the error propagation. The detector responsivity is modeled as:

\begin{equation}
R(f; g_0, f_d) = \frac{\left(1 + i \frac{f}{f_d}\right)}
                      {L_{arm}g_0}e^{i 2\pi \frac{f L_{arm}}{c}}
\end{equation}

Fig.~\ref{fig:BNSCalErrCompDiffDet} shows the estimated total fractional errors
for four parameters of the BNS event for different calibration errors in the
detector pole frequencies. Moreover, we used GWINC\cite{gwinc} to project the
calculations for the current upgrade on Advanced LIGO known as A+ LIGO, a
proposed cryogenic upgrade on Advanced LIGO known as Voyager\cite{Adhikari2020}
(see Ch.\ref{ch:Voyager}), and a few future detector proposals known as Cosmic
Explorer\cite{CEP2100003} in various phases and configurations.

Note that for Advanced LIGO, a 1\% calibration uncertainty is acceptable as
parameter estimation for all four parameters are limited by statistical noise
of the detector which is the desirable case. However, for upgraded LIGO and
future detectors that improve the sensitivity, even though the statistical noise
floor for each parameter is lower, we can only take advantage of the better
sensitivity if the calibration error is less than 1\%. For instance, the tidal
deformability of the neutron stars can be estimated to much better than 8\%
uncertainty with the future Cosmic Explorer detectors, but the calibration error
would dominate the results unless it is reduced to 0.1\% level. Note that
these calculations are very optimistic with simplified model of the event, for
instance, the merger is assumed to happen face on with the detectors which is
not always the case or known to a good certainty. Thus the noise floors obtained
here are optimistic estimates and are worse in practice.

Fig.~\ref{fig:BBHCalErrCompDiffDet} shows similar analysis performed for a
nominal BBH event. We see that that the fractional errors in the
parameter estimates are right
at the crossover with the calibration uncertainty for Advanced LIGO. Any better
detector is limited by calibration uncertainty for calibration errors above
0.1\%. With better sensitivity, the restrictions on required calibration
uncertainty wil be tighter. These two calculations set the basis for our target
of developing a calibration method to reach 0.1\% uncertainty. We might want to
do even better in future if nearby events are too loud and the estimates for
such events are limited by calibration error in uncertainty.
