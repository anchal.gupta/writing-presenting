\section{Implementation at LIGO observatories and future scalability}

One of the biggest advantages of this method is that it is ready to be
implemented at the Advanced LIGO observatories with very minimal extra hardware
required. The observatories already utilize \gls{ALS} system for acquiring the
lock. This means the infrastructure for auxiliary laser injection and locking
them to individual arms exist. However, the noise performance of the auxiliary
laser lock would need to be improved significantly.

For lock acquisition, the noise requirement of the auxiliary laser loop is not
very stringent as it is used to bring the common arm length motion to within
the locking regime of \gls{CARM} loop. The noise floor for \gls{CARM} is about
three orders of magnitude more than \gls{DARM} loop which rejects the dominant
residual main laser frequency noise. But for calibration, a beatnote between
\gls{CARM} and \gls{AUX} loop needs to measure the same motion as measured by a
much more sensitive \gls{DARM} loop. The injected calibration line needs to be
visible in the beatnote measurement without breaching the dynamic range of
\gls{DARM} loop or making it unstable due to non-linear effects. Craig
Cahillane thesis\cite[Fig.~4.12]{CraigThesis} shows how the current noise
performance of the auxiliary laser loop is seven orders of magnitude worse than
\gls{DARM} loop noise. However, it also shows that the shot noise limit in the
auxiliary laser loop is as good as the \gls{CARM} noise performance. If the
auxiliary laser loop is improved to achieve shot noise limited noise
performance, then the beatnote measurement would be able to measure injected
calibration lines with sufficient averaging time. Further quantitative analysis
to calculate the required calibration line strength for a desired calibration
update rate to achieve 0.1\%  uncertainty is required.

Since we need low noise around the particular calibration line frequencies,
resonant gain filters can be used to further reduce noise floor in \gls{CARM}
and \gls{AUX} loop at the calibration line frequencies. We demonstrated this
for the 33.921 Hz line in the \gls{CARM} loop in our experiment at the CIT 40m
prototype. In the full Dual recycled Fabry-P\'erot Michelson interferometer
configuration, the \gls{CARM} loop bandwidth covers the whole frequency band of
calibration thus such resonant gain filters will be required at all calibration
line frequencies. For this step, extra loop stability considerations might have
to be taken into account as we do not want to affect the detector sensitivity
for making a good calibration. So more work and experimentation are required on
this part. The CIT 40m prototype would perform experiments on multicolor
calibration with \gls{PRFPMI} configuration to inform the observatories better
on this topic.

For the actuation of the calibration lines, the observatories can continue to
use the photon calibration lasers to apply modulating force on the mirror since
the multicolor calibration method is agnostic to the method used for actuation.
The existing photon calibration would also serve as a good diagnostic tool for
any systematic offset in the multicolor calibration scheme, up to the
uncertainty of measurement in a photon calibration system.

The observatories can also utilize other actuators at their disposal if they
can work better at higher frequencies. Photon calibration is particularly
limited at higher frequencies because of a lack of faithful knowledge about
mechanical transfer functions at high frequencies. This issue is not present
for multicolor calibration as it does not matter if the actuation is perfect or
even linear. In principle, even violin modes of the suspensions can be utilized
to track the motion of \gls{DARM} strain at kHz frequencies. However,
additional knowledge of the detector response function at high frequencies
would be necessary to make better estimates on phase delay effects in the
\gls{DARM} loop. This idea can be extended to the lower frequency band as well,
where large noise peaks exist at 5-30 Hz region. In particular, bounce and roll
modes of the suspended optics also naturally produce actuation in \gls{DARM}
degree of freedom that can be utilized for calibration if they are visible in
the beatnote frequency with sufficient signal-to-noise ratio.

The observatories should implement multicolor calibration with beatnote
measurements of both auxiliary lasers with the main laser. This will provide
two simultaneous calibration systems that can shed more light on the possible
systematics of the method.

While we chose 0.1\% as a target for this calibration scheme, there is no
reason to stop there. If in the future, new detector topologies or technologies
make the noise floor of gravitational wave detectors better, this method is in
principle scalable to even lower calibration uncertainties. For this purpose,
the residual frequency noise in the auxiliary laser lock to the arm cavities
would need to be suppressed further and the beatnote frequency fluctuations
measurement would need to be improved. One can increase the calibration line
actuation strengths as well if that is an option without disturbing the
interferometer too much. Another option is to increase the integration time
further to 1000 seconds and use multicolor calibration to calibrate photon
calibration every 1000 seconds while faster real-time calibration is provided
by the photon calibration in between its calibrations.
