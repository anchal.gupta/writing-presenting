\section{Multicolor calibration measurement at the 40m prototype}

We conducted several measurements for calibrating the \gls{DARM} output of the
40m prototype in \gls{FPMI} configuration. These measurements were made to
identify the maximum averaging one can do with the multicolor calibration
method, and to identify any systematics that can change between different
measurements. The measurement sequence was run through a Python script to
ensure standardized testing across multiple instances.

\subsection{Arm locking and alignment}

The sequence starts by attempting a lock on the \gls{YARM} cavity with the main
laser loop. Since the interferometer is routinely used to perform other
measurements, the arm cavities are nominally aligned enough to acquire the lock
at the very least, if not with maximum mode matching. Or the other way around,
we ensured before the start of each measurement that the arm cavities are
aligned enough that we see good TEM00 mode flashing in the arm cavities for
incident main laser on the cameras that are pointed to the suspended test
masses. Once the lock is acquired, the \gls{UGF} of the \gls{YARM} lock to the
main laser is set to 200 Hz. For doing this quickly, a script injects
band-limited Gaussian noise between 100 Hz to 400 Hz for 10 seconds and gets
the open loop transfer function magnitude in this region. The \gls{UGF} is
identified by a simple linear fit to the transfer function in this region and
the gain of the loop is scaled to take it to 200 Hz. This process takes only 20
seconds and is automated, so every time we acquire a lock on any degree of
freedom, we can quickly tune the gain of the feedback filter according to the
current optical gain of the system.

Once the \gls{YARM} is locked, Angular Dither Stabilization Servo (ASS) is run
on \gls{YARM} to match the pointing of the input beam angle to the cavity mode
created by \gls{YARM} (which is in the transmission of the beamsplitter), and
to center the beam spots on \gls{ITMY} and \gls{ETMY} to reduce any
angle-to-length coupling through these two optics. The ASS dithers \gls{ITMY} ,
\gls{ETMY} , PR2, and PR3 in PIT and YAW degrees of freedom at frequencies in
the 30 Hz to 100 Hz range. Then we look for the error signal due to input
pointing mismatch in the transmitted light intensity and the error signal for
beam spot centering in the \gls{PDH} error signal of the cavity lock. Once
these error signals are sufficiently reduced (when the signal-to-noise ratio in
measured demodulated signals go below 10 in an average over 2 seconds), the
dithering is stopped.

Then we lock the \gls{XARM} cavity with the main laser, set its \gls{UGF}, and
turn on ASS for this cavity. However, since there is only one steering optic
for the \gls{XARM} cavity (\gls{BS}), we can not control all 8 degrees of
freedom associated with input pointing mismatch and beam spot on the mirrors.
We decide to not close the loop on \gls{ITMX} beam spot centering as it is a
flat mirror and thus the beam spot offset has less coupling from the angular
motion to the length of the cavity. The beam spot is still centered on
\gls{ETMX} and the input alignment is made the same as the cavity mode
alignment by aligning \gls{BS} and
\gls{ITMX} appropriately. After this, the arm cavities are aligned well with
the input main laser beam. Since the input test masses are flat mirrors, they
get aligned so that the laser is at normal incidence to the cavities. This in
turn aligns the Michelson interferometer as well, so no more alignment is
required for the \gls{FPMI} configuration.

\subsection{FPMI Locking}
\label{subsec:FPMIlocking}

The next step in the sequence is to lock \gls{FPMI} . Three error signals are
required to lock the \gls{DARM} , \gls{CARM} , and \gls{MICH} degrees of
freedom. For \gls{DARM} , we used the RF readout technique (see
Sec.\ref{subsec:RFreadout}) \gls{AS55}. For locking \gls{CARM} and \gls{MICH} ,
we use the symmetric port reflected light, demodulated at 55 MHz on
\gls{REFL55} photodiode. The two quadratures of \gls{REFL55} give error signals
for \gls{CARM} and \gls{MICH}.

The three error signals are dependent on each other to remain in a linear
regime though, that is, the error signals are truly useable only when all of
them are controlled together or the interferometer is simultaneously near the
lock point for all three error signals. This of course does not happen
serendipitously. To get the lock started, we first lock the \gls{DARM} and
\gls{CARM} using electronically generated \gls{CARM} and \gls{DARM} error
signals through the individual cavity's \gls{PDH} error signals. For this
purpose, the ITMs have a small wedge angle, which picks off part of the
reflected light from each arm cavity into the RF photodiodes named POX11 and
POY11. The demodulated signal from these photodiodes is added and subtracted to
create electronic \gls{CARM} and \gls{DARM} signals like:

\begin{equation}
\begin{aligned}
\epsilon_{CARM} &= \frac{1}{2} \left(POX11_I + POY11_I \right)\\
\epsilon_{DARM} &= \frac{1}{2} \left(POX11_I - POY11_I \right)
\end{aligned}
\end{equation}

The \gls{DARM} error signal is fed back to the ETMs to create a differential
length actuation and the \gls{CARM} error signal is fed back to the mode
cleaner end optic MC2 to lock the main laser (which is locked to the mode
cleaner) to the common mode of the two arms:

\begin{equation}
\begin{aligned}
\zeta_{CARM} &= \frac{1}{\sqrt{2}} MC2\\
\zeta_{DARM} &= \frac{1}{2} \left(ETMX - \gls{ETMY} \right)
\end{aligned}
\end{equation}

This lock is not robust or clean as we are creating the common and differential
arm modes electronically instead of optically using the beam splitter of
Michelson, but it allows us to reduce the \gls{CARM} error signal motion and
keep \gls{CARM} near the lock point. This makes locking \gls{MICH} easier as it
uses the orthogonal quadrature in \gls{REFL55} from the \gls{CARM} quadrature.
We adjust the demodulation phase such that the \gls{MICH} signal appears only
in REFL55\_Q. We turn on the \gls{MICH} loop to lock the interferometer output
port at the dark point by feeding back to \gls{BS}. This in turn allows us to
swiftly change the error signal for \gls{CARM} and \gls{DARM} loops to true
optical signals REFL55\_I and AS55\_Q. This completes the lock acquisition for
\gls{FPMI} . Using the same method mentioned above, the \gls{UGF} for
\gls{DARM} and \gls{CARM} are each set to 200 Hz.

Once the lock is acquired, we turn on a set of 5 filters in the \gls{CARM}
loop, each for a calibration line frequency, either increasing the gain at the
calibration frequency by 60 dB using a resonant gain filter if it is within the
\gls{CARM} loop bandwidth or introducing a notch at the calibration line
frequency using a notch filter of depth 120 dB. See
Sec.\ref{subsec:calBeatMeas} for the reasoning behind introducing these
filters.

\input{McCal/figureTex/CARM_OLTF}
\input{McCal/figureTex/YAUX_OLTF}

\subsection{Auxiliary Laser Lock and beatnote frequency tuning}

The auxiliary laser at the end station on \gls{YARM} was used for the
measurements. Before locking the laser to the \gls{YARM} cavity, the laser
crystal temperature is adjusted to obtain a beatnote of about 40 MHz between
the auxiliary seed laser (sent over 40m optical fiber to the vertex area) and
the main laser. The laser frequency is modulated at 230 kHz by exciting a
\gls{PZT} resonance on the \gls{NPRO} crystal. This removes the need for any
external phase modulator. The \gls{PDH} error signal is read in the reflection
from the cavity and sent through a \gls{PDH} servo box to actuate on the laser
\gls{PZT} . We get about 20 kHz of unity gain frequency which is sufficient for
this proof of principle test, but ideally we need to either extend the
\gls{UGF} further to at least 100 kHz. Or we can use resonant gain filters at
the calibration line frequencies used as the calibration uncertainty increases
with the fluctuations in this loops gain if the gain is not high enough at the
calibration line frequencies. To lock the laser to the \gls{YARM} , we simply
open and close the shutter a few times until the TEM00 mode is locked.

\subsection{OLTF measurements}

Before the calibration is measured, the open loop transfer function for
\gls{DARM}, \gls{CARM}, and the auxiliary laser loop is measured. The
\gls{CARM} and
auxiliary laser loops are important as their values are used in calibration
(See Eq.~\ref{eq:calibration}). Fig.~\ref{fig:CARM_OLTF} shows the \gls{CARM}
\gls{OLTF} taken at the calibration line frequencies. This measurement is taken
to ensure that the loop gain is high at the line frequency within the bandwidth
and very low outside the bandwidth.

Fig.~\ref{fig:YAUX_OLTF} shows the \gls{OLTF} of the auxiliary laser lock with
the \gls{YARM} cavity. The measurement was taken near the calibration line
frequencies. Ideally, we would like to have resonant gain filters placed in
this loop at the calibration line frequencies to ensure the loop gain is very
high at those frequencies. But due to lack of time and equipment availability,
we had to make do with no gains and the usual loop filters with the \gls{UGF}
of about 20 kHz.

\subsection{Reference data measurement}

Before turning on the calibration lines, we let the lock stay with the applied
settings for 500 seconds to measure the statistical noise in the measurement.
This is essentially the integrated noise of the beatnote frequency at the
calibration line frequencies when no actuation is applied at these frequencies.
This gives us an estimate of the statistical noise in the measurement so that
we can place statistical error bars around the calibration constants obtained
later.

\subsection{Calibration measurement and results}

\input{McCal/tableTex/calLines}

The measurement is started by injecting oscillations at the chosen calibration
line frequencies. The chosen frequency values and the digital counts of
amplitude used for actuating on the \gls{ETMY} are shown in
Table.~\ref{tab:calLines}. Note that less physical actuation is used at higher
frequencies as the noise floor is also significantly less at higher
frequencies. The frequencies are scattered around the frequency band of
interest to mimic how Advanced LIGO uses the calibration line frequencies. For
calibrating just \gls{FPMI} though, not all these frequencies are required as
the number of parameters associated with the model is very small. The filters
used for demodulation are 10 mHz 8$^{\mathrm{th}}$ order Butterworth low pass
filters.

\input{McCal/figureTex/BeatAS_SNR}

The first thing to identify is the signal-to-noise ratio (SNR) of each
calibration line. We measure the spectrum of the beatnote frequency at 128s
averaging which is close to the 100s averaging we applied on demodulated
signal. Fig.~\ref{fig:BeatAS_SNR} shows the calibration line height for each
frequency. Note we achieved an SNR of more than 1000 on three central lines.
The 33.921 Hz line could not be actuated any harder as the position-to-angle
coupling in the imperfect coil actuation of \gls{ETMY} resulted in the cavity
getting misaligned too much and \gls{FPMI} lock would fail. For the 1418.93 Hz
line, the actuation gets limited due to the high suppression of force by the
suspension at this frequency.

\input{McCal/figureTex/FPMICalTS}

\input{McCal/figureTex/FPMICalRatio}

Using Eq.~\ref{eq:calibration}, we calculated the \gls{DARM} strain as defined
in Eq.~\ref{eq:DARMstrain} from the measured demodulated beatnote frequency.
Fig.~\ref{fig:FPMICalTS} shows the time series of calibrated \gls{DARM} strain.
The measured value is of the correct order of magnitude. We performed a
verification by calibrating the \gls{DARM} error signal with the Michelson
interferometer ($2.7\times10^{-12}$ m/err-cts) and using the average actuation
strength that was measured for \gls{ETMY} and \gls{ETMX} (10.9 nm
Hz$^2$/ctrl-cts). The err-cts and ctrl-cts refer to the digital counts at the
error and control points respectively. The \gls{DARM} control and error points
were also demodulated using the same low-pass filters and the same oscillators
that were used for arm-length modulation. The demodulated values are multiplied
with the above calibration factors and summed to get the inferred \gls{DARM}
strain by the Michelson calibration method.

\input{McCal/figureTex/BeatYtoYARMTF}

\input{McCal/figureTex/BeatYtoDARMTF}

Fig.~\ref{fig:FPMICalRatio} shows the ratio of the inferred \gls{DARM} strain
from Michelson calibration to the \gls{DARM} strain measured by the multi-color
calibration method. We see that there is a frequency-dependent discrepancy
between the two methods. The discrepancy is large enough that we can not rule
it out as uncertainty in either of the methods. To investigate this further,
tried to establish the frequency independence of beatnote measurement and its
correspondence to true arm length changes. The simplest test is to lock the
\gls{YARM} to the main laser and lock the \gls{AUX} laser to \gls{YARM} . Then
we took a transfer function measurement between the beatnote frequency
measurement and the \gls{YARM} error point while exciting at \gls{ETMY}.
Fig.~\ref{fig:BeatYtoYARMTF} shows this transfer function. We see that the
transfer function is flat as expected. So we verified that at least for the
single arm case, the beatnote frequency does track the arm modulation as sensed
by the \gls{PDH} error signal.

Then we locked the interferometer in \gls{FPMI} configuration and again ran the
swept sine transfer function while exciting \gls{ETMY} and taking the transfer
function from the beatnote frequency measurement to the \gls{DARM} loop error
signal. Fig.~\ref{fig:BeatYtoDARMTF} shows this transfer function. It is
interesting to see a similar frequency dependence in this transfer function as
seen in Fig.~\ref{fig:FPMICalRatio}. We think that the \gls{DARM} loop feedback
actuation might not be perfect, any asymmetry between the actuation strengths
of \gls{ETMY} and \gls{ETMX}, particularly a frequency-dependent asymmetry at
higher frequencies can result in the \gls{DARM} loop feeding back to the
\gls{CARM} loop.

Since the configuration we are testing is only \gls{FPMI} and not the full
interferometer lock, the \gls{CARM} loop bandwidth is very small (only around
200 Hz) and is comparable with the \gls{DARM} loop bandwidth. Thus the
\gls{DARM} loop might be interfering with the \gls{CARM} loop during the
calibration measurement since the calibration lines are high enough that even
outside the bandwidth of the \gls{DARM} loop, the loop actuates back at the
line frequencies. Due to the asymmetry, the \gls{DARM} loop feedback some of
the actuation to the \gls{CARM} loop which alters the beatnote measurement
between the main laser (which is locked to the \gls{CARM} degree of freedom)
and the beatnote frequency. This effect warrants more investigation which is
out of the scope of this thesis due to lack of time. But the silver lining is
that in \gls{PRFPMI} or \gls{DRFPMI} lock of the interferometer, the \gls{CARM}
loop is locked with very high bandwidth so that even if there is an asymmetry
in \gls{DARM} feedback, the \gls{CARM} loop can correct it faster than the
\gls{DARM} loop by adjusting the main laser frequency in feedback.
