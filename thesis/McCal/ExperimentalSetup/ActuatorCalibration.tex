\section{Actuator calibration test}
\label{sec:ActuatorCalibration}

We tested the multicolor calibration scheme first for calibrating the actuation
strength of \gls{ITMY}. For this test, the scheme is slightly different.
\gls{ITMY} is sent modulation signal at calibration line frequencies to
modulate the arm length:

\begin{equation}
L_Y = \sum_i \frac{A_{ITMY}}{f_i^2} \cos(2\pi f_i t)
\end{equation}

Here, $A_{ITMY}$ is the \gls{ITMY} actuation transfer function strength. The
factor of $f^2$ in the denominator comes from the pendulum suppression above 1
Hz (this expression is only true well above 1 Hz where our calibration line
frequencies lie). We are interested in finding the value of $A_{ITMY}$ in units
of m Hz$^2$/cts. Here 'cts' refer to DAC counts that sends the signal to the
ITMY actuator.

For the measurement, a single arm, the \gls{YARM} in this case is locked to the
main laser by actuating on cavity length through \gls{ETMY}. Thus the main
laser fluctuations are followed by the \gls{YARM} length $L_Y$ in this case:

\begin{equation}
\begin{aligned}
\delta \tilde{L}_y(s)&=
            \frac{J_{\mathrm{OL}}(s)}{1 - J_{\mathrm{OL}}(s)}
            \frac{L_y}{\nu_{\mathrm{main}}}
            \delta \tilde{\nu}_{\mathrm{main}}(s)\\
\delta \tilde{L}_y(s) &=  J_{\nu \rightarrow L_y}(s)
             \frac{L_y}{\nu_{\mathrm{main}}}
             \delta \tilde{\nu}_{\mathrm{main}}(s)
\end{aligned}
\end{equation}

This is only to reduce the motion of the arm cavity with respect to the main
laser. The \gls{OLTF} $J_{\mathrm{OL}}$ has notches at the calibration line
frequencies $f_i$ to ensure that the arm length is free to move on these
frequencies. So $|J_{\mathrm{OL}}(f_i)| \ll 1$ resulting in $J_{\nu \rightarrow
L_y}(f_i) \ll 1$. Thus at the calibration line frequencies, the arm length does
not follow the main laser and is free to be modulated by the actuation provided
on \gls{ITMY}.

\input{McCal/figureTex/ITMYActCalTS}

The auxiliary laser is locked to \gls{YARM} in the same way as shown
Sec.\ref{subsec:auxloop}. The auxiliary laser seed frequency is given by:

\begin{equation}
\delta \tilde{\nu}_{\mathrm{seed}}(s) =
                                \frac{\nu_{\mathrm{seed}}}{\nu_{\mathrm{aux}}}
                                \delta \tilde{\nu}_{\mathrm{aux}}(s)\\
                                = G_{L \rightarrow \nu}(s) \nu_{\mathrm{seed}}
                                   \frac{\delta \tilde{L}_y(s)}{L_y}
\end{equation}

Since the main laser is quiet at the calibration line frequencies, while the
auxiliary laser is following the modulations made through \gls{ITMY}, the beat
note frequency between the main laser frequency and seed laser of the auxiliary
laser, on demodulation at the calibration line frequencies, gets signal
contribution only from the auxiliary laser fluctuations.

\begin{equation}
\begin{aligned}
\delta \tilde{\nu}_{\mathrm{beat}, i}
                          &= G_{L \rightarrow \nu} \nu_{\mathrm{seed}}
                             \frac{\delta \tilde{L}_y(s)}{L_y}\\
                          &= G_{L \rightarrow \nu}
                             \frac{c}
                                  {L_y \lambda_{\mathrm{main}} f_i^2}
                             A_{ITMY}\\
A_{ITMY} = &= \frac{L_y \lambda_{\mathrm{main}} f_i^2}{c G_{L \rightarrow \nu}}
              \delta \tilde{\nu}_{\mathrm{beat}, i}
\end{aligned}
\end{equation}

This provides a calibration for \gls{ITMY} actuation transfer function
strength. For our test, the \gls{AUX} loop \gls{OLTF} had \gls{UGF} of about 11
kHz, so for all calibration frequencies, $G_{L \rightarrow \nu}(f_i) \approx
1$, but we took drift in circulating optical power into account.
Fig.~\ref{fig:ITMYAUXOGC} shows the applied optical gain correction while taking
into account varying loop gain. Fig.~\ref{fig:ITMYActCalTS} shows the time
series of the calibration result where the beatnote frequencies are demodulated
and low passed at 10 mHz which is equivalent to averaging for 100 seconds.

\input{McCal/figureTex/MICHFreeSwingCal}

For an ideal suspension, it is expected to get the calibration transfer
function strength $A_{ITMY}$ as a frequency-independent parameter. But we found
that the actuation is higher for higher frequencies. To verify if this is due
to our digital phase locked loop not working correctly, we performed an offline
analysis on the \gls{DFD} output data. This involves taking the arctangent of
the I and Q outputs of \gls{DFD} directly (see Eq.~\ref{eq:DFDout}). The
computation of arctangent is not deployable in our real-time system to remain
within the delay constraints, that is why we need the digital phase tracker to
do this for us. But in offline analysis, we can verify if the phase tracker
worked correctly. In Fig.~\ref{fig:ITMYActCal} we present the output of the
calibration with DFD and phase tracker, and with DFD and the offline analysis.
The two measurements closely match, following the same frequency dependence.
However, we are not sure what is the reason for the difference between the two.
One possibility is that in offline demodulation, the local oscillator source is
ideal and does not cancel out the dither and drift of the real-time oscillator
which happens in real-time demodulation correctly.

\input{McCal/figureTex/ITMYActCal}

We used the only other calibration method, the Michelson interferometer, that
is available to us to verify the anomalous frequency dependence of the
actuation strength. We first calibrate the error signal to differential
displacement in m by measuring the peak-to-peak response of the error signal
when the Michelson interferometer is freely swinging through the fringes. This
is the most noisy part of this calibration method. To estimate some kind of
error estimate, the free swing data is histogrammed and the difference in the 2
peak positions is taken as the full swing value, and the average full width at
half maximum of each peak is taken as their standard deviation. Then, the
interferometer was locked with \gls{UGF} of 20 Hz to keep control bandwidth
outside of calibration lines, and notches were introduced in the control loop
filter at the calibration line frequencies. Thus the error signal in the
presence of calibration line modulation at \gls{ITMY}  would measure the displacement
of \gls{ITMY}  in meters. This method is prone to a lot of systematics, thus the error
bars for Michelson calibration in Fig.~\ref{fig:ITMYActCal} are still
underestimates. However, this method also showed an increase in the actuation
strength at high frequencies, but with poor confidence. So the anomalous
frequency dependence is still to be solved.
