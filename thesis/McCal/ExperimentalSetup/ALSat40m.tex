\section{Arm Length Stabilization (ALS) at CIT 40m}
\label{sec:ALSat40m}

Sec.\ref{sec:ALS} introduced the functionality of Arm Length Stabilization
(ALS) at the Advanced LIGO and 2nd generation gravitational wave detectors. At
CIT 40m, since the arm length is much shorter than 4 km, there is an alternate
way to implement the \gls{ALS}. While Advanced LIGO takes the beatnote between
frequency doubled (Green, 532 nm) pick off of the main laser and the
transmitted
\gls{AUX} laser, since the \gls{AUX} laser is generated itself by frequency
doubling a seed 1064 nm laser, we can also take a beatnote between a pick off
of the \gls{AUX} seed laser and pick off of the main laser at \gls{IR} (1064
nm). However, one will need to transmit the seed laser pick-off light from the
end station to the vertex area where the main laser is present. For a 40m
distance, this can be easily done with an optical fiber cable that does not
introduce too much noise over this distance. But the same cannot be done so
easily over 4 km at the Advanced LIGO.

However, there are many benefits of taking the beatnote at \gls{IR}. First, the
beatnote is always present whether the \gls{AUX} laser is locked to the arm
cavity or not. This helps in tuning the wavelength of the \gls{AUX} laser with
the laser crystal temperature to ensure the beatnote frequency is within the
bandwidth of the beat note photodiode. Second, the beatnote amplitude does not
suffer from angular misalignment or drifts of the arm cavity alignment over
time. This is essential because as we'll see later, the beatnote amplitude can
couple into the frequency motion tracking if it falls below a certain level.
Since the \gls{AUX} green and \gls{AUX} seed \gls{IR} are related to each other
through a highly coherent\cite{Yeaton2012} second harmonic generation process,
the relative frequency noise of the two frequencies remains the same:

\begin{equation}
\begin{aligned}
\nu_{\mathrm{green}}(t) &=  2 \nu_{\mathrm{IR}}(t)\\
\delta \nu_{\mathrm{green}}(t) &=  2 \delta \nu_{\mathrm{IR}}(t)\\
\frac{\delta \nu_{\mathrm{green}}}{\nu_{\mathrm{green}}}(t) &=
                  \frac{\delta \nu_{\mathrm{IR}}}{\nu_{\mathrm{IR}}}(t)\\
\end{aligned}
\end{equation}

Thus, the beat note frequencies in the two cases carry the same information as
the relative fluctuations are equal.

\begin{equation}
\begin{aligned}
\nu_{\mathrm{beat, green}}(t) =
                \nu_{\mathrm{Main, green}}(t) - \nu_{\mathrm{AUX, green}}(t)
                &=  2 \nu_{\mathrm{Main, IR}}(t) - 2\nu_{\mathrm{AUX, IR}}(t)
                = 2 \nu_{\mathrm{beat, IR}}(t)\\
\delta \nu_{\mathrm{beat, green}}(t) &=  2 \delta \nu_{\mathrm{beat, IR}}(t)\\
\frac{\delta \nu_{\mathrm{beat, green}}}{\nu_{\mathrm{beat, green}}}(t) &=
           \frac{\delta \nu_{\mathrm{beat, IR}}}{\nu_{\mathrm{beat, IR}}}(t)\\
\end{aligned}
\end{equation}

Thus at CIT 40m, the \gls{IR} beatnote is implemented. A pick-off from the
\gls{AUX} seed laser is carried over optical fiber to the main laser table in
the vertex area. A pickoff from the main laser is coupled into another fiber
optic, and the two cables meet in a fiber optic BS whose output is read by a
New Focus 1811 fiber-coupled photodiode with a bandwidth of 125 MHz.

\subsection{Beanote frequency tracking}
\label{subsec:DFD}
The beatnote frequency from the photodiode is sent to a Delayline Frequency
Discriminator (DFD). Fig.~\ref{fig:DFD} shows the implementation of DFD. Due to
damage thresholds associated with the fiber-coupled photodiode, the optical
power needs to stay low. Thus the signal is amplified after the photodiode for
the beatnote tracking. The amplified signal is then split into two paths. One
going over 50m of LMR-195 low loss (8.4 dB/100m) coaxial cable. The delayed
signal is mixed with the other path signal in LIGO LSC IQ Demodulator
Board\cite{D0902745}. If the input beatnote signal is
$V_{\mathrm{beat}}cos(2\pi \nu_{\mathrm{beat}} t)$, then the signal at the two
paths is given by:
\begin{equation}
\begin{aligned}
V_{\mathrm{delayed}} &= \frac{V_{\mathrm{beat}}}{\sqrt{2}}
                            cos(2\pi \nu_{\mathrm{beat}} (t - \tau_{d}))\\
V_{\mathrm{short}} &= \frac{V_{\mathrm{beat}}}{\sqrt{2}}
                            cos(2\pi \nu_{\mathrm{beat}} t)\\
\end{aligned}
\end{equation}

\input{McCal/figureTex/DFD}

Here, $\tau_d$ is the delay time in the 50m cable. The IQ demodulator creates
two quadratures of the short leg signal by using a $90^\circ$ splitter after
amplifying it by 10 dB. Thus the two quadrature outputs of the demodulator look
like this:

\begin{equation}
\label{eq:DFDout}
\begin{aligned}
V_{\mathrm{beat, I}} &=  V_{\mathrm{beat}} M
                            cos(2\pi \nu_{\mathrm{beat}} (t - \tau_{d}))
                            cos(2\pi \nu_{\mathrm{beat}} t)\\
                     &= \frac{1}{2}V_{\mathrm{beat}} M
                        \left(
                        cos(2\pi \nu_{\mathrm{beat}} \tau_{d})
                        + cos(2\pi \nu_{\mathrm{beat}} (2t - \tau_{d}))
                        \right)\\
V_{\mathrm{beat, Q}} &=  V_{\mathrm{beat}} M
                            cos(2\pi \nu_{\mathrm{beat}} (t - \tau_{d}))
                            cos(2\pi \nu_{\mathrm{beat}} t - \frac{\pi}{2})\\
                     &= \frac{1}{2}V_{\mathrm{beat}} M
                        \left(
                        sin(2\pi \nu_{\mathrm{beat}} \tau_{d})
                        + sin(2\pi \nu_{\mathrm{beat}} (2t - \tau_{d}))
                        \right)\\
\end{aligned}
\end{equation}

The demodulated signals are low passed at 8 kHz by anti-aliasing filters and
read by ADC. This discards the twice-beat note frequency signal part above. The
remaining signals are time-varying only because of any motion in the beatnote
frequency itself:

\begin{equation}
\begin{aligned}
V_{\mathrm{beat, I}}(t) &= \frac{1}{2}V_{\mathrm{beat}} M
                        cos(2\pi \nu_{\mathrm{beat}}(t) \tau_{d})\\
V_{\mathrm{beat, Q}}(t) &= \frac{1}{2}V_{\mathrm{beat}} M
                        sin(2\pi \nu_{\mathrm{beat}}(t) \tau_{d})\\
\end{aligned}
\end{equation}

\input{McCal/figureTex/PhaseTracker}

Thus the information about the beatnote frequency and its motion is in the
phase between the measured I and Q signals. To unwrap this information, a
digital phase tracker is employed. Fig.~\ref{fig:PhaseTracker} shows how the
phase tracker works. The incoming I and Q signals are "rotated" in phase space
by multiplying them with $-sin(\phi)$ and $cos(\phi)$ respectively and summing
them, for some phase angle $\phi$:

\begin{equation}
\begin{aligned}
\epsilon(t) &= -\frac{1}{2}V_{\mathrm{beat}} M
                        cos(2\pi \nu_{\mathrm{beat}}(t) \tau_{d})
                        sin(\phi(t))
               + \frac{1}{2}V_{\mathrm{beat}} M
                        sin(2\pi \nu_{\mathrm{beat}}(t) \tau_{d})
                        cos(\phi(t))\\
            &= \frac{1}{2}V_{\mathrm{beat}} M
               sin(2\pi \nu_{\mathrm{beat}}(t) \tau_{d} - \phi(t))
\end{aligned}
\end{equation}

This error signal is integrated with a 0 pole filter $K(s)$ and substituted for
$\phi$, thus creating a control loop. Near lock point, $\phi(t) \approx 2\pi
\nu_{\mathrm{beat}}(t) \tau_{d}$, and thus in Laplace domain:

\begin{equation}
\begin{aligned}
\epsilon(s) &= \frac{1}{2}V_{\mathrm{beat}} M
                \left(2\pi \nu_{\mathrm{beat}}(s) \tau_{d} - \phi(s)\right)\\
\phi(s) &= \frac{1}{2}V_{\mathrm{beat}} M K(s)
                   \left(2\pi \nu_{\mathrm{beat}}(s) \tau_{d} - \phi(s)
                   \right)\\
\phi(s) &= \frac{1}{\frac{2}{V_{\mathrm{beat}} M K(s)} + 1}
           2\pi \nu_{\mathrm{beat}}(s) \tau_{d}
\end{aligned}
\end{equation}

Thus, if filter $K(s)$ is adjusted such that $\frac{1}{2}V_{\mathrm{beat}} M
K(s) \gg 1$ for the frequencies of interest, then $\phi(s) \approx 2\pi
\nu_{\mathrm{beat}}(s) \tau_{d}$. Thus one can read the beatnote frequency and
its fluctuations by multiplying $\frac{1}{2\pi \tau_d}$ with the control signal
of the above loop. This is how we measure the beatnote frequency.

Note that the gain required to have a certain \gls{UGF} in the phase tracker
the loop is dependent on the amplitude of RF power. This proved to be an issue
for us since the beatnote amplitude fluctuations change the transfer function
from $\phi(t)$ to $\nu_{\mathrm{beat, meas}}$. We fixed this by normalizing the
input to the digital phase tracker by quadrature sum of I and Q signals before
applying the rotations. This removes the factor of $V_{\mathrm{beat}} M$ in the
above equation gives a clean stationary loop:

\begin{equation}
\phi(s) = \frac{1}{\frac{1}{K(s)} + 1}
           2\pi \nu_{\mathrm{beat}}(s) \tau_{d}
\end{equation}

With this change, the loop filter $K(s)$ is set to a constant value to get
\gls{UGF} of 2000 Hz, and it remains fixed. In the output of our signal, we can
faithfully compensate by the inverse of the transfer function shown above to
get the true beatnote fluctuations as:

\begin{equation}
\label{eq:DFDcorrection}
\nu_{\mathrm{beat, meas}}(s) = \left(
                                     \frac{1}{K(s)} + 1
                                \right)
                                \frac{1}{2 \pi \tau_{d}}
                                \phi(s)
\end{equation}

Note that the above filter is not realistic to implement as it is as it has
more zeros than poles. Two high-frequency poles will be required to implement
this in real time but we can not put those poles much higher than 2 kHz as the
CIT 40m \gls{CDS} system runs at 16 kHz. So we'll have to correct for the
complex gain factor in the demodulated signals offline.
