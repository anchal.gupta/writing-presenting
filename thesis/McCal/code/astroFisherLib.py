"""
Hang Yu
hangyu@caltech.edu

Modified by:
Anchal Gupta
anchal@calteh.edu
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as interp
import scipy.integrate as integ
import scipy.constants as scc
import pycbc.waveform as wf
from time import sleep
from tqdm import tqdm
from collections.abc import Iterable


Ms = 1.98847e30  # Solar mass in kg (https://en.wikipedia.org/wiki/Solar_mass)

####################################
#     waveform generation          #
####################################


def get_BNS_fd_waveform(freq, par_ast, tc=2.5e-3):
    Mc_Ms, qq, dist_Mpc, lamTide, phic = par_ast
    mass1_Ms, mass2_Ms = get_comp_mass(Mc_Ms, qq)

    # enforcing lambda1=lambda2=lamTide
    # and select only the plus polarization
#     tc = get_t_merger(freq.min(), mass1_Ms * Ms, mass2_Ms * Ms)
    freq_wf = wf.Array(freq)
    hp, __ = wf.get_fd_waveform_sequence(approximant='IMRPhenomD_NRTidal',
                                         distance=dist_Mpc,
                                         mass1=mass1_Ms, mass2=mass2_Ms,
                                         lambda1=lamTide, lambda2=lamTide,
                                         NRTidal_version='NRTidalv2_V',
                                         sample_points=freq_wf,
                                         coa_phase=phic)

    hp *= np.exp(1j * 2.*np.pi*freq*tc)
    return hp


def get_BBH_fd_waveform(freq, par_ast, tc=2.5e-3):
    Mc_Ms, qq, dist_Mpc, spinz, phic = par_ast
    mass1_Ms, mass2_Ms = get_comp_mass(Mc_Ms, qq)

    # enforcing spin1z=spin2z=spinz
    # and select only the plus polarization
#     tc = get_t_merger(freq.min(), mass1_Ms * Ms, mass2_Ms * Ms)
    freq_wf = wf.Array(freq)
    hp, __ = wf.get_fd_waveform_sequence(approximant='IMRPhenomD',
                                         distance=dist_Mpc,
                                         mass1=mass1_Ms, mass2=mass2_Ms,
                                         spin1z=spinz, spin2z=spinz,
                                         sample_points=freq_wf,
                                         coa_phase=phic)
    hp *= np.exp(1j * 2.*np.pi*freq*tc)
    return hp


def get_IFO_cal(freq, par_cal,
                Larm=3995.):
    """
    closed-loop power to open-loop length

    here ignores loop effect
    """
    g0, fd = par_cal

    dPdL = g0 * np.exp(-1j*2.*np.pi*freq*Larm/scc.c) / (1. + 1j*freq/fd)

    # ignores loop effect
    R = 1/dPdL
    return R


def get_waveform_w_cal_err(freq,
                           waveform_func, IFO_cal_func,
                           par, par_cal_t):
    """
    Get waveform with calibration errors
    Calculated by scaling true waveform by ratio of
    true calibration to real calibration (measured)
    """
    n_par = len(par)
    n_cal = len(par_cal_t)
    n_ast = n_par - n_cal

    par_ast = par[:n_ast]
    par_cal = par[n_ast:]

    hh = waveform_func(freq, par_ast)

    R_t = IFO_cal_func(freq, par_cal_t)
    RR = IFO_cal_func(freq, par_cal)

    gg = R_t / RR * hh
    return np.array(gg)


####################################
#       Fisher matrices            #
####################################

def get_fisher(freq, psd,
               waveform_func, IFO_cal_func,
               par,
               par_cal_t,
               dpar,
               idx_par=None):

    if not isinstance(dpar, Iterable):
        dpar = dpar * np.ones_like(par)
    if idx_par is None:
        idx_par = np.arange(len(par))
#     if log_flag is None:
#         log_flag = np.ones_like(par)

    n_dof = len(idx_par)
    n_freq = len(freq)

    dgg = np.zeros((n_dof, n_freq), dtype=np.complex)
    for ii in range(n_dof):
        par_u = par.copy()
        par_u[idx_par[ii]] += dpar[idx_par[ii]]

        gg_u = get_waveform_w_cal_err(freq,
                                      waveform_func, IFO_cal_func,
                                      par_u, par_cal_t)

        par_l = par.copy()
        par_l[idx_par[ii]] -= dpar[idx_par[ii]]

        gg_l = get_waveform_w_cal_err(freq,
                                      waveform_func, IFO_cal_func,
                                      par_l, par_cal_t)

        dgg[ii, :] = (gg_u - gg_l) / (2*dpar[idx_par[ii]])

#         if log_flag[idx_par[ii]]:
#             dgg[ii, :] *= par[idx_par[ii]]

    gamma = np.zeros((n_dof, n_dof))
    for ii in range(n_dof):
        for jj in range(ii, n_dof, 1):
            gamma[ii, jj] = np.real(inner_product(dgg[ii, :], dgg[jj, :],
                                                  freq, psd))

    for ii in range(n_dof):
        for jj in range(ii):
            gamma[ii, jj] = gamma[jj, ii]

    return gamma


def get_fisher2(freq, psd,
                waveform_func, IFO_cal_func,
                par,
                par_cal_t,
                dpar,
                idx_par=None,
                dIFO_cal_mag=0.01, dIFO_cal_ph=0.01):
    '''
    Function to get fisher matrix with respect to all astro parameters
    and changes in detector calibration function at each frequency bin
    in magnitude and phase
    '''

    if not isinstance(dpar, Iterable):
        dpar = dpar * np.ones_like(par)
    if idx_par is None:
        idx_par = np.arange(len(par))

    n_dof = len(idx_par)
    n_freq = len(freq)

    dgg = np.zeros((n_dof + 2 * n_freq, n_freq), dtype=np.complex)

    print('Calculating derivatives wrt astro parameters:')
    for ii in range(n_dof):
        par_u = par.copy()
        par_u[idx_par[ii]] += dpar[idx_par[ii]]
        gg_u = waveform_func(freq, par_u)

        par_l = par.copy()
        par_l[idx_par[ii]] -= dpar[idx_par[ii]]
        gg_l = waveform_func(freq, par_l)

        dgg[ii, :] = par[idx_par[ii]] * (gg_u - gg_l) / (2*dpar[idx_par[ii]])

    print('Calculating derivatives wrt calibration function magnitude')
    IFO_cal = IFO_cal_func(freq, par_cal_t)
    hh = waveform_func(freq, par)

    for ii in range(n_freq):
        IFO_cal_u = IFO_cal.copy()
        IFO_cal_u[ii] *= (1 + dIFO_cal_mag)
        gg_u = hh * IFO_cal / IFO_cal_u

        IFO_cal_l = IFO_cal.copy()
        IFO_cal_l[ii] *= (1 - dIFO_cal_mag)
        gg_l = hh * IFO_cal / IFO_cal_l

        # dgg[n_dof + ii, :] = ((gg_u - gg_l)
        #                       / (2 * np.abs(IFO_cal_u[ii]) * dIFO_cal_mag))
        # Multiplying with np.abs(IFO_cal_u[ii]) to get fractional EPS
        dgg[n_dof + ii, :] = ((gg_u - gg_l)
                              / (2 * dIFO_cal_mag))

    print('Calculating derivatives wrt calibration function phase')
    for ii in range(n_freq):
        IFO_cal_u = IFO_cal.copy()
        IFO_cal_u[ii] = (np.abs(IFO_cal_u[ii])
                         * np.exp(1j * (np.angle(IFO_cal_u[ii])
                                        * (1 + dIFO_cal_ph))))
        gg_u = hh * IFO_cal / IFO_cal_u

        IFO_cal_l = IFO_cal.copy()
        IFO_cal_l[ii] = (np.abs(IFO_cal_l[ii])
                         * np.exp(1j * (np.angle(IFO_cal_l[ii])
                                        * (1 - dIFO_cal_ph))))
        gg_l = hh * IFO_cal / IFO_cal_l

        # dgg[n_dof + n_freq + ii, :] = ((gg_u - gg_l)
        #                                / (2 * np.angle(IFO_cal_u[ii])
        #                                   * dIFO_cal_ph))
        # Multiplying with np.angle(IFO_cal_u[ii]) to get fractional EPS
        dgg[n_dof + n_freq + ii, :] = ((gg_u - gg_l)
                                       / (2 * dIFO_cal_ph))

    gamma = np.zeros((n_dof + 2 * n_freq, n_dof + 2 * n_freq))

    print('Calculating fisher matrix:')
    sleep(1)

    for ii in tqdm(range(n_dof + 2 * n_freq)):
        for jj in range(ii, n_dof + 2 * n_freq, 1):
            gamma[ii, jj] = np.real(inner_product(dgg[ii, :], dgg[jj, :],
                                                  freq, psd))

    for ii in range(n_dof):
        for jj in range(ii):
            gamma[ii, jj] = gamma[jj, ii]

    return gamma


####################################
#       small functions            #
####################################

def get_Mc(M1, M2):
    Mc = (M1*M2)**(3./5.)/(M1+M2)**(1./5.)
    return Mc


def get_comp_mass(Mc, q):
    Mt = (1.+q)**(6./5.)*Mc/q**0.6
    M1 = Mt/(1.+q)
    M2 = q*Mt/(1.+q)
    return M1, M2


def get_t_merger(f_gw, M1, M2):
    Mc = get_Mc(M1, M2)
    t_m = (5 / (256.*np.pi**(8./3.))
           * (scc.c**3./(scc.G*Mc))**(5./3.)
           * f_gw**(-8./3.))
    return t_m


def get_f_gw_2(f_gw_1, M1, M2, t):
    Mc = get_Mc(M1, M2)
    A = (96./5.)*np.pi**(8./3.)*(scc.G*Mc/scc.c**3.)**(5./3.)
    f_gw_2 = (f_gw_1**(-8./3.)-(8./3.)*A*t)**(-3./8.)
    return f_gw_2


def inner_product(h1, h2, freq, psd):
    rho_sq = 4 * integ.trapz((np.real(np.conj(h1) * h2)) / psd, freq)
    return rho_sq


def read_mag(freq, asd_file, fill_value=+np.inf):
    """
    when generating timeseries, should do fill_value=-np.inf
    when directly using the psd for snr calc, should do fill_value=np.inf
    """
    freq_in, asd_in = np.loadtxt(asd_file, unpack=True)

    idx = np.where(freq_in > 0)
    asd_vs_freq = interp.interp1d(freq_in[idx], np.log(asd_in[idx]),
                                  bounds_error=False, fill_value=fill_value)
    asd = np.exp(asd_vs_freq(freq))
    return asd


# codes for plotting the error ellipses
def get_prob_contour(x0, w, v):
    """
    x0 is the true value (center of the ellipse)
    w is the eigenvalue  (semi-major/minor axes)
    v is the matrix formed by eigenvectors (for rotation)
    """
    nPt = 1000
    vT = v.transpose()
    y0 = np.dot(vT, x0)
    y1 = np.linspace(y0[0] - np.sqrt(w[0]), y0[0] + np.sqrt(w[0]), nPt)
    y2 = np.zeros([2, nPt], dtype=np.complex)
    y2[0, :] = y0[1]+np.sqrt(w[1])*np.sqrt(1.+0j-(y1-y0[0])**2./(w[0]))
    y2[1, :] = y0[1]-np.sqrt(w[1])*np.sqrt(1.+0j-(y1-y0[0])**2./(w[0]))

    x1 = np.zeros([2, nPt])
    x2 = np.zeros([2, nPt])
    for ii in range(nPt):
        y_temp = np.array([y1[ii], np.real(y2[0, ii])])
        x_temp = np.dot(v, y_temp)
        x1[0, ii] = x_temp[0]
        x2[0, ii] = x_temp[1]

        y_temp = np.array([y1[ii], np.real(y2[1, ii])])
        x_temp = np.dot(v, y_temp)
        x1[1, ii] = x_temp[0]
        x2[1, ii] = x_temp[1]
    return x1, x2


def plot_prob_contour(sigma, theta, idx,
                      ax=None, label='', **ax_kwargs):
    """
    sigma is the covariance matrix
    theta is the list of true values
    idx is the indices for the two components whose covariance we want to check
    ax is an axis object onto which we plot the contour
    """
    err_mtrx = np.zeros([2, 2])
    err_mtrx[0, 0] = sigma[idx[0], idx[0]]
    err_mtrx[0, 1] = sigma[idx[0], idx[1]]
    err_mtrx[1, 0] = sigma[idx[1], idx[0]]
    err_mtrx[1, 1] = sigma[idx[1], idx[1]]
    w, v = np.linalg.eigh(err_mtrx)
    x1, x2 = get_prob_contour(np.array([theta[idx[0]], theta[idx[1]]]), w, v)
    if ax is None:
        fig = plt.figure(figsize=(4, 3))
        ax = fig.add_subplot(111)
    ax.plot(x1[0, :], x2[0, :], label=label, **ax_kwargs)
    ax.plot(x1[1, :], x2[1, :], **ax_kwargs)
    return ax
