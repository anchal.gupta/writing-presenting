\section{Calibration Scheme}
\label{sec:scheme}

Once the lock is acquired in Advanced LIGO, they typically turn off the
auxiliary lasers. But if the lasers are kept on, the individual arm length
tracking by the auxiliary lasers can be used to calibrate the \gls{DARM} the
response function of the detector. Fig.~\ref{fig:CalSchematic} shows this
multicolor calibration scheme in broad strokes. In this section, I'll introduce
this scheme with mathematical details. Please note that the
Fig.~\ref{fig:CalSchematic} uses beat notes between the seed laser of the
auxiliary frequency and the transmitted main laser in comparison to taking
beatnote at the auxiliary WaveLength in transmission as shown in the \gls{ALS}
scheme (Fig.~\ref{fig:ALSschematic}), but there is no difference between the two
cases and the same set of reasoning works no matter how the lasers are compared
with each other.

\subsection{Arm length modulation}
Consider a single arm cavity with nominal length $L_x$. An auxiliary laser
field of frequency $\nu_{\mathrm{aux}}$ is locked to the $q$-th resonant mode
at $\nu_q = q c / 2 L_x$, with $q \in \mathbb{Z}$ such that
$\nu_{\mathrm{aux}}$ measures fluctuations in $L_x$. To calibrate the linear
strain response of the interferometer, we may inject one or more single
frequency calibration lines of strength $\Gamma_i$ and frequencies $f_i$, by
modulating a single arm length such that:

\begin{equation}
  \frac{\delta L_x(t)}{L_x} = \sum_i \Gamma_i \cos(2\pi f_i t)
\end{equation}

While we set $f_i$, we typically only have an estimate of $\Gamma_i$ through
test mass actuation transfer function estimate, and method used to apply the
actuation. If we can accurately measure the actual $\Gamma_i$, we can calibrate
the \gls{DARM} strain in the interferometer since the \gls{DARM} strain is
given by:

\begin{equation}
\label{eq:DARMstrain}
\Gamma_{\mathrm{DARM}}
= \frac{\delta (L_x - L_y)(t)}{L_x + L_y} = \frac{\delta L_x(t)}{2L_x}
                                          = \frac{1}{2}
                                          \sum_i \Gamma_i \cos(2\pi f_i t)
\end{equation}

Since we care about \gls{DARM} strain and not the actual transfer function of
our actuation, this method simply aims to measure $\Gamma_i$ regardless of how
it was generated in the system. This agnosticism of actuation is important to
get rid of many possible systematic uncertainties that come with transfer
function estimations.

The above equations are simplified in the sense that we are only talking about
our excitation signal at those particular frequencies. In practice, there is
noise present in all frequencies but we will demodulate our measurements at the
particular calibration line frequencies so that we reject noise in other
frequency bands. So only the noise present within demodulation bandwidth near
the calibration line frequencies will enter the measurement which can be made
negligible by using appropriate strength in driving the calibration lines. For
now, we continue with this simplified model to get a basic understanding of
this scheme.

\input{McCal/figureTex/CalSchematic}

\subsection{Auxiliary laser control loop}
\label{subsec:auxloop}

The auxiliary lasers at the ends are locked to individual arm cavities using
the conventional \gls{PDH} technique. If we call the open loop transfer
function of the frequency stabilization control loop for auxiliary laser
$G_{\mathrm{OL}}(s)$, then the fluctuations in arm cavity length are
transferred to laser frequency such that:

\begin{equation}
\label{eq:AUXfreq}
\begin{aligned}
  \delta \tilde{\nu}_{\mathrm{aux}}(s) &= G_{\mathrm{OL}}(s)
                                         \left(
                                         \delta \tilde{\nu}_{\mathrm{aux}}(s)
                                         -
                                         \delta \tilde{\nu}_q (s)
                                         \right) \\
  \delta \tilde{\nu}_{\mathrm{aux}}(s) &= G_{\mathrm{OL}}(s)
                                         \left(
                                         \delta \tilde{\nu}_{\mathrm{aux}}(s)
                                         +
                                         \delta \tilde{L}_x(s)
                                         \frac{q c}{2 L_x^2}
                                         \right)\\
  \delta \tilde{\nu}_{\mathrm{aux}}(s) &= G_{\mathrm{OL}}(s)
                                         \left(
                                         \delta \tilde{\nu}_{\mathrm{aux}}(s)
                                         +
                                         \delta \tilde{L}_x(s)
                                         \frac{\nu_{\mathrm{aux}}}{L_x}
                                         \right)\\
  \delta \tilde{\nu}_{\mathrm{aux}}(s) &= \frac{G_{\mathrm{OL}}(s)}{1 -
G_{\mathrm{OL}}(s)}
                                         \frac{\nu_{\mathrm{aux}}}{L_x}
                                         \delta \tilde{L}_x(s)\\
  \delta \tilde{\nu}_{\mathrm{aux}}(s) &= G_{L \rightarrow \nu}(s)
                                         \frac{\nu_{\mathrm{aux}}}{L_x}
                                         \delta \tilde{L}_x(s)\\
\end{aligned}
\end{equation}

Here we defined the transfer function from length fluctuations to frequency
fluctuations for this loop as:
\begin{equation}
\label{eq:AUXloop}
  G_{L \rightarrow \nu}(s) = \frac{G_{\mathrm{OL}}(s)}{1 - G_{\mathrm{OL}}(s)}
\end{equation}

Thus the laser frequency encodes the strain $\delta L_x / L_x$ in it. In
practice, we generate auxiliary laser fields through a second harmonic
generation (SHG) stage seeded by a laser field operating near the main
interferometer frequency $\nu_{\mathrm{seed}} = \nu_{\mathrm{aux}} / 2$.
Because SHG is a highly coherent process~\cite{Yeaton2012}, the relative
frequency noise $\delta \nu_{\mathrm{seed}}/\nu_{\mathrm{seed}}$ in the main
seed laser encodes the single-arm strain signal as well.

\begin{equation}
  \delta \tilde{\nu}_{\mathrm{seed}}(s) =
                                \frac{\nu_{\mathrm{seed}}}{\nu_{\mathrm{aux}}}
                                \delta \tilde{\nu}_{\mathrm{aux}}(s)
                                = G_{L \rightarrow \nu}(s) \nu_{\mathrm{seed}}
                                  \frac{\delta \tilde{L}_x(s)}{L_x}
\end{equation}

\subsection{Main laser CARM control loop}
Let the closest resonance frequency for \gls{CARM} (Common mode between the two
arms) degree of freedom of the interferometer be $\nu_p = pc/(L_x + L_y)$,
where $p
\in \mathbb{Z}$. If we call the open loop transfer function of the frequency
stabilization control loop that locks the main laser to this resonance of
\gls{CARM} mode be $H_{\mathrm{OL}}$. Then following the same calculations as
Sec.\ref{subsec:auxloop}, we get:

\begin{equation}
\label{eq:CARMfreq}
\begin{aligned}
  \delta \tilde{\nu}_{\mathrm{main}}(s) &= H_{L \rightarrow \nu}(s)
                                           \nu_{\mathrm{main}}
                                           \frac{\delta \tilde{(L_x + L_y)}(s)}
                                           {L_x + L_y}\\
  \delta \tilde{\nu}_{\mathrm{main}}(s) &= \frac{1}{2}
                                           H_{L \rightarrow \nu}(s)
\nu_{\mathrm{main}}
                                           \frac{\tilde{\delta L_x}(s)}{L_x}
\end{aligned}
\end{equation}

Here we used the fact that $L_x \approx L_y$ and we define the transfer
function from \gls{CARM} length fluctuations to frequency fluctuations for this
loop as:
\begin{equation}
\label{eq:CARMloop}
  H_{L \rightarrow \nu}(s) = \frac{H_{\mathrm{OL}}(s)}{1 - H_{\mathrm{OL}}(s)}
\end{equation}

Thus the main laser frequency carries half as much strain of a single arm
within its loop bandwidth. This is interesting to look a bit deeper. The
single-arm actuation is equivalent to applying half as much strain actuation in
both \gls{DARM} and \gls{CARM} simultaneously. For calibration lines that are
within the CARM loop bandwidth, half of the strain would transfer to relative
main laser frequency fluctuations. For calibration lines outside the \gls{CARM}
loop bandwidth (which will not be the case in the full lock configuration of
Advanced LIGO), the main laser frequency does not see these lines. The equal
division of applied strain in \gls{DARM} and \gls{CARM} modes depends on the
macroscopic arm lengths. If the arm lengths are equal to within 0.1\%, we can
take them to be equal in our calculations without worrying about the strain
mismatch. Even if they are not equal, measuring 4 km arms to better than 40 cm
accuracy is not that hard and can be easily done to get the correct division of
strain. For now, we'll assume that the arm lengths are equal to each other
within 0.1\%.

\subsection{Beatnote frequency measurement}
\label{subsec:calBeatMeas}
If beatnote is measured between the locked main laser frequency and seed laser
of the auxiliary laser, we get the following relations:

\begin{equation}
\label{eq:beat}
\begin{aligned}
\delta \tilde{\nu}_{\mathrm{beat}}(s) &= \delta \tilde{\nu}_{\mathrm{main}}(s)
                                  - \delta \tilde{\nu}_{\mathrm{seed}}(s)\\
                                      &= \frac{1}{2}
                                          H_{L \rightarrow \nu}(s)
                                          \nu_{\mathrm{main}}
                                          \frac{\tilde{\delta L_x}(s)}{L_x}
                                          -  G_{L \rightarrow \nu}(s)
                                          \nu_{\mathrm{seed}}
                                          \frac{\delta \tilde{L}_x(s)}{L_x}\\
                                      &= \nu_{\mathrm{main}}
                                         \left(
                                               \frac{1}{2}
                                               H_{L \rightarrow \nu}(s)
                                               -  G_{L\rightarrow \nu}(s)
                                         \right)
                                         \frac{\delta \tilde{L}_x(s)}{L_x}
\end{aligned}
\end{equation}

Here we used the fact that $\nu_{\mathrm{main}} \approx \nu_{\mathrm{seed}}$
within 0.1\%. One can easily verify that the beatnote frequency fluctuations
come out to be the same in the case when the beatnote is measured between the
second harmonic of the main laser frequency and the auxiliary laser.

If the beatnote frequency is demodulated at the calibration line frequencies to
get $\delta \nu_{\mathrm{beat},i}$, then we get the following relationship:

\begin{equation}
\label{eq:calibration}
\begin{aligned}
\delta \nu_{\mathrm{beat},i} &= \nu_{\mathrm{main}}
                                 \left(
                                       \frac{1}{2} H_{L \rightarrow \nu}
                                       -  G_{L \rightarrow \nu}
                                 \right)
                                 \Gamma_i\\
\Gamma_i &= \frac{\delta \nu_{\mathrm{beat},i}\, \lambda_{\mathrm{main}}}
                 {c \left(
                          \frac{1}{2}H_{L \rightarrow \nu}
                          -  G_{L \rightarrow \nu}
                    \right)
                  }
\end{aligned}
\end{equation}

Thus, we have a physical calibration for $\Gamma_i$ that can be used to
calibrate the \gls{DARM} strain. Let's verify this expression for a line at
around 30 Hz which is within the bandwidth of both the \gls{CARM} loop and the
auxiliary loop. This line would very high open loop gains $H_{\mathrm{OL}}(30
\mathrm{Hz}) \gg 1$, and $G_{\mathrm{OL}}(30 \mathrm{Hz}) \gg 1$ so that $H_{L
\rightarrow \nu}(30
\mathrm{Hz}) \approx -1$ and $G_{L \rightarrow \nu}(\mathrm{30} Hz) \approx -1$
giving $\Gamma_{30\mathrm{Hz}} = 2\delta \nu_{\mathrm{beat},30\mathrm{Hz}}
\lambda_{\mathrm{main}}/c$. While if the calibration line is at 500 Hz which
could be outside the \gls{CARM} loop bandwidth with still well within the
auxiliary laser loop bandwidth, we get $H_{\mathrm{OL}}(500 \mathrm{Hz}) \ll 1$,
and $G_{\mathrm{OL}}(500 \mathrm{Hz}) \gg 1$ so that $H_{L \rightarrow \nu}(500
\mathrm{Hz}) \approx 0$, and $G_{L \rightarrow \nu}(500 \mathrm{Hz}) \approx
-1$ giving $\Gamma_{500\mathrm{Hz}} = \delta \nu_{\mathrm{beat},500\mathrm{Hz}}
\lambda_{\mathrm{main}}/c$. Note that there is a factor of 2 difference between
the two cases, but in either case, as long as the calibration lines are within
the bandwidth of the auxiliary laser loop, we can utilize the demodulated
beatnote to estimate the actual single arm strain $\Gamma_i$ and thus the
actual \gls{DARM} strain that was applied $\Gamma_i/2$.

\subsection{Calibrating the DARM strain}
\label{subsec:calibration_pipeline}

\input{McCal/figureTex/CalPipeline}

For a \gls{DARM} strain appearing in the arms, we can calculate the error point
signal and control point signal using the control loop diagram shown in
Fig.~\ref{fig:calibration_pipeline}.

\begin{equation}
\begin{aligned}
d_{\mathrm{e}}(s) &= (\Gamma_{\mathrm{DARM}} + d_{\mathrm{c}}(s) A)C(s)\\
d_{\mathrm{c}}(s) &= d_{\mathrm{e}}(s) K(s)\\ d_{\mathrm{e}}(s) &=
\frac{C(s)}{1 - CKA(s)}\Gamma_{\mathrm{DARM}}\\ d_{\mathrm{c}}(s) &= \frac{C(s)
K(s)}{1 - CKA(s)}\Gamma_{\mathrm{DARM}}\\
\end{aligned}
\end{equation}

Note, we have taken the convention of not assuming unaccounted negative signs
in the loop. All signs are absorbed in the transfer function blocks in the loop
shown. Then, the following combination of error and control points gives the
injected \gls{DARM} strain:

\begin{equation}
\Gamma_{\mathrm{DARM}} = \frac{d_{\mathrm{e}}(s)}{C(s)} - d_{\mathrm{c}}(s)
A(s)
\end{equation}

Thus to calibrate, we need to model transfer functions for $C(s)$ and $A(s)$.
For Fabry-P\'erot Michelson Interferometer (FPMI), we assume that
$C^{\mathrm{model}}(s)$ is a simple low pass filter due to the \gls{DARM}
cavity pole with some gain due to the optical gain. For
$A^{\mathrm{model}}(s)$, since we are interested in frequencies far above the
suspension resonance frequencies ($\sim 1 Hz$ for us), we can model it as a
2-pole at 0 Hz filter with some gain. This is because the suspension resonance
frequency changes do not affect the shape of the transfer function above the
resonance, it goes down as $1/f^2$ with changes in overall strength only. Thus
we introduce the parameters listed in Table \ref{tab:calModelParams}.

\input{McCal/tableTex/calModelParams}

And we defined the model transfer functions as:

\begin{equation}
\begin{aligned}
C^{\mathrm{model}}(f) &= \frac{g_d}{1 + i f / f_d}\\ A^{\mathrm{model}}(f)
                      &= -\frac{a_d}{f^2}
\end{aligned}
\end{equation}

Thus while calibration lines are on, we demodulate the beat frequency signal at
the calibration line frequencies to obtain $\Gamma_{\mathrm{DARM}, i} =
\Gamma_i/2$ and we demodulate \gls{DARM} error and control points at the same
line
frequencies to get $d_{\mathrm{e}, i}$ and $d_{\mathrm{c}, i}$. Then we obtain
the values for $g_d$, $f_d$, and $a_d$ such that the following cost function is
minimized (simple least squares estimate):

\begin{equation}
\sum_i \left(
       d_{\mathrm{e}, i}\frac{1 + i f_i/f_d}{g_d}
       + d_{\mathrm{c}, i} \frac{a_d}{f_i^2}
       - \Gamma_{\mathrm{DARM}}\right)^2
\end{equation}

This completes one instance of calibration. Depending on how long we integrate
that is how small the low pass filter cut-off frequency we use in the
demodulation of the signals, we can repeat this calculation periodically to
calibrate the \gls{DARM} output in real time. For example, if we choose to
integrate for 100s, we would implement a 0.01 Hz low pass filter in the
demodulation, and would ideally wait for 500s after any changes to the
calibration model to read back the demodulated signal. Thus we can correct the
calibration model every 500s in this scenario.
