\section{Uncertainty sources}
The calibration of the detector can be off of the true value due to two
possible reasons. First, the measurement is noisy due to statistical noise in
the different terms appearing in Eq.~\ref{eq:calibration}. Second, the
measurements of the different terms in Eq.~\ref{eq:calibration} can be off the
true value due to a systematic error, such as miscalibration of the measurement
apparatus or 1/f noise in the measurement apparatus such as ADC, DAC, etc. It
is important to understand why 1/f noise, also known as flicker noise, is not
the same as statistical noise.

\subsection{Systematic uncertainty due to flicker noise}
\input{McCal/figureTex/ITMYallenDev}

When averaging a measurement, common intuition is that if you average for
longer, you sample more points, and thus the standard deviation of the
measurement goes down as the square root of the number of sample points. But
not all points are independent in time-varying signals. Taking an average for a
time $\tau$ of a time-varying signal is equivalent to applying a low pass
filter on the signal with a cut-off frequency of $1/\tau$ and then summing up
the output of the filter. The averaging reduces noise in the measurement by
suppressing high-frequency noise above $1/\tau$ Hz. The noise is suppressed by
the frequency domain response of the single pole low pass filter, that is,
$1/f$. Thus averaging works for reducing the noise if the noise in the signal
is dominated by a source, whose noise spectral density drops slower than $1/f$,
like $1/f^{-0.5}$, or flat noise, etc. But if the noise spectral density is
dominated by flicker noise which goes as $1/f$, then the averaging does not
help and the standard deviation remains constant even after averaging for
longer.

If the dominating noise source is worse, like $1/f^2$, one would see the
standard deviation increasing with an increase in averaging time. Almost all
processes and thus signals in the world are limited by flicker noise at low
frequencies and even steeper noise sources below that. This means that there is
an optimal averaging time for estimating the signal to the best possible
estimate beyond which the estimate will become more uncertain because the
system drifts over long periods. At this junction, this kind of error is
systematic noise, since it is not possible to statistically remove it.

We analyzed the beatnote frequency signal to look for uncertainty due to
systematic noise coming from low-frequency drifts like flicker noise. For this
purpose, we looked at the beatnote frequency signal mixed with the calibration
line frequency and averaged it with different averaging times.
Fig.~\ref{fig:ITMYallenDev} shows the Allan deviation for one such dataset taken
for calibration of \gls{ITMY} actuation (discussed in
Sec.\ref{sec:ActuatorCalibration}). We see that the fractional error is not
saturating for any line up to 400 seconds of averaging, and except for 575.17
Hz, all other lines kept improving with averaging up to 1000s. Our goal for
this calibration method is to have a calibrator running at a 0.01 Hz rate, so
we do not plan to integrate longer than 100 seconds anyways. This measurement
proves that the beatnote frequency measurement is not limited by systematic
noise due to flicker noise, and provides the statistical uncertainty limit for
each line. Alternatively, if the averaging is not limited by flicker noise, one
can use this method with longer integration times to calibrated the photon
calibrator and use the photon calibrator for the in-between time when it is not
getting calibrated. To do this properly, the two methods should be run together
to identify the drift time in photon calibrator calibration and this method can
be used with long averaging times to calibrate photon calibrator once in a
while to keep it corrected against systematic drifts.

\subsection{Systematic offset uncertainty}
If the measurement of any of the terms in Eq.~\ref{eq:calibration} is at an
offset from the true value, we would have a systematic uncertainty that is not
possible to remove or avoid by careful averaging. The best we can do is make
our measurement robust to such kind of systematics. The arguments become
qualitative at this point as it is not possible to find such systematics until
an even better calibration method can be used to compare against them. One
simple first check would be to use whatever method is available with its worse
uncertainty to compare if the two methods agree with each other within their
confidence interval. In Fig.~\ref{fig:ITMYActCal} we compared the actuation
calibration of \gls{ITMY} performed using the beatnote frequency and by using
the Michelson interferometer fringes as that is the only other method available
at CIT 40m to compare our calibration method against. Here, we do see a
systematic offset between the two methods but we do not know which method the
systematic is coming from. At the Advanced LIGO sites, Photon calibration
should be used to perform this comparison test to rule out and/or debug any
systematic offsets in this calibration method, if found.

Additionally, we can individually estimate how much each term in
Eq.~\ref{eq:calibration} could be off from the true value based on experience.
Here we list some possible systematic uncertainty sources:

\subsubsection{$\lambda_{\mathrm{main}}$:}

The main laser frequency is known accurately to 5 digits from the spec sheet of
the laser. Since the tuning range of the \gls{NPRO} laser we use is 30 GHz as
per the datasheet, the systematic shift from tuning the laser crystal
temperature can be a maximum of 0.01\%. This is not a true limitation though
since we can use the Iodine spectrum line P 83(33-0) \cite{Ye1999} compared
with the frequency-doubled main laser to measure the wavelength to better than
0.1 ppm. After this measurement, the temperature drift of the free-running
laser is reported to be less than 50 MHz/hour which means even with no
temperature stabilization efforts, the systematic error would be less than 0.2
ppm due to this drift. All these points support the fact that the high
frequency of laser as a reference gives immense relative error advantage for
making precision measurements.

\subsubsection{$H_{L \rightarrow \nu}$:}

\input{McCal/tableTex/CARMloopGainDrift}

The transfer function from \gls{CARM} length fluctuations to the main laser
frequency is used in the measurement. This transfer function is measured before
the start of calibration (when calibration frequency lines are turned on). It
can drift over time due to optical gain drift of the \gls{CARM} \gls{PDH} error
signal. To increase the robustness of our calibration against such drifts, we
implemented digital filters in the \gls{CARM} servo loop. A resonant gain
filter with 60 dB gain is applied at 33.921 Hz to ensure that the open loop
gain for \gls{CARM} loop is high and that the loop follows the length
fluctuations faithfully at this calibration line. Using Eq.~\ref{eq:CARMloop},
we see that:

\begin{equation}
\begin{aligned}
\frac{\Gamma_i}{\Gamma_i} &= \left|
                             \frac{H_{L \rightarrow \nu}}
                                  {H_{L \rightarrow \nu}
                                   - 2 G_{L \rightarrow \nu}}
                            \right|
                            \frac{\Delta |H_{L \rightarrow \nu}|}
                                 {|H_{L \rightarrow \nu}|}\\
\frac{\Delta |H_{L \rightarrow \nu}|}{|H_{L \rightarrow \nu}|}
                          &= \left|\frac{1}{1 - H_{\mathrm{OL}}}\right|
                             \frac{\Delta |H_{\mathrm{OL}}|}{|H_{\mathrm{OL}}|}
\end{aligned}
\end{equation}

To reduce the effect of fluctuations in \gls{OLTF}, we implement resonant gain
at 33.921 Hz in the \gls{CARM} loop to have a very high gain at this frequency.
While for calibration lines outside the loop bandwidth, we apply deep notches
in the \gls{CARM} loop to ensure the loop does not respond at those frequencies
(see Fig.~\ref{fig:CARM_OLTF}). Table.~\ref{tab:CARMloopGainDrift} lists
different sources of drift in \gls{CARM} loop gain and their effect on the
calibration. We see that the uncertainty in the calibration of strain even with
worst-case scenarios does not go beyond 0.1\% (1000 ppm) for any of the lines.
The uncertainty is maximum for the 211.11 Hz line because it is very close to
the \gls{UGF} of the loop (200 Hz) and the notch is not deep enough there.

\subsubsection{$G_{L \rightarrow \nu}$:}

\input{McCal/tableTex/AUXloopGainDrift}

\input{McCal/figureTex/ITMYAUXOGC}

The auxiliary loop transfer function is also used for the measurement and all
lines are within the \gls{UGF} of this loop. Ideally, we want the auxiliary
loop to follow the lines with high fidelity, which means we should implement
resonant gain filters in the feedback filter for this loop. Since this loop is
high bandwidth though, it is implemented with analog filters at CIT 40m and the
team is still working on trying to insert a digital resonant filter using
\gls{FPGA} in this loop. Without such resonant gains, the nominal gain of the
loop at the calibration line frequencies will determine the uncertainty at
different lines. Using Eq.~\ref{eq:AUXloop}, we see that:

\begin{equation}
\begin{aligned}
\frac{\Gamma_i}{\Gamma_i} &= \left|\frac{G_{L \rightarrow \nu}}
                                  {\frac{1}{2}H_{L \rightarrow \nu}
                                   - G_{L \rightarrow \nu}}\right|
                            \frac{\Delta |G_{L \rightarrow \nu}|}
                                 {|G_{L \rightarrow \nu}|}\\
\frac{\Delta |G_{L \rightarrow \nu}|}{|G_{L \rightarrow \nu}|}
                          &= \left|\frac{1}{1 - G_{\mathrm{OL}}}\right|
                             \frac{\Delta |G_{\mathrm{OL}}|}{|G_{\mathrm{OL}}|}
\end{aligned}
\end{equation}

Fig.~\ref{fig:YAUX_OLTF} shows the measured open loop gain during calibration
measurement. We performed a fit to extract the value at 33.921 Hz where the
loop gain is so high that the measurement setup runs out of dynamic range.
Using the values from this curve, we estimate uncertainty contributions from
different sources of drift in \gls{AUX} loop gain in
Table.~\ref{tab:AUXloopGainDrift}. We again see that due to less gain at higher
frequencies, the possible systematic uncertainty goes above 1000 pm, primarily
due to laser power fluctuations or mode matching fluctuations that happen due
to cavity misalignment. Both these effects reduce the circulating power inside
the cavity which reduces the \gls{PDH} error signal optical gain for the loop.
To get around this problem, we measure the transmitted power of the auxiliary
laser from the arm cavity which is directly proportional to the circulating
power inside the cavity. We calculate the ratio of this power from the power
when the transfer function was measured and use this correction in real time
for the value of $G_{L \rightarrow \nu}$ used in calculating $\Gamma_i$.
Fig.~\ref{fig:ITMYAUXOGC} shows the optical gain correction applied in this
measurement. This way, we reduce the contribution from circulating power
fluctuations and the resulting possible systematic uncertainty. Note that these
are still very pessimistic estimates but fluctuations in phase modulation index
or RFPD response functions are not of the order of 0.1\% in practice. This
limitation is also not fundamental as laser locking bandwidths well above 100
kHz can be achieved easily if uncertainty from this source is to be reduced in
the future.

\input{McCal/tableTex/ArmLengthEffect}

\subsubsection{$\delta \nu_{\mathrm{beat},i}$ measurement:}

The beatnote measurement setup is specifically designed to accurately measure
the beatnote fluctuations. The calibration of \gls{DFD} is good to 0.03\%
certainty\cite{Gautam2021} which includes the effects of all electronics in the
chain to digital counts. We measured the effect of amplitude modulation of
beatnote frequency showing up as frequency modulation due to the phase tracker
servo. This value is also very small, 0.09\%. By normalizing the inputs before
sending them to the phase tracker, we removed this source of error in our
measurement (See Sec.\ref{subsec:DFD}).

\subsubsection{Arm length measurements:}

In Eq.~\ref{eq:calibration}, the arm lengths do not show up explicitly because
we made the assumptions about the arm length being equal to each other. The
full form without any assumptions is:

\begin{equation}
\delta \Gamma_{\mathrm{DARM}} = -2 \frac{L_{\mathrm{X}} L_{\mathrm{Y}}}
                                        {(L_{\mathrm{X}} + L_{\mathrm{Y}})^2}
                                \frac{1}
                                     {\frac{L_{\mathrm{Y}}}
                                           {L_{\mathrm{X}} + L_{\mathrm{Y}}}
                                           H_{L \rightarrow \nu}
                                      - G_{L \rightarrow \nu}}
                                \frac{\lambda_{\mathrm{main}}
                                     \delta \nu_{\mathrm{beat, i}}}
                                     {c}
\end{equation}

\input{McCal/tableTex/TotalUncertainty}

The derivative of this equation with respect to $L_{\mathrm{X}}$ and
$L_{\mathrm{Y}}$ provide the uncertainty propagation. The arm length is
measured by measuring the free spectral range of the arm cavity by locking the
arm cavity to the auxiliary laser, locking the main laser to the beatnote
frequency between itself and the auxiliary laser, and then scanning the laser
by changing the offset in the lock point. Table.~\ref{tab:ArmLengthEffect} lists
the uncertainties in current arm length measurements at CIT 40m. The CIT 40m
prototype suffers worse in this case than would be the case of Advanced LIGO as
longer arms mean less uncertainty in individual arm length measurement. The
above measurement can also be improved further.

\subsubsection{Total uncertainty:}

We estimate the total uncertainty in our results as the quadrature sum of
statistical uncertainty and each worst-case scenario systematic uncertainty
that we estimated in the previous sections. Table.~\ref{tab:TotalUncertainty}
shows the summary of all the uncertainty sources and total uncertainty expected
at each calibration line for measuring \gls{DARM} strain. Note that we are
limited by statistical uncertainty at each calibration line which is the ideal
case. The statistical uncertainty can be reduced further by increasing the
strength of modulation in each calibration line, increasing the averaging time,
or reducing the statistical noise in the measurement by improving the \gls{AUX}
laser loop. The systematic effects can be studied further to narrow the
uncertainty due to them.
